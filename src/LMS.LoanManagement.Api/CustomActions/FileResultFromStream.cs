﻿using System.IO;
using System.Threading.Tasks;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using LMS.LoanManagement.Api.Utilities;

namespace LMS.LoanManagement.Api.Controllers.CustomActions
{
    /// <summary>
    /// FileResultFromStream
    /// </summary>
    public class FileResultFromStream : ActionResult
    {
        /// <summary>
        /// FileResultFromStream
        /// </summary>
        /// <param name="fileDownloadName"></param>
        /// <param name="fileStream"></param>
        /// <param name="contentType"></param>
        public FileResultFromStream(string fileDownloadName, Stream fileStream, string contentType = null)
        {
            FileDownloadName = fileDownloadName;
            FileStream = fileStream;
            ContentType = contentType ?? MimeMapping.GetMimeMapping(Path.GetExtension(fileDownloadName));
        }

        private string ContentType { get; }
        private string FileDownloadName { get; }
        private Stream FileStream { get; }

        /// <summary>
        /// ExecuteResultAsync
        /// </summary>
        /// <param name="context"></param>
        public override async Task ExecuteResultAsync(ActionContext context)
        {
            var response = context.HttpContext.Response;
            response.ContentType = ContentType;
            response.Headers.Add("Content-Disposition", new[] { "attachment; filename=" + FileDownloadName });
            await FileStream.CopyToAsync(context.HttpContext.Response.Body);
            FileStream.Dispose();
        }
    }

}
