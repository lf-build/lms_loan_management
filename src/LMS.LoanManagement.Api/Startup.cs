﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LMS.LoanManagement.Persistence;
using LendFoundry.NumberGenerator.Client;
using LMS.Foundation.Amortization;
using LendFoundry.StatusManagement.Client;
using LMS.LoanAccounting.Client;
using LendFoundry.ProductConfiguration.Client;
using LMS.LoanManagement.Abstractions;
using LendFoundry.Security.Encryption;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.ProductRule.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Calendar.Client;
using LMS.Payment.Processor.Client;
using LendFoundry.EventHub;
using System.Threading.Tasks;
using System.Reflection;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.MoneyMovement.Ach.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using LendFoundry.Flag.Client;
using LendFoundry.Email.Client;

namespace LMS.LoanManagement.Api
{
    /// <summary>
    /// Startup
    /// </summary>
    internal class Startup  
    {
        /// <summary>
        /// ConfigureServices
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "LoanManagement"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LMS.LoanManagement.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddAmortization();
            services.AddEncryptionHandler();
            services.AddDependencyServiceUriResolver<LoanConfiguration>(Settings.ServiceName);
            services.AddTenantService();
            services.AddEventHub(Settings.ServiceName);
            services.AddNumberGeneratorService();
            services.AddStatusManagementService();
            services.AddAccrualBalance();
            services.AddProductService();
            services.AddLookupService();
            services.AddDataAttributes();
            services.AddProductRuleService();
            services.AddDecisionEngine();
            services.AddPaymentProcessorService();
            services.AddCalendarService();
            services.AddAchConfigurationService();
            services.AddFlag();
            services.AddEmailService();
            // interface resolvers
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });
            
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddConfigurationService<LoanConfiguration>( Settings.ServiceName);
          //  services.AddTransient(provider => provider.GetService<IConfigurationService<LoanConfiguration>>().Get());
            //services.AddTransient<IConfiguration, Configuration>();
            //services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<LoanConfiguration>>().Get());

            services.AddTransient<ILoanOnboardingService, LoanOnboardingService>();
            services.AddTransient<ILoanOnboardingServiceFactory, LoanOnboardingServiceFactory>();

            services.AddTransient<ILoanOnboardingRepository, LoanOnboardingRepository>();
            services.AddTransient<ILoanOnboardingRepositoryFactory, LoanOnboardingRepositoryFactory>();

            services.AddTransient<IBankDetailsRepository, BankDetailsRepository>();
            services.AddTransient<IBankDetailsRepositoryFactory, BankDetailsRepositoryFactory>();

            services.AddTransient<ILoanScheduleRepository, LoanScheduleRepository>();
            services.AddTransient<ILoanScheduleRepositoryFactory, LoanScheduleRepositoryFactory>();

            services.AddTransient<ILoanScheduleService, LoanScheduleService>();
            services.AddTransient<ILoanScheduleServiceFactory, LoanScheduleServiceFactory>();

            services.AddTransient<IBankDetailsHistoryRepository, BankDetailsHistoryRepository>();
            services.AddTransient<IBankDetailsHistoryRepositoryFactory, BankDetailsHistoryRepositoryFactory>();

            services.AddTransient<ILoanOnboardingHistoryRepository, LoanOnboardingHistoryRepository>();
            services.AddTransient<ILoanOnboardingHistoryRepositoryFactory, LoanOnboardingHistoryRepositoryFactory>();

            services.AddTransient<ILoanScheduleHistoryRepository, LoanScheduleHistoryRepository>();
            services.AddTransient<ILoanScheduleHistoryRepositoryFactory, LoanScheduleHistoryRepositoryFactory>();
            services.AddTransient<IPayOffFeeDiscountRepository, PayOffFeeDiscountRepository>();
            services.AddTransient<IPayOffFeeDiscountRepositoryFactory, PayOffFeeDiscountRepositoryFactory>();
            services.AddTransient<ILoanOnboardingListener, LoanOnboardingListener>();

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        /// <summary>
        /// Configure
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Loan Management Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseLoanOnboardingListener();
            app.UseMvc();
        }

        // public class RequestLoggingMiddleware
        // {
        //    private RequestDelegate Next { get; }

        //  public RequestLoggingMiddleware(RequestDelegate next)
        //  {
        //      Next = next;
        //    }

        //  public async Task Invoke(HttpContext context)
        //   {
        //      //The MVC middleware reads the body stream before we do, which
        //      //means that the body stream's cursor would be positioned at the end.
        //       // We need to reset the body stream cursor position to zero in order
        //       // to read it. As the default body stream implementation does not
        //      // support seeking, we need to explicitly enable rewinding of the stream:
        //        try
        //       {
        //            await Next.Invoke(context);
        //       }
        //        catch (ReflectionTypeLoadException exception)
        //       {
        //           Console.WriteLine(exception.LoaderExceptions[0].ToString());
        //         }
        //    }
        // }
    }
}
