﻿﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LMS.LoanManagement.Abstractions;
using LMS.LoanManagement.Api.Controllers.CustomActions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Api.Controllers
{
    /// <summary>
    /// LoanManagementController
    /// </summary>
    [Route("/")]
    public class LoanManagementController : ExtendedController
    {
         /// <summary>
        /// Constructor with logger injection
        /// </summary>
        /// <param name="loanOnboardingService"></param>
        /// <param name="loanScheduleService"></param>
        /// <param name="logger"></param>
        public LoanManagementController(ILoanOnboardingService loanOnboardingService,
            ILoanScheduleService loanScheduleService, ILogger logger) : base (logger)
        {
            LoanOnboardingService = loanOnboardingService;
            LoanScheduleService = loanScheduleService;
        }
        private ILoanOnboardingService LoanOnboardingService { get; }
        private ILoanScheduleService LoanScheduleService { get; }



        private static NoContentResult NoContentResult { get; } = new NoContentResult();
        
        #region Write APIs

        /// <summary>
        /// Add Applicant Details
        /// </summary>
        /// <param name="loanApplicationNumber"></param>
        /// <param name="applicantRequest"></param>
        [HttpPost("{loanApplicationNumber}/Applicant")]
        [ProducesResponseType(typeof(ILoanSchedule), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddApplicantDetails(string loanApplicationNumber,
            [FromBody]List<ApplicantRequest> applicantRequest)
        {
            try
            {
                return Ok(await LoanOnboardingService.AddApplicantDetails(loanApplicationNumber, applicantRequest));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanApplicationNumber, applicantRequest });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Add Bank Details
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="bankDetails"></param>
        [HttpPost("/bank/{loanNumber}")]
        [ProducesResponseType(typeof(IBankDetails), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> AddBankDetails(string loanNumber, [FromBody]BankDetails bankDetails)
        {
            try
            {
                return Ok(await LoanOnboardingService.AddBankDetails(loanNumber, bankDetails));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, bankDetails });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Add Fee Details
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="feeDetails"></param>
        [HttpPost("{loanNumber}/feedetails")]
        [ProducesResponseType(typeof(Abstractions.IFeeDetails), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddFeeDetails(string loanNumber, [FromBody]FeeDetails feeDetails)
        {
            try
            {
                return Ok(await LoanScheduleService.AddFeeDetails(loanNumber, feeDetails));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, feeDetails });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// AdHoc Reamortization
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="loanModificationRequest"></param>
        [HttpPost("{loanNumber}/adhoc")]
        [ProducesResponseType(typeof(List<ILoanScheduleDetails>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AdHocReamortization(string loanNumber, [FromBody]LoanModificationRequest loanModificationRequest)
        {
            try
            {
                return Ok(await LoanScheduleService.AdHocReamortization(loanNumber, loanModificationRequest));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, loanModificationRequest });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Add Other Contact
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="otherContact"></param>
        [HttpPost("{loanNumber}/othercontact")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddOtherContact(string loanNumber, [FromBody]OtherContact otherContact)
        {
            try
            {
                return Ok(await LoanOnboardingService.AddOtherContact(loanNumber, otherContact));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, otherContact });
                return ErrorResult.BadRequest(exception.Message);
            }

        }

        /// <summary>
        /// Add PayOff Discount
        /// </summary>
        /// <param name="payoffDiscount"></param>
        [HttpPost("/payoffschedule")]
        [ProducesResponseType(typeof(IPayOffFeeDiscount), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddPayOffDiscount([FromBody]PayOffFeeDiscount payoffDiscount)
        {
            try
            {
                return Ok(await LoanScheduleService.AddPayOffDiscount(payoffDiscount));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { payoffDiscount });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Amortize Loan
        /// </summary>
        /// <param name="loanScheduleRequest"></param>
        [HttpPost("/amortize")]
        [ProducesResponseType(typeof(List<ILoanScheduleDetails>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Amortize([FromBody]LoanScheduleRequest loanScheduleRequest)
        {
            try
            {
                return await ExecuteAsync(async () =>
                    Ok(await LoanScheduleService.Amortize(loanScheduleRequest, loanScheduleRequest.LoanAmount)));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanScheduleRequest });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Amortize Loan with product parameter
        /// </summary>
        /// <param name="portfolio"></param>
        /// <param name="roundingMethod"></param>
        /// <param name="roundingDigit"></param>
        /// <param name="loanScheduleRequest"></param>
        [HttpPost("/amortize/{portfolio}/{roundingMethod}/{roundingDigit}")]
        [ProducesResponseType(typeof(List<ILoanScheduleDetails>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult Amortizztion(string portfolio, string roundingMethod, int roundingDigit,
            [FromBody]LoanScheduleRequest loanScheduleRequest)
        {
            try
            {
                return Execute(() => Ok(LoanScheduleService.Amortize(loanScheduleRequest,
                    loanScheduleRequest.LoanAmount, portfolio, roundingMethod, roundingDigit)));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { portfolio, roundingMethod, roundingDigit,
                    loanScheduleRequest });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Add Automatic Fee Schedule
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="automaticFeeRequest"></param>
        [HttpPut("/automaticfee/{loanNumber}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AutomaticFeeSchedule(string loanNumber, [FromBody]AutomaticFeeRequest automaticFeeRequest)
        {
            try
            {
                return Ok(await LoanScheduleService.AutomaticFeeSchedule(loanNumber, automaticFeeRequest));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, automaticFeeRequest });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Change Loan Status
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="status"></param>
        /// <param name="reasons"></param>
        [HttpPost("/status/{loanNumber}/{status}/{*reasons}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> ChangeStatus(string loanNumber, string status, List<string> reasons)
        {
            try
            {
                await LoanOnboardingService.ChangeStatus(loanNumber, status, reasons);
                return NoContentResult;
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, status, reasons });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Change Status To Close
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="reasons"></param>
        [HttpPut("/status/close/{loanNumber}/{*reasons}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> ChangeStatusToClose(string loanNumber, string reasons)
        {
            try
            {
                List<string> reasonList = null;
                if (!string.IsNullOrWhiteSpace(reasons))
                {
                    reasons = WebUtility.UrlDecode(reasons);
                    reasonList = SplitReasons(reasons);
                }
                await LoanOnboardingService.ChangeStatusToClose(loanNumber, reasonList);
                return NoContentResult;
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, reasons });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Delete Fee Details
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="feeId"></param>
        [HttpDelete("/{loanNumber}/{feeId}/fee")]
        [ProducesResponseType(typeof(Abstractions.IFeeDetails), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteFeeDetails(string loanNumber, string feeId)
        {
            try
            {
                return Ok(await LoanScheduleService.DeleteFeeDetails(loanNumber, feeId));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, feeId });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Delete Loan Modification
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="loanScheduleId"></param>
        [HttpPut("{loanNumber}/loanmods/{loanScheduleId}/reject")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteLoanMod(string loanNumber, string loanScheduleId)
        {
            try
            {
                await LoanScheduleService.DeleteLoanMod(loanNumber, loanScheduleId);
                return NoContentResult;
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, loanScheduleId });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Download Amortization Schedule
        /// </summary>
        /// <param name="loanScheduleRequest"></param>
        [HttpPost("/download")]
        [ProducesResponseType(typeof(Stream), 200)]
        public async Task<IActionResult> DownloadAmortizationSchedule([FromBody]LoanScheduleRequest loanScheduleRequest)
        {
            try
            {
                return await ExecuteAsync(async () =>
                {

                    var result = await LoanScheduleService.DownloadAmortizationSchedule(loanScheduleRequest);

                    return new FileResultFromStream("test.csv", result);
                });
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanScheduleRequest });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
       	 /// <summary>
        /// Initialize new Loan
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="autoAccrualPastDate"></param>
        [HttpPost("/initialize/{loanNumber}/{autoAccrualPastDate}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Initialize(string loanNumber,bool autoAccrualPastDate)
        {
            try
            { 
                await LoanScheduleService.InitializeLoan(loanNumber,autoAccrualPastDate);
                return NoContentResult;
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Create Loan Schedule
        /// </summary>
        /// <param name="loanScheduleRequest"></param>
        [HttpPost("/schedule")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> LoanSchedule([FromBody]LoanScheduleRequest loanScheduleRequest)
        {
            try
            {
                return Ok(await LoanScheduleService.LoanSchedule(loanScheduleRequest));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanScheduleRequest });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// OnBoard Loan
        /// </summary>
        /// <param name="loanInformationRequest"></param>
        [HttpPost]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> OnBoard([FromBody]LoanInformationRequest loanInformationRequest)
        {
            try
            {
                return Ok(await LoanOnboardingService.OnBoard(loanInformationRequest));
            }
            catch (Exception exception)
            {
                 Logger.Error(FlattenException(exception),new {loanInformationRequest});
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Miss Schedule Payment
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="id"></param>
        /// <param name="paymentScheduleRequest"></param>
        [HttpPut("{loanNumber}/schedulemiss/{id}")]
        [ProducesResponseType(typeof(ILoanSchedule), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> ScheduleMissed(string loanNumber, string id,
            [FromBody]PaymentScheduleRequest paymentScheduleRequest)
        {
            try
            {
                return Ok(await LoanScheduleService.ScheduleMissed(loanNumber, id, paymentScheduleRequest));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, id, paymentScheduleRequest });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Select Loan Modification
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="loanModificationActivationRequest"></param>
        [HttpPost("{loanNumber}/loanmods/select")]
        [ProducesResponseType(typeof(ILoanSchedule), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> SelectLoanModification(string loanNumber,
            [FromBody]LoanModificationActivationRequest loanModificationActivationRequest)
        {
            try
            {
                return Ok(await LoanScheduleService.SelectLoanModification(loanNumber, loanModificationActivationRequest));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, loanModificationActivationRequest });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Send PayOff Schedule
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="version"></param>
        /// <param name="loanScheduleRequest"></param>
        [HttpPost("/{loanNumber}/{version}/payoffSchedule")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> SendPayOffSchedule(string loanNumber, string version,
            [FromBody]List<PayOffResponse> loanScheduleRequest)
        {
            try
            {
                await LoanScheduleService.SendPayOffSchedule(loanNumber, version, loanScheduleRequest);
                return NoContentResult;
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, version, loanScheduleRequest });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Set Active Bank
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="bankId"></param>
        [HttpPut("/bank/{loanNumber}/{bankId}/active")]
        [ProducesResponseType(typeof(IBankDetails), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> SetBankActive(string loanNumber, string bankId)
        {
            try
            {
                return Ok(await LoanOnboardingService.SetBankActive(loanNumber, bankId));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, bankId});
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Active Loan Modification
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="loanScheduleId"></param>
        [HttpPut("{loanNumber}/loanmods/{loanScheduleId}/activate")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> SetLoanModActive(string loanNumber, string loanScheduleId)
        {
            try
            {
                await LoanScheduleService.SetLoanModActive(loanNumber, loanScheduleId);
                return NoContentResult;
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, loanScheduleId });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Update Loan Applicant
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="applicantId"></param>
        /// <param name="applicantDetails"></param>
        [HttpPut("/applicant/{loanNumber}/{applicantId}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateApplicant(string loanNumber, string applicantId,
            [FromBody]ApplicantRequest applicantDetails)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdateApplicant(loanNumber, applicantId, applicantDetails));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, applicantId, applicantDetails });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Update Applicant Address
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="applicantId"></param>
        /// <param name="address"></param>
        [HttpPut("/applicant/address/{loanNumber}/{applicantId}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateAddress(string loanNumber, string applicantId, [FromBody]Address address)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdateAddress("applicant", loanNumber, applicantId, address));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, applicantId, address });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Update AutoPayment Detail
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="paymentDetail"></param>
        [HttpPut("{loanNumber}/autopay")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateAutoPayDetail(string loanNumber, [FromBody]AutoPayRequest paymentDetail)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdateAutoPayDetail(loanNumber, paymentDetail));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, paymentDetail });
                return ErrorResult.BadRequest(exception.Message);
            }

        }
        
        /// <summary>
        /// Update BankDetails
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="bankId"></param>
        /// <param name="bankDetails"></param>
        [HttpPut("/bank/{loanNumber}/{bankId}")]
        [ProducesResponseType(typeof(IBankDetails), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateBankDetails(string loanNumber, string bankId, [FromBody]BankDetails bankDetails)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdateBankDetails(loanNumber, bankId, bankDetails));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, bankId, bankDetails });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Update Business
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="businessId"></param>
        /// <param name="businessDetails"></param>
        [HttpPut("/business/{loanNumber}/{businessId}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateBusiness(string loanNumber, string businessId,
            [FromBody]BusinessDetails businessDetails)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdateBusiness(loanNumber, businessId, businessDetails));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, businessId, businessDetails });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Update Business Address
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="businessId"></param>
        /// <param name="address"></param>
        [HttpPut("/business/address/{loanNumber}/{businessId}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateBusinessAddress(string loanNumber, string businessId, [FromBody]Address address)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdateAddress("business", loanNumber, businessId, address));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, businessId, address });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Update Business Email
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="businessId"></param>
        /// <param name="email"></param>
        [HttpPut("/business/email/{loanNumber}/{businessId}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateBusinessEmail(string loanNumber, string businessId, [FromBody]Abstractions.Email email)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdateEmail("business", loanNumber, businessId, email));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, businessId, email });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Update Business Phone
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="businessId"></param>
        /// <param name="phone"></param>
        [HttpPut("/business/phone/{loanNumber}/{businessId}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateBusinessPhone(string loanNumber, string businessId, [FromBody]Phone phone)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdatePhone("business", loanNumber, businessId, phone));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, businessId, phone });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Update Collection Stage
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="collectionStage"></param>
        [HttpPut("{loanNumber}/collection")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateCollectionStage(string loanNumber, [FromBody] string collectionStage)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdateCollectionStage(loanNumber, collectionStage));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, collectionStage });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Update Applicant Email
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="applicantId"></param>
        /// <param name="email"></param>
        [HttpPut("/applicant/email/{loanNumber}/{applicantId}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateEmail(string loanNumber, string applicantId, [FromBody]Email email)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdateEmail("applicant", loanNumber, applicantId, email));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, applicantId, email });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Update Fee Details
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="id"></param>
        /// <param name="feeDetails"></param>
        [HttpPut("{loanNumber}/feedetails/{id}")]
        [ProducesResponseType(typeof(ILoanSchedule), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateFeeDetails(string loanNumber, string id, [FromBody]FeeDetails feeDetails)
        {
            try
            {
                return Ok(await LoanScheduleService.UpdateFeeDetails(loanNumber, id, feeDetails));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, id, feeDetails });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Update Loan Information
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="loanInformationRequest"></param>
        [HttpPut("/{loanNumber}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateLoanInformation(string loanNumber, [FromBody]LoanInformationRequest
            loanInformationRequest)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdateLoanInformation(loanNumber, loanInformationRequest));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, loanInformationRequest });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Update Loan Modification
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="loanScheduleId"></param>
        /// <param name="loanModificationActivationRequest"></param>
        [HttpPut("{loanNumber}/loanmods/update/{loanScheduleId}")]
        [ProducesResponseType(typeof(ILoanSchedule), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateLoanModification(string loanNumber, string loanScheduleId,
            [FromBody]LoanModificationActivationRequest loanModificationActivationRequest)
        {
            try
            {
                return Ok(await LoanScheduleService.UpdateLoanModification(loanNumber, loanScheduleId, loanModificationActivationRequest));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, loanScheduleId,
                    loanModificationActivationRequest });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Update LoanSchedule
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="scheduleId"></param>
        /// <param name="loanScheduleRequest"></param>
        [HttpPut("/schedule/{loanNumber}/{scheduleId}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateLoanSchedule(string loanNumber, string scheduleId, [FromBody]LoanScheduleRequest loanScheduleRequest)
        {
            try
            {
                return Ok(await LoanScheduleService.UpdateLoanSchedule(loanNumber, scheduleId, loanScheduleRequest));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, scheduleId, loanScheduleRequest });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Update Other Contact
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="otherContactId"></param>
        /// <param name="otherContact"></param>
        [HttpPut("{loanNumber}/othercontact/{otherContactId}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateOtherContact(string loanNumber, string otherContactId, [FromBody]OtherContact otherContact)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdateOtherContact(loanNumber, otherContactId, otherContact));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, otherContactId, otherContact });
                return ErrorResult.BadRequest(exception.Message);
            }

        }
        
        /// <summary>
        /// Update Paid Details
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="loanSchedule"></param>
        [HttpPut("{loanNumber}/paiddetails")]
        [ProducesResponseType(typeof(ILoanSchedule), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdatePaidDetails(string loanNumber, [FromBody]LoanSchedule loanSchedule)
        {
            try
            {
                return Ok(await LoanScheduleService.UpdatePaidDetails(loanNumber, loanSchedule));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, loanSchedule });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Update Payment Instrument
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="paymentInstrumentId"></param>
        /// <param name="paymentInstrument"></param>
        [HttpPut("/paymentinstrument/{loanNumber}/{paymentInstrumentId}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdatePaymentInstrument(string loanNumber, string paymentInstrumentId,
            [FromBody]PaymentInstrument paymentInstrument)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdatePaymentInstrument(loanNumber, paymentInstrumentId, paymentInstrument));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, paymentInstrumentId, paymentInstrument });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Update Applicant Phone
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="applicantId"></param>
        /// <param name="phone"></param>
        [HttpPut("/applicant/phone/{loanNumber}/{applicantId}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdatePhone(string loanNumber, string applicantId, [FromBody]Phone phone)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdatePhone("applicant", loanNumber, applicantId, phone));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, applicantId, phone });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Update Payment Details
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="id"></param>
        /// <param name="paymentScheduleRequest"></param>
        [HttpPut("{loanNumber}/scheduledetails/{id}")]
        [ProducesResponseType(typeof(ILoanSchedule), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdatePaymentDetails(string loanNumber, string id,
            [FromBody]PaymentScheduleRequest paymentScheduleRequest)
        {
            try
            {
                return Ok(await LoanScheduleService.UpdatePaymentDetails(loanNumber, id, paymentScheduleRequest));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, id, paymentScheduleRequest });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Update Renewal Details
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="renewalDetail"></param>
        [HttpPut("{loanNumber}/renewal")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateRenewalDetails(string loanNumber, [FromBody] UpdateRenewalDetailRequest renewalDetail)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdateRenewalDetails(loanNumber, renewalDetail));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, renewalDetail });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        #endregion

        #region Read APIs

        /// <summary>
        /// Get Bank Details
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="bankId"></param>
        [HttpGet("/bank/{loanNumber}/{bankId?}")]
        [ProducesResponseType(typeof(IBankDetails), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetBankDetails(string loanNumber, string bankId = null)
        {
            try
            {
                return Ok(await LoanOnboardingService.GetBankDetails(loanNumber, bankId));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, bankId });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get All Bank Details
        /// </summary>
        /// <param name="loanNumber"></param>
        [HttpGet("/all/bank/{loanNumber}")]
        [ProducesResponseType(typeof(List<IBankDetails>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllBankDetails(string loanNumber)
        {
            try
            {
                return Ok(await LoanOnboardingService.GetAllBankDetails(loanNumber));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber});
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get Schedule Detail by Loan version number between two dates
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="version"></param>
        /// <param name="processingDate"></param>
        [HttpPost("{loanNumber}/{version}/schedule")]
        [ProducesResponseType(typeof(List<ILoanScheduleDetails>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetScheduleDetail(string loanNumber, string version, [FromBody] ProcessingDateRequst processingDate)
        {
            try
            {
                return Ok(await LoanScheduleService.GetScheduleDetail(loanNumber, version, processingDate));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, version});
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get Schedule Detail By LoanNumber
        /// </summary>
        /// <param name="loanNumber"></param>
        [HttpGet("/schedule/{loanNumber}")]
        [ProducesResponseType(typeof(List<ILoanSchedule>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLoanScheduleByLoanNumber(string loanNumber)
        {
            try
            {
                return Ok(await LoanScheduleService.GetLoanScheduleByLoanNumber(loanNumber));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get PayOff Fee Discount Schedule
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="feeType"></param>
        [HttpGet("/payoffschedule/{loanNumber}/{feeType?}")]
        [ProducesResponseType(typeof(List<IPayOffFeeDiscount>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetPayOffFeeDiscountSchedule(string loanNumber, string feeType)
        {
            try
            {
                return Ok(await LoanScheduleService.GetPayOffFeeDiscountSchedule(loanNumber, feeType));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, feeType });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Calculate Installment Amount
        /// </summary>
        /// <param name="loanSchedule"></param>
        [HttpPost("paymentamount")]
        [ProducesResponseType(typeof(double), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetPaymentAmount([FromBody]LoanScheduleRequest loanSchedule)
        {
            try
            {
                return Ok(LoanScheduleService.GetPaymentAmount(loanSchedule));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanSchedule });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get Loan Modification Details
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="loanModificationRequest"></param>
        [HttpPost("{loanNumber}/loanmods")]
        [ProducesResponseType(typeof(List<ILoanScheduleDetails>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLoanModification(string loanNumber,
            [FromBody]LoanModificationRequest loanModificationRequest)
        {
            try
            {
                return Ok(await LoanScheduleService.GetLoanModification(loanNumber, loanModificationRequest));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, loanModificationRequest });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get Loan Information Between Onboarding dates
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        [HttpGet("/loans/{startdate}/{enddate}")]
        [ProducesResponseType(typeof(List<ILoanDetails>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLoanList(DateTime startDate, DateTime endDate)
        {
            try
            {
                return Ok(await LoanOnboardingService.GetLoanList(startDate, endDate));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { startDate, endDate });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get Related Loan Information for renewed loan
        /// </summary>
        /// <param name="loanNumber"></param>        
        [HttpGet("{loanNumber}/relatedloans")]
        [ProducesResponseType(typeof(List<RelatedLoanResponse>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetRelatedLoanInformation(string loanNumber)
        {
            try
            {
                return Ok(await LoanOnboardingService.GetRelatedLoanInformation(loanNumber));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber});
                return ErrorResult.BadRequest(exception.Message);
            }

        }
        
         /// <summary>
        /// Get Loan Information By ApplicationNumber
        /// </summary>
        /// <param name="loanReferenceNumber"></param>
        [HttpGet("/loan/{loanReferenceNumber}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLoanInformationByLoanReferenceNumber(string loanReferenceNumber)
        {
            try
            {
                return Ok(await LoanOnboardingService.GetLoanInformationByLoanReferenceNumber(loanReferenceNumber));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanReferenceNumber });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Get Loan Information by loanNumber
        /// </summary>
        /// <param name="loanNumber"></param>
        [HttpGet("/{loanNumber}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Get(string loanNumber)
        {
            try
            {
                return Ok(await LoanOnboardingService.GetLoanInformationByLoanNumber(loanNumber));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get Loan Information By ApplicationNumber
        /// </summary>
        /// <param name="applicationNumber"></param>
        [HttpGet("/application/{applicationNumber}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLoanInformationByApplicationNumber(string applicationNumber)
        {
            try
            {
                return Ok(await LoanOnboardingService.GetLoanInformationByApplicationNumber(applicationNumber));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { applicationNumber });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Get Current Loan Schedule Detail
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="version"></param>
        [HttpGet("/{loanNumber}/schedule/{version?}")]
        [ProducesResponseType(typeof(ILoanSchedule), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetCurrentLoanSchedule(string loanNumber, string version = null)
        {
            try
            {
                return Ok(await LoanScheduleService.GetLoanSchedule(loanNumber, version));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, version });
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        ///  Update invoice details
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="invoiceId"></param>
        /// <param name="invoiceDetails"></param>
        [HttpPut("/invoicedetails/{loanNumber}/{invoiceId?}")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateInvoiceDetails(string loanNumber, string invoiceId,[FromBody] List<LoanInvoiceDetails> invoiceDetails)
        {
            try
            {
                return Ok(await LoanOnboardingService.UpdateInvoiceDetails(loanNumber,invoiceId,invoiceDetails.ToList<ILoanInvoiceDetails>()));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber, invoiceDetails });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get loan schedules for all provided loan numbers
        /// </summary>
        /// <param name="loanNumbers"></param>
        /// <returns></returns>
        [HttpPost("/loan/schedules")]
        [ProducesResponseType(typeof(ILoanSchedule), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLoanSchedules([FromBody]List<string> loanNumbers)
        {
            try
            {
                return Ok(await LoanScheduleService.GetLoanSchedules(loanNumbers));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumbers });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get MinimumAmountDue Flag status based on application 
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <returns></returns>
        [HttpGet("/{applicationNumber}/minimumamountdue/flag")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetMADFlagStatus(string applicationNumber)
        {
            try
            {
                return Ok(await LoanOnboardingService.GetMADFlagStatus(applicationNumber));
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { applicationNumber });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Unwind the loan
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="request"></param>
        [HttpPost("{loanNumber}/unwind")]
        [ProducesResponseType(typeof(ILoanInformation), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Unwind(string loanNumber,[FromBody]UnwindRequest request)
        {
            try
            {
                await LoanOnboardingService.Unwind(loanNumber,request);
                return NoContentResult;
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception), new { loanNumber });
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Send Borrower Invite
        /// </summary>
        /// <param name="emailData"></param>
        [HttpPost("send/borrower/invite")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> SendBorrowerInvite([FromBody]BorrowerEmailData emailData)
        {
            try
            {
                 await LoanOnboardingService.SendBorrowerInvite(emailData);
                 return NoContentResult;
            }
            catch (Exception exception)
            {
                Logger.Error(FlattenException(exception));
                return ErrorResult.BadRequest(exception.Message);
            }
        }


        #endregion
        
        #region Private Methods
        
        /// <summary>
        /// Split Reasons
        /// </summary>
        /// <param name="reasons"></param>
        private static List<string> SplitReasons(string reasons)
        {
            return string.IsNullOrWhiteSpace(reasons)
                ? new List<string>()
                : reasons.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        private static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }
        
        #endregion
    }
}
