﻿using System;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LMS.LoanManagement.Abstractions;
using LendFoundry.Security.Encryption;

namespace LMS.LoanManagement.Persistence
{
    public class LoanOnboardingHistoryRepositoryFactory : ILoanOnboardingHistoryRepositoryFactory
    {
        public LoanOnboardingHistoryRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public ILoanOnboardingHistoryRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfigurationFactory =Provider.GetService<IMongoConfigurationFactory>();
            var configuration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            var encryptionService = Provider.GetService<IEncryptionService>();
            return new LoanOnboardingHistoryRepository(tenantService, configuration, encryptionService);
        }
    }
}
