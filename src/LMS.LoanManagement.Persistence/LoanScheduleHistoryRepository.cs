﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using LMS.LoanManagement.Abstractions;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Persistence
{
    public class LoanScheduleHistoryRepository : MongoRepository<ILoanScheduleHistory, LoanScheduleHistory>, ILoanScheduleHistoryRepository
    {
        static LoanScheduleHistoryRepository()
        {
            BsonClassMap.RegisterClassMap<LoanScheduleHistory>(map =>
            {
                map.AutoMap();
                var type = typeof(LoanScheduleHistory);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public LoanScheduleHistoryRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "loan-schedule-history")
        {
            CreateIndexIfNotExists("loan-number-index", Builders<ILoanScheduleHistory>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.LoanNumber));
            CreateIndexIfNotExists("schedule-version-index", Builders<ILoanScheduleHistory>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.LoanNumber).Ascending(i => i.ScheduleVersionNo));
        }

        public async Task AddHistory(ILoanScheduleHistory loanScheduleHistory)
        {
            if (string.IsNullOrWhiteSpace(loanScheduleHistory.UpdatedBy))
            {
                loanScheduleHistory.UpdatedBy = TenantService.Current.Email;
            }
            await Task.Run(() => Add(loanScheduleHistory));
        }

    }
}