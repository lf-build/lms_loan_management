﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;
using System.Linq;
using MongoDB.Driver;
using System.Collections.Generic;
using LMS.LoanManagement.Abstractions;
using LMS.Foundation.Amortization;

namespace LMS.LoanManagement.Persistence
{
    public class LoanScheduleRepository : MongoRepository<ILoanSchedule, LoanSchedule>, ILoanScheduleRepository
    {
        static LoanScheduleRepository()
        {
            BsonClassMap.RegisterClassMap<LoanSchedule>(map =>
            {
                map.AutoMap();
                var type = typeof(LoanSchedule);
                map.MapProperty(p => p.ModStatus).SetSerializer(new EnumSerializer<ModStatus>(BsonType.String));
                map.MapProperty(p => p.InterestAdjustmentType).SetSerializer(new EnumSerializer<InterestAdjustmentType>(BsonType.String));
                map.MapProperty(p => p.PaymentFrequency).SetSerializer(new EnumSerializer<PaymentFrequency>(BsonType.String));
                map.MapProperty(p => p.LoanModType).SetSerializer(new EnumSerializer<LoanModType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LoanScheduleDetails>(map =>
            {
                map.AutoMap();
                var type = typeof(LoanScheduleDetails);
                map.MapMember(m => m.ScheduleDate).SetSerializer(new DateTimeSerializer(true));
                map.MapMember(m => m.OriginalScheduleDate).SetSerializer(new DateTimeSerializer(true));
                map.MapProperty(p => p.InterestHandlingType).SetSerializer(new EnumSerializer<Foundation.Amortization.InterestHandlingType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LoanScheduleDetail>(map =>
            {
                map.AutoMap();
                var type = typeof(LoanScheduleDetail);
                map.MapMember(m => m.ScheduleDate).SetSerializer(new DateTimeSerializer(true));
                map.MapMember(m => m.OriginalScheduleDate).SetSerializer(new DateTimeSerializer(true));
                map.MapProperty(p => p.InterestHandlingType).SetSerializer(new EnumSerializer<Foundation.Amortization.InterestHandlingType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<Fee>(map =>
            {
                map.AutoMap();
                var type = typeof(Fee);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<FeeDetails>(map =>
            {
                map.AutoMap();
                var type = typeof(FeeDetails);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<PaybackTier>(map =>
            {
                map.AutoMap();
                var type = typeof(PaybackTier);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<RemainderDetails>(map =>
            {
                map.AutoMap();
                var type = typeof(RemainderDetails);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<PaymentStream>(map =>
            {
                map.AutoMap();
                var type = typeof(PaymentStream);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public LoanScheduleRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "loan-schedule")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(LoanScheduleDetails)))
            {
                BsonClassMap.RegisterClassMap<LoanScheduleDetails>();
            }
            CreateIndexIfNotExists("loan-number-index", Builders<ILoanSchedule>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.LoanNumber));
            CreateIndexIfNotExists("schedule-version-index", Builders<ILoanSchedule>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.LoanNumber).Ascending(i => i.ScheduleVersionNo));
            CreateIndexIfNotExists("schedule-id-index", Builders<ILoanSchedule>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.LoanNumber).Ascending(i => i.Id));
        }

        public async Task<List<ILoanSchedule>> GetLoanScheduleByLoanNumber(string loanNumber)
        {
            return await Query.Where(loanSchedule => loanSchedule.TenantId == TenantService.Current.Id && loanSchedule.LoanNumber == loanNumber).ToListAsync();
        }

        public async Task<ILoanSchedule> GetLoanScheduleByLoanNumberAndScheduleVersion(string loanNumber, string scheduleVersion)
        {
            return await Task.Run(() => Query.Where(loanSchedule => loanSchedule.TenantId == TenantService.Current.Id && loanSchedule.LoanNumber == loanNumber && loanSchedule.ScheduleVersionNo == scheduleVersion).FirstOrDefault());
        }

        public async Task<ILoanSchedule> GetLoanSchedule(string loanNumber, string version)
        {
            if (string.IsNullOrWhiteSpace(version))
            {
                return await Task.Run(() => Query.Where(loanSchedule => loanSchedule.TenantId == TenantService.Current.Id && loanSchedule.LoanNumber == loanNumber && loanSchedule.IsCurrent).FirstOrDefault());
            }
            return await Task.Run(() => Query.Where(loanSchedule => loanSchedule.TenantId == TenantService.Current.Id && loanSchedule.LoanNumber == loanNumber && loanSchedule.ScheduleVersionNo == version).FirstOrDefault());
        }

        public async Task<ILoanSchedule> GetLoanScheduleById(string loanNumber, string loanScheduleId)
        {
            return await Task.Run(() => Query.Where(loanSchedule => loanSchedule.TenantId == TenantService.Current.Id && loanSchedule.LoanNumber == loanNumber && loanSchedule.Id == loanScheduleId).FirstOrDefault());
        }

        public async Task<ILoanSchedule> UpdateLoanSchedule(string loanNumber, string scheduleId, ILoanSchedule loanSchedule)
        {
            if (string.IsNullOrWhiteSpace(loanSchedule.Id))
            {
                loanSchedule.Id = scheduleId;
            }
            await Collection.UpdateOneAsync(Builders<ILoanSchedule>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                       a.LoanNumber == loanNumber && a.Id == scheduleId),
                Builders<ILoanSchedule>.Update
                                .Set(a => a.AnnualRate, loanSchedule.AnnualRate)
                                .Set(a => a.CreditLimit, loanSchedule.CreditLimit)
                                .Set(a => a.DailyRate, loanSchedule.DailyRate)
                                .Set(a => a.FactorRate, loanSchedule.FactorRate)
                                .Set(a => a.FirstPaymentDate, loanSchedule.FirstPaymentDate)
                                .Set(a => a.FundedAmount, loanSchedule.FundedAmount)
                                .Set(a => a.FundedDate, loanSchedule.FundedDate)
                                .Set(a => a.IsCurrent, loanSchedule.IsCurrent)
                                .Set(a => a.LoanAmount, loanSchedule.LoanAmount)
                                .Set(a => a.LoanScheduleNumber, loanSchedule.LoanScheduleNumber)
                                .Set(a => a.PaymentFrequency, loanSchedule.PaymentFrequency)
                                .Set(a => a.PaybackTiers, loanSchedule.PaybackTiers)
                                .Set(a => a.PaymentFrequencyRate, loanSchedule.PaymentFrequencyRate)
                                .Set(a => a.ScheduledCreatedReason, loanSchedule.ScheduledCreatedReason)
                                .Set(a => a.ScheduleDetails, loanSchedule.ScheduleDetails)
                                .Set(a => a.ScheduleNotes, loanSchedule.ScheduleNotes)
                                .Set(a => a.ScheduleVersionNo, loanSchedule.ScheduleVersionNo)
                                .Set(a => a.ScheduleVersionDate, loanSchedule.ScheduleVersionDate)
                                .Set(a => a.Tenure, loanSchedule.Tenure)
                                .Set(a => a.TotalDrawDown, loanSchedule.TotalDrawDown)
                                .Set(a => a.Fees, loanSchedule.Fees)
                                .Set(a => a.FeeDetails, loanSchedule.FeeDetails)
                                .Set(a => a.ModReason, loanSchedule.ModReason)
                                .Set(a => a.RemainderDetails, loanSchedule.RemainderDetails)
                                .Set(a => a.ModStatus, loanSchedule.ModStatus)
                                .Set(a => a.PaymentFailure, loanSchedule.PaymentFailure)
                                .Set(a => a.NextEscalationStage, loanSchedule.NextEscalationStage)
                                .Set(a => a.InterestAdjustmentType, loanSchedule.InterestAdjustmentType)
                                .Set(a => a.LoanModType, loanSchedule.LoanModType)
                                .Set(a => a.PaymentStreams, loanSchedule.PaymentStreams)
                                .Set(a => a.InterestToRecover, loanSchedule.InterestToRecover)
                                );

            return loanSchedule;
        }

        public async Task<ILoanSchedule> UpdatePaidDetails(string loanNumber, ILoanSchedule loanSchedule)
        {
            await Collection.UpdateOneAsync(Builders<ILoanSchedule>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                       a.LoanNumber == loanNumber && a.Id == loanSchedule.Id),
                Builders<ILoanSchedule>.Update
                                .Set(a => a.ScheduleDetails, loanSchedule.ScheduleDetails)
                                );
            return loanSchedule;
        }

        public async Task<List<ILoanSchedule>> GetLoanSchedules(List<string> loanNumbers)
        {
            return await Query.Where(x => loanNumbers.Contains(x.LoanNumber) && x.IsCurrent).ToListAsync();
        }
    }
}