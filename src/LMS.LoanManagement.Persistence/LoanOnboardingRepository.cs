﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver.Linq;
using System.Linq;
using System.Threading.Tasks;
using System;
using MongoDB.Driver;
using LMS.LoanManagement.Abstractions;
using LendFoundry.Security.Encryption;
using System.Collections.Generic;
using LendFoundry.Foundation.Services;

namespace LMS.LoanManagement.Persistence
{
    public class LoanOnboardingRepository : MongoRepository<ILoanInformation, LoanInformation>,
        ILoanOnboardingRepository
    {
        static LoanOnboardingRepository()
        {
            BsonClassMap.RegisterClassMap<LoanInformation>(map =>
            {
                map.AutoMap();
                var type = typeof(LoanInformation);
                map.MapProperty(p => p.ProductPortfolioType)
                    .SetSerializer(new EnumSerializer<ProductPortfolioType>(BsonType.String));
                map.MapProperty(p => p.AutoPayType).SetSerializer(new EnumSerializer<AutoPayType>(BsonType.String));
                map.MapProperty(p => p.Currency).SetSerializer(new EnumSerializer<Currency>(BsonType.String));
                map.MapProperty(p => p.ProductCategory)
                    .SetSerializer(new EnumSerializer<ProductCategory>(BsonType.String));

                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<ApplicantDetails>(map =>
            {
                map.AutoMap();
                var type = typeof(ApplicantDetails);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LoanInvoiceDetails>(map =>
            {
                map.AutoMap();
                var type = typeof(LoanInvoiceDetails);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.MapMember(m => m.DateOfInvoice).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.DueDateOfInvoice).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
            });


            BsonClassMap.RegisterClassMap<DrawDownDetails>(map =>
            {
                map.AutoMap();
                var type = typeof(DrawDownDetails);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<Email>(map =>
            {
                map.AutoMap();
                var type = typeof(Email);
                map.MapProperty(p => p.EmailType).SetSerializer(new EnumSerializer<EmailType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<Address>(map =>
            {
                map.AutoMap();
                var type = typeof(Address);
                map.MapProperty(p => p.AddressType).SetSerializer(new EnumSerializer<AddressType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<Phone>(map =>
            {
                map.AutoMap();
                var type = typeof(Phone);
                map.MapProperty(p => p.PhoneType).SetSerializer(new EnumSerializer<PhoneType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<Name>(map =>
            {
                map.AutoMap();
                var type = typeof(Name);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<PaymentInstrument>(map =>
            {
                map.AutoMap();
                var type = typeof(PaymentInstrument);
                map.MapProperty(p => p.PaymentInstrumentType)
                    .SetSerializer(new EnumSerializer<PaymentInstrumentType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<OtherContact>(map =>
            {
                map.AutoMap();
                var type = typeof(OtherContact);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<OtherEmail>(map =>
            {
                map.AutoMap();
                var type = typeof(OtherEmail);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<OtherPhone>(map =>
            {
                map.AutoMap();
                var type = typeof(OtherPhone);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<OtherAddress>(map =>
            {
                map.AutoMap();
                var type = typeof(OtherAddress);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<CollectionDetail>(map =>
            {
                map.AutoMap();
                var type = typeof(CollectionDetail);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<AutoPayHistory>(map =>
            {
                map.AutoMap();
                var type = typeof(AutoPayHistory);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });


            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
        }

        public LoanOnboardingRepository(ITenantService tenantService, IMongoConfiguration configuration,
            IEncryptionService encrypterService)
            : base(tenantService, configuration, "loan-onboarding")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(PII)))
            {
                BsonClassMap.RegisterClassMap<PII>(map =>
                {
                    map.AutoMap();
                    map.MapMember(m => m.DOB).SetSerializer(new BsonEncryptor<DateTime, DateTime>(encrypterService));
                    map.MapMember(m => m.SSN).SetSerializer(new BsonEncryptor<string, string>(encrypterService));
                    var type = typeof(PII);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }

            if (!BsonClassMap.IsClassMapRegistered(typeof(BusinessDetails)))
            {
                BsonClassMap.RegisterClassMap<BusinessDetails>(map =>
                {
                    map.AutoMap();
                    map.MapMember(m => m.FedTaxId).SetSerializer(new BsonEncryptor<string, string>(encrypterService));
                    var type = typeof(BusinessDetails);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                });
            }

            if (!BsonClassMap.IsClassMapRegistered(typeof(Identifier)))
            {
                BsonClassMap.RegisterClassMap<Identifier>(map =>
                {
                    map.AutoMap();
                    var type = typeof(Identifier);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                });
            }

            CreateIndexIfNotExists("loan-number-index",
                Builders<ILoanInformation>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.LoanNumber));
            CreateIndexIfNotExists("app-number-index",
                Builders<ILoanInformation>.IndexKeys.Ascending(i => i.TenantId)
                    .Ascending(i => i.LoanApplicationNumber));
        }

        public async Task<ILoanInformation> GetLoanInformationByApplicationNumber(string applicationNumber)
        {
            return await Task.Run(() => Query.FirstOrDefaultAsync(loanInformation =>
                loanInformation.TenantId == TenantService.Current.Id &&
                loanInformation.LoanApplicationNumber == applicationNumber));
        }

        public async Task<ILoanInformation> GetLoanInformationByLoanNumber(string loanNumber)
        {
            return await Task.Run(() =>
                Query.Where(loanInformation => loanInformation.TenantId == TenantService.Current.Id &&
                                               loanInformation.LoanNumber.ToLower() == loanNumber.ToLower())
                    .FirstOrDefault());
        }

        public async Task<List<ILoanInformation>> GetRelatedLoanInformation(string loanNumber)
        {
            var currentLoanDetails = await GetLoanInformationByLoanNumber(loanNumber);
            var relatedLoans = await Task.Run(() => Query.Where(c =>
                    c.OriginalParentLoanNumber == currentLoanDetails.OriginalParentLoanNumber &&
                    c.LoanNumber != loanNumber)
                .ToList());
            //List<ILoanInformation> relatedLoans = new List<ILoanInformation>();
            // if(currentLoanDetails.OriginalParentLoanNumber == loanNumber)
            // {
            //     var loan =new RelatedLoanDetail();            
            //     relatedLoans =  await Task.Run(() => Query.Where(c =>c.OriginalParentLoanNumber == loanNumber && c.LoanNumber != loanNumber).ToList());
            //     loan.RelatedLoans =  relatedLoans;
            //     loan.RelationShip = "Child";
            //     RelatedLoanDetail.Add(loan);
            // }
            // else
            // {
            //     var parentLoan =new RelatedLoanDetail();         
            //     var parentLoans = await Task.Run(() => Query.Where(c =>c.LoanNumber == currentLoanDetails.ParentLoanNumber).ToList());
            //     parentLoan.RelatedLoans = parentLoans;
            //     parentLoan.RelationShip = "parent";
            //     RelatedLoanDetail.Add(parentLoan);
            //     var childLoan =new RelatedLoanDetail();         
            //     var childLoans = await Task.Run(() => Query.Where(c =>c.ParentLoanNumber == loanNumber).ToList());
            //     childLoan.RelatedLoans = childLoans;
            //     childLoan.RelationShip = "Child";
            //     RelatedLoanDetail.Add(childLoan);
            // }
            return relatedLoans;
        }

        public async Task<ILoanInformation> UpdateLoanInformation(string loanNumber, ILoanInformation loanInformation)
        {
            await Collection.UpdateOneAsync(Builders<ILoanInformation>.Filter.Where(a =>
                    a.TenantId == TenantService.Current.Id &&
                    a.LoanNumber == loanNumber),
                Builders<ILoanInformation>.Update
                    .Set(a => a.ApplicantDetails, loanInformation.ApplicantDetails)
                    .Set(a => a.BusinessDetails, loanInformation.BusinessDetails)
                    .Set(a => a.LoanApplicationNumber, loanInformation.LoanApplicationNumber)
                    .Set(a => a.LoanEndDate, loanInformation.LoanEndDate)
                    .Set(a => a.LoanFundedDate, loanInformation.LoanFundedDate)
                    .Set(a => a.LoanOnboardedDate, loanInformation.LoanOnboardedDate)
                    .Set(a => a.LoanProductId, loanInformation.LoanProductId)
                    .Set(a => a.LoanStartDate, loanInformation.LoanStartDate)
                    .Set(a => a.PrimaryApplicantDetails, loanInformation.PrimaryApplicantDetails)
                    .Set(a => a.PrimaryBusinessDetails, loanInformation.PrimaryBusinessDetails)
                    .Set(a => a.ProductPortfolioType, loanInformation.ProductPortfolioType)
                    .Set(a => a.DrawDownDetails, loanInformation.DrawDownDetails)
                    .Set(a => a.ProductCategory, loanInformation.ProductCategory)
                    .Set(a => a.PaymentInstruments, loanInformation.PaymentInstruments)
                    .Set(a => a.Currency, loanInformation.Currency)
                    .Set(a => a.PortFolioId, loanInformation.PortFolioId)
                    .Set(a => a.IsAutoPay, loanInformation.IsAutoPay)
                    .Set(a => a.AutoPayStartDate, loanInformation.AutoPayStartDate)
                    .Set(a => a.AutoPayStopDate, loanInformation.AutoPayStopDate)
                    .Set(a => a.AutoPayHistory, loanInformation.AutoPayHistory)
                    .Set(a => a.AutoPayType, loanInformation.AutoPayType)
                    .Set(a => a.OtherContacts, loanInformation.OtherContacts)
                    .Set(a => a.CollectionStage, loanInformation.CollectionStage)
            );

            return loanInformation;
        }

        public async Task<ILoanInformation> UpdateRenewalDetails(string loanNumber,
            UpdateRenewalDetailRequest renewalDetails)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Loan Information for {loanNumber} not found");

            loanInformation.IsRenewable = renewalDetails.IsRenewal;
            await Collection.UpdateOneAsync(Builders<ILoanInformation>.Filter.Where(a =>
                    a.TenantId == TenantService.Current.Id &&
                    a.LoanNumber == loanNumber),
                Builders<ILoanInformation>.Update
                    .Set(a => a.IsRenewable, renewalDetails.IsRenewal)
            );

            return loanInformation;
        }

        public async Task<ILoanInformation> UpdateAddress(string type, string loanNumber, string applicantId,
            IAddress address)
        {
            if (string.Equals(type, "business", StringComparison.InvariantCultureIgnoreCase))
            {
                return await GenericUpdateBusiness("address", loanNumber, applicantId, null, address);
            }

            return await GenericUpdateApplicant("address", loanNumber, applicantId, null, address);
        }

        public async Task<ILoanInformation> UpdatePhone(string type, string loanNumber, string applicantId,
            IPhone phone)
        {
            if (string.Equals(type, "business", StringComparison.InvariantCultureIgnoreCase))
            {
                return await GenericUpdateBusiness("phone", loanNumber, applicantId, null, null, phone);
            }

            return await GenericUpdateApplicant("phone", loanNumber, applicantId, null, null, phone);
        }

        public async Task<ILoanInformation> UpdateEmail(string type, string loanNumber, string applicantId,
            IEmail email)
        {
            if (string.Equals(type, "business", StringComparison.InvariantCultureIgnoreCase))
            {
                return await GenericUpdateBusiness("email", loanNumber, applicantId, email);
            }

            return await GenericUpdateApplicant("email", loanNumber, applicantId, email);
        }

        private async Task<ILoanInformation> GenericUpdateApplicant(string update, string loanNumber,
            string applicantId, IEmail email = null, IAddress address = null, IPhone phone = null)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Loan Information for {loanNumber} not found");

            IApplicantDetails applicant;
            if (loanInformation.PrimaryApplicantDetails.Id == applicantId)
            {
                applicant = loanInformation.PrimaryApplicantDetails;
            }
            else
            {
                applicant = loanInformation.ApplicantDetails.Where(a => a.Id == applicantId).FirstOrDefault();
            }

            if (applicant == null)
                throw new NotFoundException($"Applicant for {applicantId} not found");

            switch (update)
            {
                case "email":
                    if (email == null)
                    {
                        email = new Email();
                    }

                    if (string.IsNullOrWhiteSpace(email.Id))
                    {
                        email.Id = Guid.NewGuid().ToString("N");
                    }

                    applicant.Emails[0] = email;
                    break;
                case "address":
                    if (address == null)
                    {
                        address = new Address();
                    }

                    if (string.IsNullOrWhiteSpace(address.Id))
                    {
                        address.Id = Guid.NewGuid().ToString("N");
                    }

                    applicant.Addresses[0] = address;
                    break;
                case "phone":
                    if (phone == null)
                    {
                        phone = new Phone();
                    }

                    if (string.IsNullOrWhiteSpace(phone.Id))
                    {
                        phone.Id = Guid.NewGuid().ToString("N");
                    }

                    applicant.Phones[0] = phone;
                    break;
                default:
                    break;
            }

            if (loanInformation.PrimaryApplicantDetails.Id == applicantId)
            {
                await Collection.UpdateOneAsync(Builders<ILoanInformation>.Filter.Where(a =>
                        a.TenantId == TenantService.Current.Id &&
                        a.LoanNumber == loanNumber),
                    Builders<ILoanInformation>.Update.Set(a => a.PrimaryApplicantDetails, applicant));
            }
            else
            {
                await Collection.UpdateOneAsync(Builders<ILoanInformation>.Filter.Where(a =>
                        a.TenantId == TenantService.Current.Id &&
                        a.LoanNumber == loanNumber),
                    Builders<ILoanInformation>.Update.Set(a => a.ApplicantDetails, loanInformation.ApplicantDetails));
            }

            return loanInformation;
        }

        private async Task<ILoanInformation> GenericUpdateBusiness(string update, string loanNumber, string businessId,
            IEmail email = null, IAddress address = null, IPhone phone = null)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Loan Information for {loanNumber} not found");

            IBusinessDetails business;
            if (loanInformation.PrimaryBusinessDetails.Id == businessId)
            {
                business = loanInformation.PrimaryBusinessDetails;
            }
            else
            {
                business = loanInformation.BusinessDetails.Where(a => a.Id == businessId).FirstOrDefault();
            }

            if (business == null)
                throw new NotFoundException($"Business for {businessId} not found");

            switch (update)
            {
                case "email":
                    if (email == null)
                    {
                        email = new Email();
                    }

                    if (string.IsNullOrWhiteSpace(email.Id))
                    {
                        email.Id = Guid.NewGuid().ToString("N");
                    }

                    business.Email = email;
                    break;
                case "address":
                    if (address == null)
                    {
                        address = new Address();
                    }

                    if (string.IsNullOrWhiteSpace(address.Id))
                    {
                        address.Id = Guid.NewGuid().ToString("N");
                    }

                    business.Address = address;
                    break;
                case "phone":
                    if (phone == null)
                    {
                        phone = new Phone();
                    }

                    if (string.IsNullOrWhiteSpace(phone.Id))
                    {
                        phone.Id = Guid.NewGuid().ToString("N");
                    }

                    business.Phone = phone;
                    break;
                default:
                    break;
            }

            if (loanInformation.PrimaryBusinessDetails.Id == businessId)
            {
                await Collection.UpdateOneAsync(Builders<ILoanInformation>.Filter.Where(a =>
                        a.TenantId == TenantService.Current.Id &&
                        a.LoanNumber == loanNumber),
                    Builders<ILoanInformation>.Update.Set(a => a.PrimaryBusinessDetails, business));
            }
            else
            {
                await Collection.UpdateOneAsync(Builders<ILoanInformation>.Filter.Where(a =>
                        a.TenantId == TenantService.Current.Id &&
                        a.LoanNumber == loanNumber),
                    Builders<ILoanInformation>.Update.Set(a => a.BusinessDetails, loanInformation.BusinessDetails));
            }

            return loanInformation;
        }

        public async Task<ILoanInformation> UpdateApplicant(string loanNumber, string applicantId,
            IApplicantDetails applicantDetails)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Loan Information for {loanNumber} not found");

            IApplicantDetails applicant;
            if (loanInformation.PrimaryApplicantDetails.Id == applicantId)
            {
                applicant = loanInformation.PrimaryApplicantDetails;
            }
            else
            {
                applicant = loanInformation.ApplicantDetails.Where(a => a.Id == applicantId).FirstOrDefault();
            }

            if (applicant == null)
                throw new NotFoundException($"Applicant for {applicantId} not found");

            if (string.IsNullOrWhiteSpace(applicantDetails.Id))
            {
                applicantDetails.Id = applicantId;
            }

            if (loanInformation.PrimaryApplicantDetails.Id == applicantId)
            {
                loanInformation.PrimaryApplicantDetails = new ApplicantDetails(applicantDetails);

                await Collection.UpdateOneAsync(Builders<ILoanInformation>.Filter.Where(a =>
                        a.TenantId == TenantService.Current.Id &&
                        a.LoanNumber == loanNumber),
                    Builders<ILoanInformation>.Update.Set(a => a.PrimaryApplicantDetails,
                        loanInformation.PrimaryApplicantDetails));
            }
            else
            {
                loanInformation.ApplicantDetails.ForEach(c =>
                {
                    if (c.Id == applicantId)
                    {
                        c.Name = new Name(applicantDetails.Name);
                        c.Phones = applicantDetails.Phones;
                        c.Emails = applicantDetails.Emails;
                        c.PII = new PII(applicantDetails.PII);
                        c.Addresses = applicantDetails.Addresses;
                        c.PositionOfLoan = applicantDetails.PositionOfLoan;
                        c.Role = applicantDetails.Role;
                    }
                });

                await Collection.UpdateOneAsync(Builders<ILoanInformation>.Filter.Where(a =>
                        a.TenantId == TenantService.Current.Id &&
                        a.LoanNumber == loanNumber),
                    Builders<ILoanInformation>.Update.Set(a => a.ApplicantDetails, loanInformation.ApplicantDetails));
            }

            return loanInformation;
        }

        public async Task<ILoanInformation> UpdateBusiness(string loanNumber, string businessId,
            IBusinessDetails businessDetails)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Loan Information for {loanNumber} not found");

            IBusinessDetails business;
            if (loanInformation.PrimaryBusinessDetails.Id == businessId)
            {
                business = loanInformation.PrimaryBusinessDetails;
            }
            else
            {
                business = loanInformation.BusinessDetails.Where(a => a.Id == businessId).FirstOrDefault();
            }

            if (business == null)
                throw new NotFoundException($"Business for {businessId} not found");

            if (string.IsNullOrWhiteSpace(businessDetails.Id))
            {
                businessDetails.Id = businessId;
            }

            if (loanInformation.PrimaryBusinessDetails.Id == businessId)
            {
                loanInformation.PrimaryBusinessDetails = new BusinessDetails(businessDetails);
                await Collection.UpdateOneAsync(Builders<ILoanInformation>.Filter.Where(a =>
                        a.TenantId == TenantService.Current.Id &&
                        a.LoanNumber == loanNumber),
                    Builders<ILoanInformation>.Update.Set(a => a.PrimaryBusinessDetails,
                        loanInformation.PrimaryBusinessDetails));
            }
            else
            {
                loanInformation.BusinessDetails.ForEach(c =>
                {
                    if (c.Id == businessId)
                    {
                        c.BusinessName = businessDetails.BusinessName;
                        c.Phone = new Phone(businessDetails.Phone);
                        c.Email = new Email(businessDetails.Email);
                        c.FedTaxId = businessDetails.FedTaxId;
                        c.Address = new Address(businessDetails.Address);
                        c.DBA = businessDetails.DBA;
                    }
                });
                await Collection.UpdateOneAsync(Builders<ILoanInformation>.Filter.Where(a =>
                        a.TenantId == TenantService.Current.Id &&
                        a.LoanNumber == loanNumber),
                    Builders<ILoanInformation>.Update.Set(a => a.BusinessDetails, loanInformation.BusinessDetails));
            }

            return loanInformation;
        }

        public async Task<ILoanInformation> UpdatePaymentInstrument(string loanNumber, string paymentInstrumentId,
            IPaymentInstrument paymentInstrument)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Loan Information for {loanNumber} not found");

            IPaymentInstrument paymentInstrumentDetails = loanInformation.PaymentInstruments
                .Where(a => a.Id == paymentInstrumentId).FirstOrDefault();

            if (paymentInstrumentDetails == null)
                throw new NotFoundException($"Payment instrument for {paymentInstrumentId} not found");

            if (string.IsNullOrWhiteSpace(paymentInstrument.Id))
            {
                paymentInstrumentDetails.Id = paymentInstrumentId;
            }

            loanInformation.PaymentInstruments.ForEach(c =>
            {
                if (c.Id == paymentInstrumentId)
                {
                    c.IsActive = paymentInstrument.IsActive;
                    c.PaymentInstrumentType = paymentInstrument.PaymentInstrumentType;
                }
            });

            await Collection.UpdateOneAsync(Builders<ILoanInformation>.Filter.Where(a =>
                    a.TenantId == TenantService.Current.Id &&
                    a.LoanNumber == loanNumber),
                Builders<ILoanInformation>.Update.Set(a => a.PaymentInstruments, loanInformation.PaymentInstruments));

            return loanInformation;
        }

        public async Task<ILoanInformation> UpdateOtherContact(string loanNumber, string otherContactId,
            IOtherContact otherContact)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Loan Information for {loanNumber} not found");

            var otherContacts = loanInformation.OtherContacts.Where(a => a.Id == otherContactId).FirstOrDefault();

            if (otherContacts == null)
                throw new NotFoundException($"Contact details for {otherContactId} not found");

            if (string.IsNullOrWhiteSpace(otherContact.Id))
            {
                otherContact.Id = otherContactId;
            }

            if (otherContact.Address != null && otherContact.Address.Any())
                otherContact.Address.ForEach(a => a.Id = Guid.NewGuid().ToString("N"));
            if (otherContact.Phone != null && otherContact.Phone.Any())
                otherContact.Phone.ForEach(p => p.Id = Guid.NewGuid().ToString("N"));
            if (otherContact.Email != null && otherContact.Email.Any())
                otherContact.Email.ForEach(e => e.Id = Guid.NewGuid().ToString("N"));
            if (otherContact.CollectionDetails != null && otherContact.CollectionDetails.Any())
                otherContact.CollectionDetails.ForEach(e => e.Id = Guid.NewGuid().ToString("N"));

            loanInformation.OtherContacts.ForEach(c =>
            {
                if (c.Id == otherContactId)
                {
                    c.Address = otherContact.Address;
                    c.Email = otherContact.Email;
                    c.Phone = otherContact.Phone;
                    c.CollectionDetails = otherContact.CollectionDetails;
                }
            });

            await Collection.UpdateOneAsync(Builders<ILoanInformation>.Filter.Where(a =>
                    a.TenantId == TenantService.Current.Id &&
                    a.LoanNumber == loanNumber),
                Builders<ILoanInformation>.Update.Set(a => a.OtherContacts, loanInformation.OtherContacts));

            return loanInformation;
        }

        public async Task<ILoanInformation> AddApplicant(string loanApplicationNumber,
            List<IApplicantDetails> applicantDetails)
        {
            var loanInformation = await GetLoanInformationByApplicationNumber(loanApplicationNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Loan Information for {loanApplicationNumber} not found");

            foreach (var item in applicantDetails)
            {
                loanInformation.ApplicantDetails.Add(item);
            }

            await Collection.UpdateOneAsync(Builders<ILoanInformation>.Filter.Where(a =>
                    a.TenantId == TenantService.Current.Id &&
                    a.LoanNumber == loanInformation.LoanNumber),
                Builders<ILoanInformation>.Update.Set(a => a.ApplicantDetails, loanInformation.ApplicantDetails));

            return loanInformation;
        }

        public async Task<List<ILoanInformation>> GetLoanInformationListByOnboardingDate(DateTime startDate,
            DateTime endDate)
        {
            List<ILoanInformation> loaninfos = new List<ILoanInformation>();
            var details = await Query.Where(loanInformation => loanInformation.TenantId == TenantService.Current.Id)
                .ToListAsync();

            foreach (var item in details)
            {
                if (item.LoanOnboardedDate.Time.Date >= startDate && item.LoanOnboardedDate.Time.Date <= endDate)
                    loaninfos.Add(item);
            }

            return loaninfos;
        }

        public async Task<ILoanInformation> GetLoanInformationByLoanReferenceNumber(string loanReferenceNumber)
        {
            return await Task.Run(() =>
                Query.Where(loanInformation => loanInformation.TenantId == TenantService.Current.Id &&
                                               loanInformation.LoanReferenceNumber.ToLower() ==
                                               loanReferenceNumber.ToLower()).FirstOrDefault());
        }

        public async Task<ILoanInformation> UpdateInvoiceDetails(string loanNumber, string invoiceId,
            List<ILoanInvoiceDetails> invoiceDetails)
        {
            // validateInput
            var isNew = string.IsNullOrEmpty(invoiceId);
            if (!isNew && (invoiceDetails == null || !invoiceDetails.Exists(i => i.Id == invoiceId)))
            {
                throw new NotFoundException($"Invoice Details not found for loan number {loanNumber}" +
                                            $"invoice id {invoiceId}. Therefore the invoice details for this loan cannot be updated.");
            }

            // getData
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            // validateData
            if (loanInformation == null)
            {
                throw new NotFoundException(
                    $"Loan Information for {loanNumber} not found therefore the invoice details for this loan cannot be updated.");
            }

            // prepareData
            switch (isNew)
            {
                case true:
                    loanInformation.InvoiceDetails = new List<ILoanInvoiceDetails>();
                    foreach (var invoice in invoiceDetails)
                    {
                        loanInformation.InvoiceDetails.Add(invoice);
                    }

                    break;

                case false:
                default:
                    var invoiceData = invoiceDetails.FirstOrDefault(i => i.Id == invoiceId);
                    loanInformation.InvoiceDetails.ForEach(i =>
                    {
                        if (i.Id == invoiceId)
                        {
                            // we already made sure invoiceData cannot be null in validateInput
                            // so don't worry about the possible null reference exception here
                            i.InvoiceAmount = invoiceData.InvoiceAmount;
                            i.IssuerName = invoiceData.IssuerName;
                            i.CreditDaysOfInvoice = invoiceData.CreditDaysOfInvoice;
                            i.DateOfInvoice = invoiceData.DateOfInvoice;
                            i.DueDateOfInvoice = invoiceData.DueDateOfInvoice;
                            i.InvoiceID = invoiceData.InvoiceID;
                        }
                    });
                    break;
            }

            // updateData
            await Collection.UpdateOneAsync(Builders<ILoanInformation>.Filter.Where(a =>
                    a.TenantId == TenantService.Current.Id &&
                    a.LoanNumber == loanNumber),
                Builders<ILoanInformation>.Update.Set(a => a.InvoiceDetails, loanInformation.InvoiceDetails));

            return loanInformation;
        }
    }
}