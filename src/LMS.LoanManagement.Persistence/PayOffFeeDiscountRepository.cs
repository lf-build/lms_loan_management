﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using LMS.LoanManagement.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;

namespace LMS.LoanManagement.Persistence
{
    public class PayOffFeeDiscountRepository : MongoRepository<IPayOffFeeDiscount, PayOffFeeDiscount>, IPayOffFeeDiscountRepository
    {
        static PayOffFeeDiscountRepository()
        {
            BsonClassMap.RegisterClassMap<PayOffFeeScheduleDetails>(map =>
            {
                map.AutoMap();
                var type = typeof(PayOffFeeScheduleDetails);
                map.MapMember(m => m.PayOffFromDate).SetSerializer(new DateTimeSerializer(true));
                map.MapMember(m => m.PayOffToDate).SetSerializer(new DateTimeSerializer(true));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });


        }

        public PayOffFeeDiscountRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "payoff-schedule")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(PayOffFeeScheduleDetails)))
            {
                BsonClassMap.RegisterClassMap<PayOffFeeScheduleDetails>();
            }
            CreateIndexIfNotExists("loan-number-index", Builders<IPayOffFeeDiscount>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.LoanNumber));
            CreateIndexIfNotExists("schedule-version-index", Builders<IPayOffFeeDiscount>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.LoanNumber));
        }

        public async Task<List<IPayOffFeeDiscount>> GetPayOffFeeDiscountSchedule(string loanNumber,  string feeType = null)
        {
            if (string.IsNullOrWhiteSpace(feeType))
                return await Task.Run(() => Query.Where(payoff => payoff.LoanNumber == loanNumber).ToListAsync());
            else
                return await Task.Run(() => Query.Where(payoff => payoff.LoanNumber == loanNumber && payoff.FeeType == feeType).ToListAsync());
        }


    }
}