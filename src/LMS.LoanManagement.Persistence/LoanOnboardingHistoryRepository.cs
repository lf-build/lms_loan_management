﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using LMS.LoanManagement.Abstractions;
using LendFoundry.Security.Encryption;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace LMS.LoanManagement.Persistence
{
    public class LoanOnboardingHistoryRepository : MongoRepository<ILoanInformationHistory, LoanInformationHistory>, ILoanOnboardingHistoryRepository
    {
        static LoanOnboardingHistoryRepository()
        {
            BsonClassMap.RegisterClassMap<LoanInformationHistory>(map =>
            {
                map.AutoMap();
                var type = typeof(LoanInformationHistory);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public LoanOnboardingHistoryRepository(ITenantService tenantService, IMongoConfiguration configuration, IEncryptionService encrypterService)
            : base(tenantService, configuration, "loan-onboarding-history")
        {
            CreateIndexIfNotExists("loan-number-index", Builders<ILoanInformationHistory>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.LoanNumber));
            CreateIndexIfNotExists("app-number-index", Builders<ILoanInformationHistory>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.LoanApplicationNumber));
        }

        public async Task AddHistory(ILoanInformationHistory loanInformationHistory)
        {
            if (string.IsNullOrWhiteSpace(loanInformationHistory.UpdatedBy))
            {
                loanInformationHistory.UpdatedBy = TenantService.Current.Email;
            }
            await Task.Run(() => Add(loanInformationHistory));
        }
    }
}