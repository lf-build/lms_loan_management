﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;
using MongoDB.Driver;
using System.Collections.Generic;
using LMS.LoanManagement.Abstractions;
using LendFoundry.Security.Encryption;

namespace LMS.LoanManagement.Persistence
{
    public class BankDetailsRepository : MongoRepository<IBankDetails, BankDetails>, IBankDetailsRepository
    {
        static BankDetailsRepository()
        {
        }

        public BankDetailsRepository(ITenantService tenantService, IMongoConfiguration configuration, IEncryptionService encrypterService)
            : base(tenantService, configuration, "bank-details")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(BankDetails)))
            {
                BsonClassMap.RegisterClassMap<BankDetails>(map =>
                {
                    map.AutoMap();
                    map.MapProperty(p => p.AccountType).SetSerializer(new EnumSerializer<AccountType>(BsonType.String));
                    map.MapMember(m => m.AccountNumber).SetSerializer(new BsonEncryptor<string, string>(encrypterService));
                    var type = typeof(BankDetails);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }
            CreateIndexIfNotExists("loan-number-index", Builders<IBankDetails>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.LoanNumber));
            CreateIndexIfNotExists("bank-id-index", Builders<IBankDetails>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.LoanNumber).Ascending(i => i.BankId));
        }

        public async Task<IBankDetails> GetBankDetails(string loanNumber, string bankId)
        {
            if (!string.IsNullOrWhiteSpace(bankId))
            {
                return await Task.Run(() => Query.FirstOrDefaultAsync(bank => bank.LoanNumber == loanNumber && bank.Id == bankId));
            }
            return await Task.Run(() => Query.FirstOrDefaultAsync(bank => bank.LoanNumber == loanNumber && bank.IsActive));
        }

        public async Task<List<IBankDetails>> GetAllBankDetails(string loanNumber)
        {
            return await Task.Run(() => Query.Where(bank => bank.LoanNumber == loanNumber).ToListAsync());
        }

        public async Task<IBankDetails> UpdateBankDetails(string loanNumber, string bankId, IBankDetails bankDetails)
        {
            await Collection.UpdateOneAsync(Builders<IBankDetails>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                         a.LoanNumber == loanNumber && a.Id == bankId),
                Builders<IBankDetails>.Update
                                  .Set(a => a.AccountNumber, bankDetails.AccountNumber)
                                  .Set(a => a.AccountType, bankDetails.AccountType)
                                  .Set(a => a.BankHolderName, bankDetails.BankHolderName)
                                  .Set(a => a.IsActive, bankDetails.IsActive)
                                  .Set(a => a.LoanNumber, bankDetails.LoanNumber)
                                  .Set(a => a.Name, bankDetails.Name)
                                  .Set(a => a.RoutingNumber, bankDetails.RoutingNumber)
                                  );
            return bankDetails;
        }
        public async Task<List<IBankDetails>> GetAllBankList()
        {
            return await Task.Run(() => Query.Where(bank => bank.TenantId == TenantService.Current.Id).ToListAsync());
        }



    }
}