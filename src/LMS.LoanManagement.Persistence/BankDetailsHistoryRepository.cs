﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Encryption;
using LendFoundry.Tenant.Client;
using LMS.LoanManagement.Abstractions;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace LMS.LoanManagement.Persistence
{
    public class BankDetailsHistoryRepository : MongoRepository<IBankDetailsHistory, BankDetailsHistory>, IBankDetailsHistoryRepository
    {
        static BankDetailsHistoryRepository()
        {
        }

        public BankDetailsHistoryRepository(ITenantService tenantService, IMongoConfiguration configuration, IEncryptionService encrypterService)
            : base(tenantService, configuration, "bank-details-history")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(BankDetailsHistory)))
            {
                BsonClassMap.RegisterClassMap<BankDetailsHistory>(map =>
                {
                    map.AutoMap();
                    map.MapProperty(p => p.AccountType).SetSerializer(new EnumSerializer<AccountType>(BsonType.String));
                    map.MapMember(m => m.AccountNumber).SetSerializer(new BsonEncryptor<string, string>(encrypterService));
                    var type = typeof(BankDetails);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }
            CreateIndexIfNotExists("loan-number-index", Builders<IBankDetailsHistory>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.LoanNumber));
            CreateIndexIfNotExists("bank-id-index", Builders<IBankDetailsHistory>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.LoanNumber).Ascending(i => i.BankId));
        }

        public async Task AddHistory(IBankDetailsHistory bankHistory)
        {
            if (string.IsNullOrWhiteSpace(bankHistory.UpdatedBy))
            {
                bankHistory.UpdatedBy = TenantService.Current.Email;
            }
            await Task.Run(() => Add(bankHistory));
        }

    }
}