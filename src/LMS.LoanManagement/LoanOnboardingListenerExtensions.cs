﻿using LMS.LoanManagement.Abstractions;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif

namespace LMS.LoanManagement
{
    public static class LoanOnboardingListenerExtensions
    {
        public static void UseLoanOnboardingListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<ILoanOnboardingListener>().Start();
        }
    }
}
