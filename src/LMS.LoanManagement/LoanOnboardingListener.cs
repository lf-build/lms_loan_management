﻿using System;
using LendFoundry.Security.Tokens;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Tenant.Client;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LMS.LoanManagement.Abstractions;
using System.Linq;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using LendFoundry.EventHub;
using LendFoundry.Clients.DecisionEngine;

namespace LMS.LoanManagement
{
    public class LoanOnboardingListener : ILoanOnboardingListener
    {
        #region Constructor

        public LoanOnboardingListener(IConfigurationServiceFactory<LoanConfiguration> apiConfigurationFactory,
                                        IConfigurationServiceFactory configurationFactory,
                                        ILoanOnboardingRepositoryFactory loanOnboardingRepositoryFactory,
                                        ILoanOnboardingServiceFactory loanOnboardingServiceFactory,
                                        ILoanScheduleServiceFactory loanScheduleServiceFactory,
                                        ITokenHandler tokenHandler,
                                        ILoggerFactory loggerFactory,
                                        IEventHubClientFactory eventHubFactory,
                                        ITenantServiceFactory tenantServiceFactory,
                                        ITenantTimeFactory tenantTimeFactory,
                                        IDecisionEngineClientFactory decisionEngineClientFactory)
        {
            ApiConfigurationFactory = apiConfigurationFactory;
            ConfigurationFactory = configurationFactory;
            LoanOnboardingServiceFactory = loanOnboardingServiceFactory;
            LoanScheduleServiceFactory = loanScheduleServiceFactory;
            LoanOnboardingRepositoryFactory = loanOnboardingRepositoryFactory;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            TenantTimeFactory = tenantTimeFactory;
            TenantServiceFactory = tenantServiceFactory;
            EventHubFactory = eventHubFactory;
            DecisionEngineClientFactory=decisionEngineClientFactory;
        }

        #endregion

        #region Variables

        private ILoanOnboardingRepositoryFactory LoanOnboardingRepositoryFactory { get; set; }
        private ILoanOnboardingServiceFactory LoanOnboardingServiceFactory { get; set; }
        private ILoanScheduleServiceFactory LoanScheduleServiceFactory { get; set; }
        private IConfigurationServiceFactory<LoanConfiguration> ApiConfigurationFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IDecisionEngineClientFactory DecisionEngineClientFactory{ get; }

        #endregion

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info($"Event hub listener started");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an event hub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    try
                    {
                        logger.Info($"Initializing event hub for tenant #{tenant.Id}");
                        // Tenant token creation         
                        var token = TokenHandler.Issue(tenant.Id, Abstractions.Settings.ServiceName, null, "system", null);
                        var reader = new StaticTokenReader(token.Value);

                        // Needed resources for this operation
                        var configuration = ApiConfigurationFactory.Create(reader).Get();
                        if (configuration == null){
                            logger.Warn($"Configuration not found for tenant #{tenant.Id}");
                            return;
                        }
                        if (configuration.Events == null)
                        {
                            logger.Info($"No events for tenant #{tenant.Id}");
                            return;
                        }

                        var hub = EventHubFactory.Create(reader);
                        var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
                        var repository = LoanOnboardingRepositoryFactory.Create(reader);
                        var loanOnboardingService = LoanOnboardingServiceFactory.Create(reader, TokenHandler, logger);
                        var loanScheduleService = LoanScheduleServiceFactory.Create(reader, TokenHandler, logger);
                        var decisionEngine = DecisionEngineClientFactory.Create(reader);
                        // Attach all configured events to be listen
                        configuration
                            .Events
                            .ToList()
                            .ForEach(eventConfig =>
                                {
                                    hub.On(eventConfig.Name, AddView(eventConfig, repository, logger, tenantTime, hub, configuration, loanOnboardingService, loanScheduleService,decisionEngine));
                                    logger.Info($"Event #{eventConfig.Name} attached to listener");
                                });
                        hub.StartAsync();
                    } catch(Exception e){
                        logger.Error ($"Error while processing tenant #{tenant.Id}", e);
                    }
                });

            }
            catch (Exception ex)
            {
                logger.Error("Error while listening event hub", ex);

            }
        }

        private static Action<EventInfo> AddView
        (
            EventMapping eventConfiguration,
            ILoanOnboardingRepository repository,
            ILogger logger,
            ITenantTime tenantTime,
            IEventHubClient eventHubService,
            LoanConfiguration configuration,
            ILoanOnboardingService loanService,
            ILoanScheduleService scheduleService,
            IDecisionEngineService decisionEngine
        )
        {
            return async @event =>
            {
                try
                {
                    var loanNumber = eventConfiguration.LoanNumber.FormatWith(@event);

                    var raisedEvent = configuration.Events.Where(e => e.Name == eventConfiguration.Name).FirstOrDefault();
                    if (raisedEvent == null)
                        logger.Error($"Invalid event name, not matched with events configured.");
                    else
                    {
                        if (raisedEvent.IsStatusChange)
                        {
                            var status = raisedEvent.Name;
                            await loanService.ChangeStatus(loanNumber, status);
                            logger.Info($"Changed status to {status} for {loanNumber}");
                        }
                        else
                        {
                             var response = eventConfiguration.Response.FormatWith(@event);
                            // Get active schedule.
                            var loanSchedule = await scheduleService.GetLoanSchedule(loanNumber);

                            if (loanSchedule == null)
                                return;
                            else
                            {
                                // Check next escalation stage.
                                logger.Info($"EvenName:{@event.Name} , LoanNumber: {loanNumber}");                   
                                 //   var payload = new { Loan = loanSchedule };
                                //    var executionResult = decisionEngine.Execute<dynamic, dynamic>("GetMissPaymentCount", new { payload });
                                if (!string.IsNullOrWhiteSpace(loanSchedule.NextEscalationStage))
                                {
                                    // If payment failure matches with count then update collection stage.
                                    if (Convert.ToInt32(response) == loanSchedule.PaymentFailure)
                                    {
                                        await loanService.UpdateCollectionStage(loanNumber, loanSchedule.NextEscalationStage);
                                    }
                                }
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error($"Unhandled exception while listening event {@event.Name}", ex);
                }
            };
        }
    }
}
