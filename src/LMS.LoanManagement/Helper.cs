namespace LMS.LoanManagement
{
    public class Helper
    {
        // TODO This can be different depending on the currency.
        // This should ideally be getCcyLeastCount(string ccyCode)
        const double CcyLeastCount = 0.01;

        /// <summary>
        /// returns true of amount a1 equals amount a2
        /// </summary>
        /// <param name="a1"></param>
        /// <param name="a2"></param>
        /// <param name="ccyLeastCount"></param>
        /// <returns></returns>
        public static bool IsAmtEqual(double a1, double a2, double ccyLeastCount = CcyLeastCount)
        {
            return a1 - a2 < ccyLeastCount;
        }
        
    }
}
