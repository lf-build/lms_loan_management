﻿using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.NumberGenerator.Client;
using LendFoundry.EventHub;
using LendFoundry.StatusManagement.Client;
using LendFoundry.ProductConfiguration.Client;
using LMS.LoanManagement.Abstractions;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.DataAttributes.Client;
using LMS.LoanAccounting;
using LMS.Payment.Processor.Client;

namespace LMS.LoanManagement
{
    public class LoanOnboardingServiceFactory : ILoanOnboardingServiceFactory
    {
        public LoanOnboardingServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IServiceProvider Provider { get; }

        public ILoanOnboardingService Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {
            var loanOnboardingRepositoryService = Provider.GetService<ILoanOnboardingRepositoryFactory>();
            var loanOnboardingRepository = loanOnboardingRepositoryService.Create(reader);

            var bankDetailsRepositoryService = Provider.GetService<IBankDetailsRepositoryFactory>();
            var bankDetailsRepository = bankDetailsRepositoryService.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var loanConfigurationService = configurationServiceFactory.Create<LoanConfiguration>(Abstractions.Settings.ServiceName, reader);
            var loanConfiguration = loanConfigurationService.Get();

            var numberGeneratorServiceFactory = Provider.GetService<INumberGeneratorServiceFactory>();
            var numberGeneratorService = numberGeneratorServiceFactory.Create(reader);

            var loanScheduleServiceFactory = Provider.GetService<ILoanScheduleServiceFactory>();
            var loanScheduleService = loanScheduleServiceFactory.Create(reader, handler, logger);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);

            var statusManagementServiceFactory = Provider.GetService<IStatusManagementServiceFactory>();
            var statusManagementService = statusManagementServiceFactory.Create(reader);

            var productServiceFactory = Provider.GetService<IProductServiceFactory>();
            var productService = productServiceFactory.Create(reader);

            var bankDetailsHistoryRepositoryFactory = Provider.GetService<IBankDetailsHistoryRepositoryFactory>();
            var bankDetailsHistoryRepository = bankDetailsHistoryRepositoryFactory.Create(reader);

            var loanOnboardingHistoryRepositoryFactory = Provider.GetService<ILoanOnboardingHistoryRepositoryFactory>();
            var loanOnboardingHistoryRepository = loanOnboardingHistoryRepositoryFactory.Create(reader);

            var lookupServiceFactory = Provider.GetService<ILookupClientFactory>();
            var lookupService = lookupServiceFactory.Create(reader);

            var dataAttributesServiceFactory = Provider.GetService<IDataAttributesClientFactory>();
            var dataAttributesService = dataAttributesServiceFactory.Create(reader);

            var accrualServiceFactory = Provider.GetService<IAccrualBalanceClientFactory>();
            var accrualService = accrualServiceFactory.Create(reader);

            var loanscheduleRepositoryService = Provider.GetService<ILoanScheduleRepositoryFactory>();
            var loanscheduleRepository = loanscheduleRepositoryService.Create(reader);
            

            return new LoanOnboardingService(loanConfiguration, loanScheduleService, accrualService, loanOnboardingRepository, bankDetailsRepository, numberGeneratorService, statusManagementService, productService, eventHub, tenantTime, bankDetailsHistoryRepository, loanOnboardingHistoryRepository, lookupService, dataAttributesService, loanscheduleRepository);
        }
    }
}
