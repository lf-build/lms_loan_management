﻿using System;
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LMS.Foundation.Amortization;
using LendFoundry.StatusManagement.Client;
using LMS.LoanAccounting;
using LendFoundry.EventHub;
using LendFoundry.ProductConfiguration.Client;
using LMS.LoanManagement.Abstractions;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.ProductRule.Client;
using LendFoundry.Calendar.Client;
using LMS.Payment.Processor.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.MoneyMovement.Ach;
using LendFoundry.MoneyMovement.Ach.Client;

namespace LMS.LoanManagement
{
    public class LoanScheduleServiceFactory : ILoanScheduleServiceFactory
    {
        public LoanScheduleServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IServiceProvider Provider { get; }

        public ILoanScheduleService Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {
            var loanScheduleRepositoryService = Provider.GetService<ILoanScheduleRepositoryFactory>();
            var loanScheduleRepository = loanScheduleRepositoryService.Create(reader);

            var loanOnBoardingRepositoryService = Provider.GetService<ILoanOnboardingRepositoryFactory>();
            var loanOnBoardingRepository = loanOnBoardingRepositoryService.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var loanAmortizationFactory = Provider.GetService<ILoanAmortizationFactory>();
            var loanAmortizeService = loanAmortizationFactory.Create(reader);

            var loanConfigurationService = configurationServiceFactory.Create<LoanConfiguration>(Abstractions.Settings.ServiceName, reader);
            var loanConfiguration = loanConfigurationService.Get();

            var statusManagementServiceFactory = Provider.GetService<IStatusManagementServiceFactory>();
            var statusManagementService = statusManagementServiceFactory.Create(reader);

            var accrualServiceFactory = Provider.GetService<IAccrualBalanceClientFactory>();
            var accrualService = accrualServiceFactory.Create(reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);

            var productServiceFactory = Provider.GetService<IProductServiceFactory>();
            var productService = productServiceFactory.Create(reader);

            var loanScheduleHistoryRepositoryFactory = Provider.GetService<ILoanScheduleHistoryRepositoryFactory>();
            var loanScheduleHistoryRepository = loanScheduleHistoryRepositoryFactory.Create(reader);

            var lookupServiceFactory = Provider.GetService<ILookupClientFactory>();
            var lookupService = lookupServiceFactory.Create(reader);

            var dataAttributesServiceFactory = Provider.GetService<IDataAttributesClientFactory>();
            var dataAttributesService = dataAttributesServiceFactory.Create(reader);

            var bankDetailsRepositoryService = Provider.GetService<IBankDetailsRepositoryFactory>();
            var bankDetailsRepository = bankDetailsRepositoryService.Create(reader);

            var payOffFeeDiscountRepositoryService = Provider.GetService<IPayOffFeeDiscountRepositoryFactory>();
            var payOffFeeDiscountRepository = payOffFeeDiscountRepositoryService.Create(reader);

            var productRuleServiceFactory = Provider.GetService<IProductRuleServiceClientFactory>();
            var productRuleService = productRuleServiceFactory.Create(reader);

            var decisionEngineServiceFactory = Provider.GetService<IDecisionEngineClientFactory>();
            var decisionEngineService = decisionEngineServiceFactory.Create(reader);


            var calendarServiceFactory = Provider.GetService<ICalendarServiceFactory>();
            var calendarService = calendarServiceFactory.Create(reader);

            var paymentProcessorServiceFactory = Provider.GetService<IPaymentServiceClientFactory>();
            var paymentProcessorService = paymentProcessorServiceFactory.Create(reader);

            var achServiceFactory = Provider.GetService<IAchConfigurationServiceFactory>();
            var achService = achServiceFactory.Create(reader);

            return new LoanScheduleService(loanConfiguration, loanScheduleRepository, statusManagementService, loanOnBoardingRepository, bankDetailsRepository, tenantTime, loanAmortizeService, accrualService, productService, eventHub, loanScheduleHistoryRepository, lookupService, dataAttributesService, productRuleService, decisionEngineService,payOffFeeDiscountRepository, calendarService, paymentProcessorService, achService);
        }
    }
}
