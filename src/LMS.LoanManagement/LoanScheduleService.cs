﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using LendFoundry.Calendar.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DataAttributes;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.MoneyMovement.Ach;
using LendFoundry.ProductConfiguration;
using LendFoundry.ProductRule;
using LendFoundry.StatusManagement;
using LMS.Foundation.Amortization;
using LMS.Foundation.Calculus;
using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;
using LMS.Payment.Processor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LMS.LoanManagement
{
    public partial class LoanScheduleService : ILoanScheduleService
    {
        #region Constructor

        public LoanScheduleService
        (
            LoanConfiguration loanConfiguration,
            ILoanScheduleRepository loanScheduleRepository,
            IEntityStatusService statusManagementService,
            ILoanOnboardingRepository loanOnboardingRepository,
            IBankDetailsRepository bankDetailsRepository,
            ITenantTime tenantTime,
            ILoanAmortization loanAmortization,
            IAccrualBalanceService accrualBalanceService,
            IProductService productService,
            IEventHubClient eventHubClient,
            ILoanScheduleHistoryRepository loanScheduleHistoryRepository,
            ILookupService lookup,
            IDataAttributesEngine dataAttributesEngine,
            IProductRuleService productRuleService,
            IDecisionEngineService decisionEngineService,
            IPayOffFeeDiscountRepository payOffFeeDiscountRepository,
            ICalendarService calenderService,
            IPaymentProcessorService paymentProcessorService,
            IAchConfigurationService achService
        )
        {
            if (loanConfiguration == null)
                throw new ArgumentException("Configuration not set");

            LoanConfiguration = loanConfiguration;
            LoanScheduleRepository = loanScheduleRepository;
            BankDetailsRepository = bankDetailsRepository;
            LoanOnboardingRepository = loanOnboardingRepository;
            StatusManagementService = statusManagementService;
            AccrualBalanceService = accrualBalanceService;
            TenantTime = tenantTime;
            LoanAmortization = loanAmortization;
            EventHubClient = eventHubClient;
            ProductService = productService;
            LoanScheduleHistoryRepository = loanScheduleHistoryRepository;
            Lookup = lookup;
            DataAttributesEngine = dataAttributesEngine;
            ProductRuleService = productRuleService;
            DecisionEngineService = decisionEngineService;
            PayOffFeeDiscountRepository = payOffFeeDiscountRepository;
            CalendarService = calenderService;
            PaymentProcessorService = paymentProcessorService;
            AchService = achService;
        }

        #endregion

        #region Variables

        private IPayOffFeeDiscountRepository PayOffFeeDiscountRepository { get; set; }
        private ICalendarService CalendarService { get; }
        private LoanConfiguration LoanConfiguration { get; }
        private ITenantTime TenantTime { get; }
        private ILoanScheduleRepository LoanScheduleRepository { get; }
        private ILoanAmortization LoanAmortization { get; }
        private IBankDetailsRepository BankDetailsRepository { get; }
        private ILoanOnboardingRepository LoanOnboardingRepository { get; }
        private IEntityStatusService StatusManagementService { get; }
        private IAccrualBalanceService AccrualBalanceService { get; }
        private IEventHubClient EventHubClient { get; }
        private IProductService ProductService { get; }
        private ILoanScheduleHistoryRepository LoanScheduleHistoryRepository { get; }
        private ILookupService Lookup { get; }
        private IDataAttributesEngine DataAttributesEngine { get; }
        private IProductRuleService ProductRuleService { get; }
        private IDecisionEngineService DecisionEngineService { get; }
        private DateTime StoredDate { get; set; }
        private bool IsStored { get; set; }
        private IPaymentProcessorService PaymentProcessorService { get; }
        private IAchConfigurationService AchService { get; }
        private double WeekConstant = 4.34812141;

        #endregion

        public async Task<ILoanSchedule> LoanSchedule(ILoanScheduleRequest loanScheduleRequest)
        {
            var loan = await EnsureValidInput(loanScheduleRequest);
            var loanScheduleList = loan.Item1;
            var loanInformation = loan.Item2;

            var details = await GenerateLoanSchedule(loanInformation.LoanNumber, loanInformation.ProductPortfolioType,
                loanScheduleRequest, loanScheduleList, loanInformation);
            var loanSchedule = details.Item1;
            loanSchedule.ScheduledCreatedReason = loan.Item3;
            if (loanInformation.ProductPortfolioType != ProductPortfolioType.SCF)
            {
                // TODO: if condition to be removed once new method is completely tested.
                if (LoanConfiguration.UseLatestAmortization)
                {
                    var productParameters = await GetParametersValues(loanInformation.LoanNumber, loanInformation.LoanProductId);
                    if (loanScheduleRequest.PaymentFrequency == PaymentFrequency.Weekly || loanScheduleRequest.PaymentFrequency == PaymentFrequency.BiWeekly)
                    {
                        loanSchedule.Tenure = productParameters.Rest.Item4.ToLower() == "separate"
                                                && loanInformation.ProductPortfolioType != ProductPortfolioType.Installment
                                                && loanScheduleRequest.PaymentFrequency != PaymentFrequency.Daily
                                                && loanScheduleRequest.FundedDate.DayOfWeek != (DayOfWeek)loanScheduleRequest.BillingDate
                                            ? loanScheduleRequest.Tenure + 1
                                            : loanScheduleRequest.Tenure;
                    }
                    else
                    {
                        loanSchedule.Tenure = productParameters.Rest.Item4.ToLower() == "separate"
                                                && loanInformation.ProductPortfolioType != ProductPortfolioType.Installment
                                                && loanScheduleRequest.PaymentFrequency != PaymentFrequency.Daily
                                                && loanScheduleRequest.FundedDate.Day != loanScheduleRequest.BillingDate
                                            ? loanScheduleRequest.Tenure + 1
                                            : loanScheduleRequest.Tenure;
                    }
                }
                if (loanSchedule.ScheduleDetails == null || loanSchedule.Tenure != loanSchedule.ScheduleDetails.Where(s => s.Installmentnumber > 0).Count())
                    throw new ArgumentException($"Loan schedule details does not match with tenure.");
            }

            if (loanInformation.ProductPortfolioType == ProductPortfolioType.MCA ||
                loanInformation.ProductPortfolioType == ProductPortfolioType.MCALOC)
            {
                if (loanScheduleRequest.PaybackTiers != null && loanScheduleRequest.PaybackTiers.Any())
                {
                    if (loanScheduleRequest.PaybackTiers.Any(c => !c.IsPercent))
                    {
                        if (loanScheduleRequest.PaybackTiers.Sum(c => c.Principal) != loanSchedule.LoanAmount)
                            throw new ArgumentException($"Principal does not match with loan amount");

                        if (loanScheduleRequest.PaybackTiers.Sum(c => c.FinanceCharge) != loanSchedule.ScheduleDetails
                                .Select(s => s.CumulativeInterestAmount).LastOrDefault())
                            throw new ArgumentException($"Finance charge does not match with loan amount");
                    }
                }
            }

            // Update other loan schedule IsCurrent as false.
            loanScheduleList.ForEach(l => l.IsCurrent = false);
            foreach (var schedule in loanScheduleList)
            {
                await Task.Run(() => LoanScheduleRepository.Update(schedule));
            }

            // Add new loan schedule with provided version.
            await Task.Run(() => LoanScheduleRepository.Add(loanSchedule));

            var loanEndDate = loanSchedule != null && loanSchedule.ScheduleDetails != null
                ? new TimeBucket(loanSchedule.ScheduleDetails.OrderByDescending(s => s.OriginalScheduleDate)
                    .Select(s => s.OriginalScheduleDate).FirstOrDefault())
                : loanInformation.LoanEndDate;
            await UpdateLoanEndDate(loanSchedule.LoanNumber, loanEndDate, loanInformation);

            if (loanInformation.ProductPortfolioType == ProductPortfolioType.LineOfCredit ||
                loanInformation.ProductPortfolioType == ProductPortfolioType.MCALOC)
            {
                if (loanScheduleRequest.ScheduledCreatedReason != LoanConfiguration.ScheduleReamortizeReason)
                {
                    var drawDownDetails = new DrawDownDetails();
                    drawDownDetails.DrawDownDate = loanSchedule.FundedDate.Time.Date;
                    drawDownDetails.DrawDownNumber = loanScheduleRequest.DrawDownNumber;
                    drawDownDetails.LoanAmount = loanScheduleRequest.LoanAmount;
                    drawDownDetails.TotalDrawDownAmount = loanSchedule.LoanAmount;
                    drawDownDetails.LoanScheduleNumber = loanSchedule.LoanScheduleNumber;
                    await AddDrawDownDetails(loanInformation.LoanNumber, drawDownDetails);

                    // Publish event.
                    await EventHubClient.Publish(new DrawdownCreated
                    {
                        EntityType = "loan",
                        LoanNumber = loanSchedule.LoanNumber,
                        ProductId = loanInformation.LoanProductId,
                        Response = loanSchedule
                    });
                }
            }

            // Publish event.
            await EventHubClient.Publish(new LoanScheduleCreated()
            {
                EntityType = "loan",
                LoanNumber = loanSchedule.LoanNumber,
                ProductId = loanInformation.LoanProductId,
                LoanResponse = loan.Item2,
                ScheduleResponse = loanSchedule,
                AccountingResponse = details.Item2
            });

            return loanSchedule;
        }

        public async Task SendPayOffSchedule(string loanNumber, string version,
            List<Abstractions.PayOffResponse> payOffList)
        {
            var scheduleDocument = WriteToCsv(payOffList);
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            var loanAccrual = await AccrualBalanceService.GetLoanByScheduleVersion(loanNumber, version);
            // Publish event.       
            await EventHubClient.Publish(new LoanPayoffSchedule()
            {
                EntityType = "loan",
                LoanNumber = loanNumber,
                ProductId = loanInformation.LoanProductId,
                LoanResponse = loanInformation,
                ScheduleResponse = payOffList,
                AccountingResponse = loanAccrual
            });
        }

        public async Task<ILoanSchedule> UpdateLoanSchedule(string loanNumber, string scheduleId,
            ILoanScheduleRequest loanScheduleRequest)
        {
            var loan = await EnsureValidInput(loanScheduleRequest, true);
            var loanScheduleList = loan.Item1;
            var loanInformation = loan.Item2;
            var details = await GenerateLoanSchedule(loanInformation.LoanNumber, loanInformation.ProductPortfolioType,
                loanScheduleRequest, loanScheduleList, loanInformation, true);
            var loanSchedule = details.Item1;
            loanSchedule.ScheduledCreatedReason = loan.Item3;
            if (loanSchedule.ScheduleDetails == null || loanSchedule.Tenure != loanSchedule.ScheduleDetails.Count())
                throw new ArgumentException($"Loan schedule details does not match with tenure.");

            if (loanInformation.ProductPortfolioType == ProductPortfolioType.LineOfCredit ||
                loanInformation.ProductPortfolioType == ProductPortfolioType.MCALOC)
            {
                var drawDownDetails = new DrawDownDetails();
                drawDownDetails.DrawDownDate = loanSchedule.FundedDate.Time.Date;
                drawDownDetails.DrawDownNumber = loanScheduleRequest.DrawDownNumber;
                drawDownDetails.LoanAmount = loanScheduleRequest.LoanAmount;
                drawDownDetails.TotalDrawDownAmount = loanSchedule.LoanAmount;
                await UpdateDrawDowDetails(loanInformation.LoanNumber, drawDownDetails);
            }

            await AddHistory(loanNumber, loanSchedule.ScheduleVersionNo);

            // Update other loan schedule IsCurrent as false.
            loanScheduleList.ForEach(l => l.IsCurrent = false);
            foreach (var schedule in loanScheduleList)
            {
                await Task.Run(() => LoanScheduleRepository.UpdateLoanSchedule(loanNumber, schedule.Id, schedule));
            }

            // Update loan schedule with provided schedule id.
            await Task.Run(() => LoanScheduleRepository.UpdateLoanSchedule(loanNumber, scheduleId, loanSchedule));

            var loanEndDate = loanSchedule != null && loanSchedule.ScheduleDetails != null
                ? new TimeBucket(loanSchedule.ScheduleDetails.OrderByDescending(s => s.ScheduleDate)
                    .Select(s => s.ScheduleDate).FirstOrDefault())
                : loanInformation.LoanEndDate;
            await UpdateLoanEndDate(loanSchedule.LoanNumber, loanEndDate, loanInformation);

            // Publish event.
            await EventHubClient.Publish(new LoanScheduleUpdated()
            {
                EntityType = "loans",
                LoanNumber = loanSchedule.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            return loanSchedule;
        }

        public async Task InitializeLoan(string loanNumber, bool autoAccrualPastDate)
        {
            if (string.IsNullOrWhiteSpace(loanNumber))
                throw new ArgumentNullException($"{nameof(loanNumber)} cannot be null");

            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Loan information does not exist for loan number: {loanNumber}");

            var loanSchedules = await LoanScheduleRepository.GetLoanScheduleByLoanNumber(loanNumber);

            if (loanSchedules == null || !loanSchedules.Any())
                throw new NotFoundException($"Loan schedule does not exist for loan number: {loanNumber}");

            await EnsureInitializeValid(loanNumber, loanInformation, loanSchedules.FirstOrDefault(l => l.IsCurrent));

            var loanStatus = await GetLoanStatus(loanNumber);
            var loanExpectedStatus = Convert.ToString(LoanConfiguration.Statuses["Created"]);
            if (loanSchedules.Count() > 1)
                loanExpectedStatus = Convert.ToString(LoanConfiguration.Statuses["InService"]);

            if (loanStatus.Code != loanExpectedStatus)
                throw new ArgumentException($"Status of loan is invalid");

            if (loanStatus.Code != Convert.ToString(LoanConfiguration.Statuses["InService"]))
            {
                var statusWorkFlow =
                    await StatusManagementService.GetActiveStatusWorkFlow("loan", loanInformation.LoanNumber);

                // Update status as -> InService.
                await StatusManagementService.ChangeStatus("loan", loanInformation.LoanNumber,
                    statusWorkFlow != null
                        ? Convert.ToString(statusWorkFlow.StatusWorkFlowId)
                        : LoanConfiguration.DefaultStatusFlow, LoanConfiguration.Statuses["InService"],
                    new RequestModel());
            }

            var loanSchedule = loanSchedules.Where(l => l.IsCurrent).FirstOrDefault();

            // Loan accrual initiation.
            await AccrualBalanceService.InitializationAccrual(loanSchedule.LoanNumber);
            if (autoAccrualPastDate == true && loanSchedule.FundedDate != null)
            {
                await AccrualBalanceService.Accrual(loanNumber, new AccrualRequest()
                {
                    StartDate = loanSchedule.FundedDate.Time,
                    EndDate = TenantTime.Now.Date.AddDays(-1).Date
                });

            }
        }

        public async Task<Tuple<List<ILoanScheduleDetails>, List<ILoanScheduleDetail>>> Amortize(
            ILoanScheduleRequest loanScheduleRequest, double loanAmount, double factorRate = 0.0,
            double paymentAmount = 0.0, double remainingInterest = 0, bool isLoanMod = false, DateTime? calculatedFundedDate = null)
        {
            var loanInformation =
                await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanScheduleRequest.LoanNumber);

            var requestParam = new LoanAmortizationRequest();
            requestParam.LoanAmount = loanAmount <= 0 ? loanScheduleRequest.LoanAmount : loanAmount;
            requestParam.InterestRate = loanScheduleRequest.AnnualRate;
            requestParam.PaymentFrequency = loanScheduleRequest.PaymentFrequency;
            requestParam.Term = loanScheduleRequest.Tenure;
            requestParam.PaymentStartDate = loanScheduleRequest.FirstPaymentDate.Value.Date;
            requestParam.PortfolioType = loanInformation.ProductPortfolioType.ToString();
            requestParam.IsHolidaySchedule = LoanConfiguration.ScheduleHolidays;
            requestParam.InterestHandlingType = loanScheduleRequest.InterestHandlingType == InterestHandlingType.None
                ? LoanConfiguration.InterestHandlingType
                : loanScheduleRequest.InterestHandlingType;
            requestParam.LoanStartDate = calculatedFundedDate.HasValue ? calculatedFundedDate.Value : loanScheduleRequest.FundedDate.Date;
            requestParam.FactorRate = factorRate != 0.0 ? factorRate : loanScheduleRequest.FactorRate;
            requestParam.OriginalStartDate = loanScheduleRequest.OriginalFirstPaymentDate.Date;
            requestParam.PaymentAmount = paymentAmount;
            requestParam.InterestAdjustmentType = loanScheduleRequest.InterestAdjustmentType;
            requestParam.RemainingInterestAmount = remainingInterest;
            requestParam.PaymentStream = loanScheduleRequest.PaymentStreams;

            var productParameters = await GetParametersValues(loanInformation.LoanNumber, loanInformation.LoanProductId);

            requestParam.RoundingMethod = productParameters.Item1;
            requestParam.RoundingDigit = productParameters.Item2;
            requestParam.IsDoubleScheduleForHoliday =
                requestParam.PortfolioType.ToLower().ToString() == "mca" && productParameters.Item4 == true
                    ? true
                    : false;
            requestParam.ActualAmortizationType = productParameters.Item5;
            requestParam.DailyAccrualMethod = productParameters.Item6;
            requestParam.ScheduleType = productParameters.Rest.Item1;
            requestParam.MADPercentage = loanScheduleRequest.MADPercentage;
            requestParam.NumberOfDaysInYear = productParameters.Rest.Item2;
            var paybackTiers = new List<PaybackTier>();
            if (loanScheduleRequest.PaybackTiers == null || !loanScheduleRequest.PaybackTiers.Any())
            {
                paybackTiers.Add(new PaybackTier
                {
                    FinanceCharge = 100,
                    Principal = 100,
                    NoOfPayments = loanScheduleRequest.Tenure,
                    IsPercent = true
                });
            }
            else
            {
                foreach (var paybackTier in loanScheduleRequest.PaybackTiers)
                {
                    paybackTiers.Add(new PaybackTier
                    {
                        FinanceCharge = paybackTier.FinanceCharge,
                        Principal = paybackTier.Principal,
                        NoOfPayments = paybackTier.NoOfPayments,
                        IsPercent = paybackTier.IsPercent
                    });
                }
            }

            requestParam.PaybackTier = paybackTiers;

            requestParam.SlabDetails = loanScheduleRequest.SlabDetails;
            requestParam.Term = loanScheduleRequest.Tenure;
            requestParam.NumberOfDaysInMonth = loanScheduleRequest.NumberOfDaysInMonth;
            requestParam.NumberOfDaysInWeek = loanScheduleRequest.NumberOfDaysInWeek;
            ILoanAmortizationResponse schedules = new LoanAmortizationResponse();
            if (loanScheduleRequest.IsMigration == true)
            {
                List<ILoanScheduleDetail> scheduleDetails = new List<ILoanScheduleDetail>();
                foreach (var loanScheduleDetail in loanScheduleRequest.LoanScheduleDetails)
                {
                    ILoanScheduleDetail scheduleData = new LoanScheduleDetail();
                    scheduleData.ClosingPrincipalOutStandingBalance =
                        loanScheduleDetail.ClosingPrincipalOutStandingBalance;
                    scheduleData.CumulativeInterestAmount = loanScheduleDetail.CumulativeInterestAmount;
                    scheduleData.CumulativePaymentAmount = loanScheduleDetail.CumulativePaymentAmount;
                    scheduleData.CumulativePrincipalAmount = loanScheduleDetail.CumulativePrincipalAmount;
                    scheduleData.Installmentnumber = loanScheduleDetail.Installmentnumber;
                    scheduleData.InterestAmount = loanScheduleDetail.InterestAmount;
                    scheduleData.OpeningPrincipalOutStandingBalance =
                        loanScheduleDetail.OpeningPrincipalOutStandingBalance;
                    scheduleData.PaymentAmount = loanScheduleDetail.PaymentAmount;
                    scheduleData.PrincipalAmount = loanScheduleDetail.PrincipalAmount;
                    scheduleData.ScheduleDate = loanScheduleDetail.ScheduleDate.Date;
                    scheduleData.OriginalScheduleDate = loanScheduleDetail.OriginalScheduleDate;
                    scheduleData.InterestHandlingType = loanScheduleDetail.InterestHandlingType;
                    scheduleData.FrequencyDays = loanScheduleDetail.FrequencyDays;
                    scheduleData.SurplusAmount = loanScheduleDetail.SurplusAmount;
                    scheduleDetails.Add(scheduleData);
                }

                schedules = LoanAmortization.CalculateCumulativeForSchedule(scheduleDetails,
                    loanScheduleRequest.LoanAmount, productParameters.Item1, productParameters.Item2);
            }
            else
            {
                if (!LoanConfiguration.UseLatestAmortization)
                {
                    schedules = LoanAmortization.GetAmortizationScedule(requestParam);
                }
                else
                {
                    var amortizationRequest = new AmortizationRequest();
                    amortizationRequest.RoundingDigit = productParameters.Item2;
                    amortizationRequest.RoundingMethod = productParameters.Item1;
                    amortizationRequest.ActualAmortizationType = productParameters.Item5;
                    amortizationRequest.BillingDate = Convert.ToInt32(loanScheduleRequest.BillingDate);
                    amortizationRequest.CarryForwardOutstanding = productParameters.Rest.Item3 ? "PrincipalOnly" : "PrincipalInterest";
                    amortizationRequest.DailyAccrualMethod = productParameters.Item6;
                    amortizationRequest.IsDoubleScheduleForHoliday = loanInformation.ProductPortfolioType.ToString().ToLower() == "mca" && productParameters.Item4 == true
                                                                        ? true
                                                                        : false;
                    amortizationRequest.ScheduleType = (ScheduleType)productParameters.Rest.Item1;
                    amortizationRequest.BrokenPeriodHandlingType = productParameters.Rest.Item4;
                    amortizationRequest.BrokenPeriodInterestType = productParameters.Rest.Item5;

                    amortizationRequest.LoanAmount = loanAmount <= 0 ? loanScheduleRequest.LoanAmount : loanAmount;
                    amortizationRequest.InterestRate = loanScheduleRequest.AnnualRate;
                    amortizationRequest.PaymentFrequency = loanScheduleRequest.PaymentFrequency;
                    // TODO: Once old method is removed this will also get removed.
                    if (amortizationRequest.PaymentFrequency == PaymentFrequency.Weekly || amortizationRequest.PaymentFrequency == PaymentFrequency.BiWeekly)
                    {
                        amortizationRequest.Term = amortizationRequest.BrokenPeriodHandlingType.ToLower() == "separate"
                                                && loanInformation.ProductPortfolioType == ProductPortfolioType.Installment
                                                && loanScheduleRequest.PaymentFrequency != PaymentFrequency.Daily
                                                && loanScheduleRequest.FundedDate.DayOfWeek != (DayOfWeek)loanScheduleRequest.BillingDate
                                            ? loanScheduleRequest.Tenure - 1
                                            : loanScheduleRequest.Tenure;
                    }
                    else
                    {
                        amortizationRequest.Term = amortizationRequest.BrokenPeriodHandlingType.ToLower() == "separate"
                                                && loanInformation.ProductPortfolioType == ProductPortfolioType.Installment
                                                && loanScheduleRequest.PaymentFrequency != PaymentFrequency.Daily
                                                && loanScheduleRequest.FundedDate.Day != loanScheduleRequest.BillingDate
                                            ? loanScheduleRequest.Tenure - 1
                                            : loanScheduleRequest.Tenure;
                    }
                    amortizationRequest.IsHolidaySchedule = LoanConfiguration.ScheduleHolidays;
                    amortizationRequest.PortfolioType = loanInformation.ProductPortfolioType.ToString();
                    amortizationRequest.FirstPaymentDate = loanScheduleRequest.FirstPaymentDate.Value.Date;
                    amortizationRequest.OriginalFirstPaymentDate = loanScheduleRequest.OriginalFirstPaymentDate.Date;
                    amortizationRequest.FundedDate = calculatedFundedDate.HasValue ? calculatedFundedDate.Value : loanScheduleRequest.FundedDate.Date;
                    amortizationRequest.MaturityTermDays = loanInformation.ProductPortfolioType == ProductPortfolioType.SCF
                                                        ? productParameters.Item7 * amortizationRequest.Term
                                                        : 0;
                    schedules = LoanAmortization.GetAmortizationSchedule(amortizationRequest);
                }
            }

            var rollPrincipal = await GetScheduleRollPrincipalValue(loanInformation.LoanNumber, loanInformation.LoanProductId);
            List<ILoanScheduleDetails> loanScheduleList = new List<ILoanScheduleDetails>();
            foreach (var schedule in schedules.LoanScheduleDetail)
            {
                LoanScheduleDetails loanScheduleDetail = schedule != null ? new LoanScheduleDetails(schedule) : null;
                if (!isLoanMod && rollPrincipal == false)
                {
                    loanScheduleDetail.SurplusAmount = 0;
                }

                loanScheduleList.Add(loanScheduleDetail);
            }

            return Tuple.Create(loanScheduleList, schedules.DailyLoanScheduleDetail);
        }

        public List<ILoanScheduleDetails> Amortize(ILoanScheduleRequest loanScheduleRequest, double loanAmount,
            string portfolio, string roundingMethod, int roundingDigit, double factorRate = 0.0,
            double paymentAmount = 0.0)
        {
            var requestParam = new LoanAmortizationRequest();
            requestParam.LoanAmount = loanAmount <= 0 ? loanScheduleRequest.LoanAmount : loanAmount;
            requestParam.InterestRate = loanScheduleRequest.AnnualRate;
            requestParam.PaymentFrequency = loanScheduleRequest.PaymentFrequency;
            requestParam.Term = loanScheduleRequest.Tenure;
            requestParam.PaymentStartDate = GetFirstPaymentDateBasedOnFrequency(
                loanScheduleRequest,
                new TimeBucket(loanScheduleRequest.FundedDate.Date)).Value;
            requestParam.PortfolioType = portfolio;
            requestParam.IsHolidaySchedule = LoanConfiguration.ScheduleHolidays;
            requestParam.InterestHandlingType =
                loanScheduleRequest.InterestAdjustmentType == InterestAdjustmentType.None
                    ? LoanConfiguration.InterestHandlingType
                    : InterestHandlingType.None;
            requestParam.LoanStartDate = loanScheduleRequest.FundedDate.Date;
            requestParam.FactorRate = factorRate != 0.0 ? factorRate : loanScheduleRequest.FactorRate;
            requestParam.OriginalStartDate = loanScheduleRequest.OriginalFirstPaymentDate.Date;
            requestParam.PaymentAmount = paymentAmount;
            requestParam.InterestAdjustmentType = loanScheduleRequest.InterestAdjustmentType;

            requestParam.RoundingMethod = roundingMethod;
            requestParam.RoundingDigit = roundingDigit;
            requestParam.IsDoubleScheduleForHoliday =
                requestParam.PortfolioType.ToLower().ToString() == "mca" &&
                LoanConfiguration.IsDoubleScheduleForHoliday == true
                    ? true
                    : false;
            requestParam.ScheduleType = loanScheduleRequest.ScheduleType;
            requestParam.MADPercentage = loanScheduleRequest.MADPercentage;

            var paybackTiers = new List<PaybackTier>();
            if (loanScheduleRequest.PaybackTiers == null || !loanScheduleRequest.PaybackTiers.Any())
            {
                paybackTiers.Add(new PaybackTier
                {
                    FinanceCharge = 100,
                    Principal = 100,
                    NoOfPayments = loanScheduleRequest.Tenure,
                    IsPercent = true
                });
            }
            else
            {
                foreach (var paybackTier in loanScheduleRequest.PaybackTiers)
                {
                    paybackTiers.Add(new PaybackTier
                    {
                        FinanceCharge = paybackTier.FinanceCharge,
                        Principal = paybackTier.Principal,
                        NoOfPayments = paybackTier.NoOfPayments,
                        IsPercent = paybackTier.IsPercent
                    });
                }
            }

            requestParam.PaybackTier = paybackTiers;

            var schedules = LoanAmortization.GetAmortizationScedule(requestParam);

            List<ILoanScheduleDetails> loanScheduleList = new List<ILoanScheduleDetails>();
            foreach (var schedule in schedules.LoanScheduleDetail)
            {
                LoanScheduleDetails loanScheduleDetail = schedule != null ? new LoanScheduleDetails(schedule) : null;
                loanScheduleList.Add(loanScheduleDetail);
                // TODO: Uncomment in future
                //loanScheduleList.ForEach(c => { c.OpeningPrincipalOutStandingBalance = RoundOff.Round(c.OpeningPrincipalOutStandingBalance, requestParam.RoundingDigit, requestParam.RoundingMethod); c.ClosingPrincipalOutStandingBalance = RoundOff.Round(c.ClosingPrincipalOutStandingBalance, requestParam.RoundingDigit, requestParam.RoundingMethod); });
            }

            return loanScheduleList;
        }

        public async Task<List<ILoanScheduleDetails>> GetScheduleDetail(string loanNumber, string version,
            IProcessingDateRequst prosessingDate)
        {
            if (string.IsNullOrWhiteSpace(loanNumber))
                throw new ArgumentNullException($"{nameof(loanNumber)} cannot be null");

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentNullException($"{nameof(version)} cannot be null");

            if (prosessingDate == null)
                throw new ArgumentNullException($"{nameof(prosessingDate)} cannot be null");

            var schedules =
                await LoanScheduleRepository.GetLoanScheduleByLoanNumberAndScheduleVersion(loanNumber, version);
            if (schedules == null)
                throw new InvalidOperationException(
                    $"Loan schedule for {loanNumber} version {version}  is not available.");
            var schduleDetail = new List<ILoanScheduleDetails>();
            if (!string.IsNullOrWhiteSpace(prosessingDate.StartDate) &&
                !string.IsNullOrWhiteSpace(prosessingDate.EndDate))
            {
                var startDate = Convert.ToDateTime(prosessingDate.StartDate);
                var endDate = Convert.ToDateTime(prosessingDate.EndDate);
                schduleDetail = schedules.ScheduleDetails
                    .Where(x => x.ScheduleDate >= startDate.Date && x.ScheduleDate <= endDate).ToList();
            }
            else
                schedules.ScheduleDetails.ToList();

            return schduleDetail;
        }

        public async Task<Stream> DownloadAmortizationSchedule(ILoanScheduleRequest loanScheduleRequest)
        {
            var schedules = await Amortize(loanScheduleRequest, loanScheduleRequest.LoanAmount);
            var scheduleBytes = WriteToCsv(schedules.Item1);
            return new MemoryStream(scheduleBytes);
        }

        public double GetPaymentAmount(ILoanScheduleRequest schedule)
        {
            if (schedule == null)
                throw new ArgumentNullException($"{nameof(schedule)} cannot be null");

            if (schedule.LoanAmount <= 0)
                throw new ArgumentNullException($"{nameof(schedule.LoanAmount)} cannot be 0");

            if (schedule.AnnualRate <= 0)
                throw new ArgumentNullException($"{nameof(schedule.AnnualRate)} cannot be 0");
            var tenure = CalculateTenure(schedule.Tenure, schedule.PaymentFrequency);

            var interestRate =
                InterestRate.Calculate_InterestRate(schedule.PaymentFrequency.ToString(), schedule.AnnualRate);
            return PMT.Calculate_PMT(schedule.LoanAmount, tenure, interestRate);
        }

        public async Task<ILoanSchedule> GetLoanSchedule(string loanNumber, string version = null)
        {
            var loanSchedule = await LoanScheduleRepository.GetLoanSchedule(loanNumber, version);
            if (loanSchedule == null)
            {
                throw new NotFoundException($"Loan schedule for loanNumber {loanNumber} and version {version} is not available.");
            }

            return loanSchedule;
        }

        public async Task<IStatusResponse> GetLoanStatus(string loanNumber, ILoanInformation loanInformation = null)
        {
            if (loanInformation == null)
            {
                loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            }

            if (loanInformation == null)
                throw new NotFoundException($"Loan information does not exist for loan number: {loanNumber}");

            var statusWorkFlow =
                await StatusManagementService.GetActiveStatusWorkFlow("loan", loanInformation.LoanNumber);

            return await StatusManagementService.GetStatusByEntity("loan", loanNumber,
                statusWorkFlow != null
                    ? Convert.ToString(statusWorkFlow.StatusWorkFlowId)
                    : LoanConfiguration.DefaultStatusFlow);
        }

        public async Task<List<ILoanSchedule>> GetLoanScheduleByLoanNumber(string loanNumber)
        {
            if (string.IsNullOrWhiteSpace(loanNumber))
                throw new ArgumentNullException($"{nameof(loanNumber)} cannot be null");

            var loanScheduleList = await LoanScheduleRepository.GetLoanScheduleByLoanNumber(loanNumber);

            if (loanScheduleList == null || !loanScheduleList.Any())
                throw new NotFoundException($"Loan schedule details not found for loan number: {loanNumber}.");

            return loanScheduleList;
        }

        public async Task<ILoanInformation> AddDrawDownDetails(string loanNumber, IDrawDownDetails drawDownDetails)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation.DrawDownDetails == null)
                loanInformation.DrawDownDetails = new List<IDrawDownDetails> { drawDownDetails };
            else
                loanInformation.DrawDownDetails.Add(drawDownDetails);
            await LoanOnboardingRepository.UpdateLoanInformation(loanNumber, loanInformation);

            // Publish event.
            await EventHubClient.Publish(new LoanDetailsUpdated()
            {
                EntityType = "loan",
                LoanNumber = loanInformation.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            return loanInformation;
        }

        public async Task<ILoanInformation> UpdateDrawDowDetails(string loanNumber, IDrawDownDetails drawDownDetails)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation.DrawDownDetails == null ||
                loanInformation.DrawDownDetails.Any(a => a.DrawDownNumber != drawDownDetails.DrawDownNumber))
                throw new ArgumentException($"Invalid value of drawdown number for respective schedule updating");

            loanInformation.DrawDownDetails.ForEach(d =>
            {
                d.DrawDownDate = drawDownDetails.DrawDownDate;
                d.LoanAmount = drawDownDetails.LoanAmount;
                d.LoanScheduleNumber = drawDownDetails.LoanScheduleNumber;
            });

            await LoanOnboardingRepository.UpdateLoanInformation(loanNumber, loanInformation);

            // Publish event.
            await EventHubClient.Publish(new LoanDetailsUpdated()
            {
                EntityType = "loan",
                LoanNumber = loanInformation.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            return loanInformation;
        }

        public async Task<ProductDetails> GetProductParameters(string productId, string loanNumber)
        {
            var productDetails = await DataAttributesEngine.GetAttribute("loan", loanNumber, "product");
            var productObject = JsonConvert.DeserializeObject<List<ProductDetails>>(productDetails.ToString());
            return productObject.FirstOrDefault();
        }

        public async Task<Abstractions.IFeeDetails> AddFeeDetails(string loanNumber,
            Abstractions.IFeeDetails feeDetails)
        {
            var loanSchedule = await LoanScheduleRepository.GetLoanSchedule(loanNumber, null);
            if (loanSchedule == null)
                throw new NotFoundException($"Loan schedule details for loan number: {loanNumber} is not available.");

            if (loanSchedule.FeeDetails == null)
            {
                loanSchedule.FeeDetails = new List<Abstractions.IFeeDetails>();
                feeDetails.Id = Guid.NewGuid().ToString("N");
                loanSchedule.FeeDetails.Add(feeDetails);
            }
            else
            {
                feeDetails.Id = Guid.NewGuid().ToString("N");
                loanSchedule.FeeDetails.Add(feeDetails);
            }

            await LoanScheduleRepository.UpdateLoanSchedule(loanNumber, loanSchedule.Id, loanSchedule);

            return feeDetails;
        }

        public async Task<Abstractions.IFeeDetails> DeleteFeeDetails(string loanNumber, string feeId)
        {
            var loanSchedule = await LoanScheduleRepository.GetLoanSchedule(loanNumber, null);
            if (loanSchedule == null)
                throw new NotFoundException($"Loan schedule details for loan number: {loanNumber} is not available.");

            if (loanSchedule.FeeDetails == null)
                throw new ArgumentException($"Fee details for loan number: {loanNumber} is not available.");

            var deleteFeeDetails = loanSchedule.FeeDetails.Where(i => i.Id == feeId).FirstOrDefault();

            if (deleteFeeDetails == null)
                throw new ArgumentException(
                    $"Fee details for loan number: {loanNumber} and FeeId {feeId} is not available.");

            loanSchedule.FeeDetails.Remove(deleteFeeDetails);

            await LoanScheduleRepository.UpdateLoanSchedule(loanNumber, loanSchedule.Id, loanSchedule);

            return deleteFeeDetails;
        }

        public async Task<ILoanSchedule> UpdateFeeDetails(string loanNumber, string id,
            Abstractions.IFeeDetails feeDetails)
        {
            var loanSchedule = await LoanScheduleRepository.GetLoanSchedule(loanNumber, null);
            if (loanSchedule == null)
                throw new NotFoundException($"Loan schedule details for loan number: {loanNumber} is not available.");

            if (loanSchedule.FeeDetails == null)
                throw new ArgumentException($"Fee details for loan number: {loanNumber} is not available.");

            if (string.IsNullOrWhiteSpace(feeDetails.Id))
                feeDetails.Id = id;

            if (!loanSchedule.FeeDetails.Any(f => f.Id == feeDetails.Id))
                throw new ArgumentException($"Fee details for loan number: {loanNumber} is not available.");

            loanSchedule.FeeDetails.ForEach(f =>
            {
                if (f.Id == feeDetails.Id)
                {
                    f.FeeId = feeDetails.FeeId;
                    f.AutoSchedulePayment = feeDetails.AutoSchedulePayment;
                    f.DateInitiated = feeDetails.DateInitiated;
                    f.Events = feeDetails.Events;
                    f.FeeAmount = feeDetails.FeeAmount;
                    f.FeeDueDate = feeDetails.FeeDueDate;
                    f.FeeType = feeDetails.FeeType;
                    f.IsProcessed = feeDetails.IsProcessed;
                    f.FeeName = feeDetails.FeeName;
                }
            });

            await LoanScheduleRepository.UpdateLoanSchedule(loanNumber, loanSchedule.Id, loanSchedule);

            return loanSchedule;
        }

        public async Task<List<ILoanScheduleDetails>> AdHocReamortization(string loanNumber,
            ILoanModificationRequest loanModificationRequest)
        {
            if (string.IsNullOrWhiteSpace(loanNumber))
                throw new ArgumentNullException($"{nameof(loanNumber)} cannot be null");

            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new ArgumentException($"Loan details not found for loan number: {loanNumber}");

            var loanSchedules = await LoanScheduleRepository.GetLoanScheduleByLoanNumber(loanNumber);

            if (loanSchedules == null || !loanSchedules.Any())
                throw new NotFoundException($"Loan schedule not found for loan number: {loanNumber}");

            var loanScheduleDetails = await GetLoanModificationForAdHoc(loanNumber, loanModificationRequest);

            ILoanModificationActivationRequest loanModificationActivationRequest =
                new LoanModificationActivationRequest();
            loanModificationActivationRequest.LoanScheduleDetails = loanScheduleDetails;
            loanModificationActivationRequest.ActivationDate = loanModificationRequest.ScheduleStartDate;
            loanModificationActivationRequest.ScheduledCreatedReason = "recast";
            loanModificationActivationRequest.ScheduleNotes = "Loan recast due to adhoc payment";
            loanModificationActivationRequest.LoanModificationRequest = loanModificationRequest;
            await SelectLoanModification(loanNumber, loanModificationActivationRequest);
            return loanScheduleDetails;
        }

        public async Task<List<ILoanScheduleDetails>> GetLoanModificationForAdHoc(string loanNumber,
            ILoanModificationRequest loanModificationRequest)
        {
            if (string.IsNullOrWhiteSpace(loanNumber))
                throw new ArgumentNullException($"{nameof(loanNumber)} cannot be null");

            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new ArgumentException($"Loan details not found for loan number: {loanNumber}");

            var loanSchedules = await LoanScheduleRepository.GetLoanScheduleByLoanNumber(loanNumber);

            if (loanSchedules == null || !loanSchedules.Any())
                throw new NotFoundException($"Loan schedule not found for loan number: {loanNumber}");

            var currentLoanSchedule = loanSchedules.FirstOrDefault(s => s.IsCurrent);
            if (currentLoanSchedule == null)
                throw new NotFoundException($"No active loan schedule found for loan number: {loanNumber}");

            var loanAccounting =
                await AccrualBalanceService.GetLoanByScheduleVersion(loanNumber, currentLoanSchedule.ScheduleVersionNo);
            if (loanAccounting == null)
                throw new NotFoundException($"Loan accounting details not found for loan number: {loanNumber}");

            EnsureLoanModificationRequest(loanModificationRequest, loanInformation, currentLoanSchedule,
                loanAccounting);

            var loanScheduleRequest = new LoanScheduleRequest();
            loanScheduleRequest.LoanNumber = loanNumber;
            loanScheduleRequest.AnnualRate = loanModificationRequest.AnnualRate;
            loanScheduleRequest.FactorRate = loanModificationRequest.FactorRate;
            loanScheduleRequest.PaymentFrequency = loanModificationRequest.PaymentFrequency;
            loanScheduleRequest.Tenure = loanModificationRequest.Term;
            loanScheduleRequest.FundedDate = loanModificationRequest.ScheduleStartDate.Date;

            var nextPaymentSchedule = currentLoanSchedule.ScheduleDetails != null
                ? currentLoanSchedule.ScheduleDetails
                    .Where(s => s.ScheduleDate.Date > loanModificationRequest.ScheduleStartDate.Date).FirstOrDefault()
                : null;
            if (nextPaymentSchedule != null)
            {
                loanScheduleRequest.FirstPaymentDate = nextPaymentSchedule.ScheduleDate;
                loanScheduleRequest.OriginalFirstPaymentDate = nextPaymentSchedule.OriginalScheduleDate;
            }
            else
            {
                var expectedFirstPaymentDate = GetFirstPaymentDateBasedOnFrequency(
                    loanScheduleRequest,
                    new TimeBucket(loanModificationRequest.ScheduleStartDate));
                loanScheduleRequest.FirstPaymentDate = expectedFirstPaymentDate.Value;
            }

            // TODO: Need to calculate based on which date
            var loanScheduleDetails = new List<ILoanScheduleDetails>();
            loanScheduleDetails.AddRange(
                (await Amortize(loanScheduleRequest, loanModificationRequest.LoanAmount)).Item1);

            if (loanInformation.ProductPortfolioType == ProductPortfolioType.Installment)
            {
                loanScheduleDetails = loanScheduleDetails
                    .Where(s => s.Installmentnumber <= loanModificationRequest.Term).ToList();
            }

            ILoanModificationActivationRequest loanModificationActivationRequest =
                new LoanModificationActivationRequest();
            loanModificationActivationRequest.LoanScheduleDetails = loanScheduleDetails;
            loanModificationActivationRequest.ActivationDate = loanModificationRequest.ScheduleStartDate;
            loanModificationActivationRequest.ScheduledCreatedReason = "recast";
            loanModificationActivationRequest.ScheduleNotes = "Loan recast due to adhoc payment";
            loanModificationActivationRequest.LoanModificationRequest = loanModificationRequest;

            if (loanAccounting.PBOT.TotalPrincipalOutStanding < loanModificationActivationRequest.LoanScheduleDetails
                    .LastOrDefault().CumulativePrincipalAmount)
                throw new ArgumentException(
                    $"Schedule is incorrect as outstanding information value is less than principal amount of schedule created");

            var loanSchedule = new LoanSchedule();
            loanSchedule.ScheduleDetails = loanModificationActivationRequest.LoanScheduleDetails;
            loanSchedule.AnnualRate = loanModificationActivationRequest.LoanModificationRequest.AnnualRate;
            loanSchedule.FactorRate = loanModificationActivationRequest.LoanModificationRequest.FactorRate;
            loanSchedule.PaymentFrequencyRate = InterestRate.Calculate_InterestRate(
                loanModificationActivationRequest.LoanModificationRequest.PaymentFrequency.ToString(),
                loanModificationActivationRequest.LoanModificationRequest.AnnualRate);
            loanSchedule.DailyRate =
                loanModificationActivationRequest.LoanModificationRequest.PaymentFrequency == PaymentFrequency.Monthly
                    ? (loanSchedule.PaymentFrequencyRate / 30)
                    : loanModificationActivationRequest.LoanModificationRequest.PaymentFrequency ==
                      PaymentFrequency.Weekly
                        ? (loanSchedule.PaymentFrequencyRate / 7)
                        : loanModificationActivationRequest.LoanModificationRequest.PaymentFrequency ==
                          PaymentFrequency.BiWeekly
                            ? (loanSchedule.PaymentFrequencyRate / 14)
                            : loanSchedule.PaymentFrequencyRate;
            loanSchedule.FeeDetails = currentLoanSchedule.FeeDetails;
            loanSchedule.Fees = null; // TODO: Do we need to take previous.
            loanSchedule.CreditLimit = currentLoanSchedule.CreditLimit;
            loanSchedule.FirstPaymentDate = new TimeBucket(loanModificationActivationRequest.LoanScheduleDetails
                .Select(c => c.ScheduleDate).FirstOrDefault());
            loanSchedule.FundedAmount = loanModificationActivationRequest.LoanModificationRequest.LoanAmount;
            loanSchedule.FundedDate = new TimeBucket(loanModificationActivationRequest.ActivationDate.Value);
            loanSchedule.LoanAmount = loanModificationActivationRequest.LoanModificationRequest.LoanAmount;
            loanSchedule.LoanNumber = loanInformation.LoanNumber;
            loanSchedule.PaymentFrequency = loanModificationActivationRequest.LoanModificationRequest.PaymentFrequency;
            loanSchedule.ScheduledCreatedReason = loanModificationActivationRequest.ScheduledCreatedReason;
            loanSchedule.ScheduleNotes = loanModificationActivationRequest.ScheduleNotes;
            loanSchedule.ScheduleVersionDate = new TimeBucket(TenantTime.Now);
            loanSchedule.ScheduleVersionNo =
                loanSchedules != null && loanSchedules.Any() ? "v" + (loanSchedules.Count() + 1) : "v1";
            loanSchedule.LoanScheduleNumber = loanSchedule.LoanNumber + "_" + loanSchedule.ScheduleVersionNo;
            loanSchedule.Tenure = loanModificationActivationRequest.LoanModificationRequest.Term;
            loanSchedule.TotalDrawDown = currentLoanSchedule.TotalDrawDown;
            loanSchedule.IsCurrent = true;

            // Update other loan schedule IsCurrent as false.
            loanSchedules.ForEach(l => l.IsCurrent = false);
            foreach (var schedule in loanSchedules)
            {
                await Task.Run(() => LoanScheduleRepository.Update(schedule));
            }

            await Task.Run(() => LoanScheduleRepository.Add(loanSchedule));

            // Publish event.
            await EventHubClient.Publish(new LoanScheduleCreated
            {
                EntityType = "loan",
                LoanNumber = loanInformation.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            // Loan accrual initiation.
            await AccrualBalanceService.InitializationAccrual(loanNumber);
            if (loanAccounting.PBOT.TotalInterestOutstanding > 0)
                await AccrualBalanceService.RecastOutstandingInterestHandling(loanNumber,
                    currentLoanSchedule.ScheduleVersionNo);
            // Publish event.
            await EventHubClient.Publish(new AdHocScheduleCreated
            {
                EntityType = "loan",
                LoanNumber = loanInformation.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            return loanSchedule.ScheduleDetails;
        }

        public async Task<ILoanSchedule> UpdatePaymentDetails(string loanNumber, string id,
            IPaymentScheduleRequest paymentScheduleRequest)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
                throw new NotFoundException($"Loan details for loan number: {loanNumber} is not available.");

            var loanSchedule = await LoanScheduleRepository.GetLoanSchedule(loanNumber, null);
            if (loanSchedule == null)
                throw new NotFoundException($"Loan schedule details for loan number: {loanNumber} is not available.");

            if (loanSchedule.ScheduleDetails == null)
                throw new NotFoundException(
                    $"Payment schedule details for loan number: {loanNumber} is not available.");

            if (paymentScheduleRequest == null)
                throw new ArgumentNullException($"{nameof(paymentScheduleRequest)} cannot be null");

            var schedule = paymentScheduleRequest.InstallmentNumber > 0
                ? loanSchedule.ScheduleDetails.FirstOrDefault(p =>
                    p.Installmentnumber == paymentScheduleRequest.InstallmentNumber)
                : loanSchedule.ScheduleDetails.FirstOrDefault(
                    p => p.ScheduleDate == paymentScheduleRequest.ScheduleDate);
            schedule.IsMissed = true;
            var roundingParameters =
                await GetParametersValues(loanInformation.LoanNumber, loanInformation.LoanProductId);

            // Update cumulative details for other schedule details.
            var loanAmortizationRequest = new LoanAmortizationRequest();
            loanAmortizationRequest.LoanAmount = loanSchedule.LoanAmount;
            loanAmortizationRequest.InterestRate = loanSchedule.AnnualRate;
            loanAmortizationRequest.PaymentFrequency = loanSchedule.PaymentFrequency;
            loanAmortizationRequest.Term = loanSchedule.OriginalScheduleDetails.Count();
            loanAmortizationRequest.PaymentStartDate = loanSchedule.FirstPaymentDate.Time.DateTime;
            loanAmortizationRequest.PortfolioType = loanInformation.ProductPortfolioType.ToString();
            loanAmortizationRequest.FactorRate = loanSchedule.FactorRate;
            loanAmortizationRequest.RoundingMethod = roundingParameters.Item1;
            loanAmortizationRequest.RoundingDigit = roundingParameters.Item2;
            loanAmortizationRequest.ScheduleType = roundingParameters.Rest.Item1;

            var paybackTiers = new List<PaybackTier>();
            if (loanSchedule.PaybackTiers == null || !loanSchedule.PaybackTiers.Any())
            {
                paybackTiers.Add(new PaybackTier
                {
                    FinanceCharge = 100,
                    Principal = 100,
                    NoOfPayments = loanSchedule.OriginalScheduleDetails.Count,
                    IsPercent = true
                });
            }
            else
            {
                foreach (var paybackTier in loanSchedule.PaybackTiers)
                {
                    paybackTiers.Add(new PaybackTier
                    {
                        FinanceCharge = paybackTier.FinanceCharge,
                        Principal = paybackTier.Principal,
                        NoOfPayments = paybackTier.NoOfPayments,
                        IsPercent = paybackTier.IsPercent
                    });
                }
            }

            loanAmortizationRequest.PaybackTier = paybackTiers;
            var amortizationParameters = GetMCAParameters(loanAmortizationRequest);
            var missedInstallments = loanSchedule.ScheduleDetails.Where(s => s.RefernceInstallmentnumber > 0)
                .Select(s => s.RefernceInstallmentnumber).ToList();

            if (missedInstallments != null)
            {
                missedInstallments.Add(paymentScheduleRequest.InstallmentNumber);
            }
            else
            {
                missedInstallments = new List<int> { paymentScheduleRequest.InstallmentNumber };
            }

            var schedules = GetMCASchedule(amortizationParameters, missedInstallments.ToList(), schedule, loanSchedule);

            for (int i = 0; i < loanSchedule.ScheduleDetails.Count; i++)
            {
                if (loanSchedule.ScheduleDetails[i].Installmentnumber < paymentScheduleRequest.InstallmentNumber)
                    continue;

                loanSchedule.ScheduleDetails[i].ClosingPrincipalOutStandingBalance =
                    schedules[i].ClosingPrincipalOutStandingBalance;
                loanSchedule.ScheduleDetails[i].OpeningPrincipalOutStandingBalance =
                    schedules[i].OpeningPrincipalOutStandingBalance;
                loanSchedule.ScheduleDetails[i].CumulativeInterestAmount = schedules[i].CumulativeInterestAmount;
                loanSchedule.ScheduleDetails[i].CumulativePaymentAmount = schedules[i].CumulativePaymentAmount;
                loanSchedule.ScheduleDetails[i].CumulativePrincipalAmount = schedules[i].CumulativePrincipalAmount;
            }

            var lastSchedule = loanSchedule.ScheduleDetails.LastOrDefault();
            var FinanceCharge = loanSchedule.OriginalScheduleDetails.Select(c => c.CumulativeInterestAmount)
                .LastOrDefault();
            ILoanScheduleDetails loanScheduleDetails = new LoanScheduleDetails();
            loanScheduleDetails.Installmentnumber = lastSchedule.Installmentnumber + 1;
            loanScheduleDetails.RefernceInstallmentnumber = paymentScheduleRequest.InstallmentNumber;
            IsStored = false;
            loanScheduleDetails.ScheduleDate =
                GetNextScheduleForMCA(loanSchedule.PaymentFrequency, lastSchedule.ScheduleDate);
            if (schedule.PaidAmount > 0)
            {
                var paidAmount = schedule.PaidAmount;
                if (paidAmount > schedule.InterestAmount)
                {
                    loanScheduleDetails.InterestAmount = 0;
                    paidAmount = RoundOff.Round(paidAmount - schedule.InterestAmount, roundingParameters.Item2,
                        roundingParameters.Item1);
                    if (paidAmount > schedule.PrincipalAmount)
                    {
                        loanScheduleDetails.PrincipalAmount = RoundOff.Round(paidAmount - schedule.PrincipalAmount,
                            roundingParameters.Item2, roundingParameters.Item1);
                    }
                    else
                    {
                        loanScheduleDetails.PrincipalAmount = RoundOff.Round(schedule.PrincipalAmount - paidAmount,
                            roundingParameters.Item2, roundingParameters.Item1);
                    }
                }
                else
                {
                    loanScheduleDetails.InterestAmount = RoundOff.Round(schedule.InterestAmount - paidAmount,
                        roundingParameters.Item2, roundingParameters.Item1);
                    loanScheduleDetails.PrincipalAmount = schedule.PrincipalAmount;
                }
            }
            else
            {
                loanScheduleDetails.InterestAmount = RoundOff.Round(
                    (FinanceCharge - lastSchedule.CumulativeInterestAmount), roundingParameters.Item2,
                    roundingParameters.Item1);
                loanScheduleDetails.PrincipalAmount = RoundOff.Round(
                    (loanSchedule.LoanAmount - lastSchedule.CumulativePrincipalAmount), roundingParameters.Item2,
                    roundingParameters.Item1);
            }

            loanScheduleDetails.PaymentAmount = RoundOff.Round(
                loanScheduleDetails.InterestAmount + loanScheduleDetails.PrincipalAmount, roundingParameters.Item2,
                roundingParameters.Item1);
            loanScheduleDetails.OpeningPrincipalOutStandingBalance = RoundOff.Round(
                lastSchedule.ClosingPrincipalOutStandingBalance, roundingParameters.Item2, roundingParameters.Item1);
            loanScheduleDetails.ClosingPrincipalOutStandingBalance = RoundOff.Round(
                loanScheduleDetails.OpeningPrincipalOutStandingBalance - loanScheduleDetails.PrincipalAmount,
                roundingParameters.Item2, roundingParameters.Item1);
            loanScheduleDetails.CumulativeInterestAmount = RoundOff.Round(
                lastSchedule.CumulativeInterestAmount + loanScheduleDetails.InterestAmount, roundingParameters.Item2,
                roundingParameters.Item1);
            loanScheduleDetails.CumulativePrincipalAmount = RoundOff.Round(
                lastSchedule.CumulativePrincipalAmount + loanScheduleDetails.PrincipalAmount, roundingParameters.Item2,
                roundingParameters.Item1);
            loanScheduleDetails.CumulativePaymentAmount = RoundOff.Round(
                lastSchedule.CumulativePaymentAmount + loanScheduleDetails.PaymentAmount, roundingParameters.Item2,
                roundingParameters.Item1);
            loanSchedule.ScheduleDetails.Add(loanScheduleDetails);

            if (loanSchedule.ScheduleDetails.Select(p => p.CumulativePaymentAmount).LastOrDefault() != RoundOff.Round(
                    (loanSchedule.LoanAmount + FinanceCharge), roundingParameters.Item2, roundingParameters.Item1))
                throw new ArgumentNullException("New schedule didn't worked!!!");

            await LoanScheduleRepository.UpdateLoanSchedule(loanNumber, loanSchedule.Id, loanSchedule);
            return loanSchedule;
        }

        /// <summary>
        /// update paid details
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="loanSchedule"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="NotFoundException"></exception>
        public async Task<ILoanSchedule> UpdatePaidDetails(string loanNumber, ILoanSchedule loanSchedule)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
            {
                throw new ArgumentException($"Loan details for loan number: {loanNumber} is not available.");
            }

            if (loanSchedule == null)
            {
                throw new ArgumentException($"Invalid input details");
            }

            var loanScheduleDetail =
                await LoanScheduleRepository.GetLoanSchedule(loanNumber, loanSchedule.ScheduleVersionNo);
            if (loanScheduleDetail == null)
            {
                throw new NotFoundException($"Loan schedule details for loan number: {loanNumber} is not available.");
            }

            await LoanScheduleRepository.UpdatePaidDetails(loanNumber, loanSchedule);
            return loanSchedule;
        }

        public async Task<bool> AutomaticFeeSchedule(string loanNumber, IAutomaticFeeRequest automaticFeeRequest)
        {
            try
            {
                if (automaticFeeRequest == null)
                    throw new ArgumentException($"Model automaticFeeRequest can not be null");

                var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
                if (loanInformation == null)
                    throw new NotFoundException($"Loan details for loan number: {loanNumber} is not available.");

                var loanSchedule = await LoanScheduleRepository.GetLoanSchedule(loanNumber, null);
                if (loanSchedule == null)
                    throw new NotFoundException(
                        $"Loan schedule details for loan number: {loanNumber} is not available.");

                if (loanSchedule.ScheduleDetails == null)
                    throw new ArgumentException(
                        $"Payment schedule details for loan number: {loanNumber} is not available.");
                var productDetails = await GetProductParameters(loanInformation.LoanProductId, loanNumber);
                if (productDetails == null)
                    throw new ArgumentException($"Product details not set for loan number: {loanNumber}");
                if (productDetails.ProductParameters == null)
                    throw new ArgumentException($"Product parameters not set for loan number: {loanNumber}");

                var productParameter = productDetails.ProductParameters.ToList();
                var roundingMethod = productParameter.Where(p => p.Name == "RoundingMethod").Select(r => r.Value)
                    .FirstOrDefault();
                var roundingDigit = productParameter.Where(p => p.Name == "CurrencyDecimalPoint").Select(r => r.Value)
                    .FirstOrDefault();

                if (automaticFeeRequest.PartialFeeAmount > 0)
                {
                    var remainingSchedules = loanSchedule.ScheduleDetails
                        .Where(i => i.IsMissed == false && i.IsProcessed == false && i.IsPaid == false)
                        .OrderByDescending(i => i.ScheduleDate).FirstOrDefault();
                    remainingSchedules.FeeAmount += automaticFeeRequest.PartialFeeAmount;
                    remainingSchedules.PaymentAmount =
                        remainingSchedules.PaymentAmount + automaticFeeRequest.PartialFeeAmount;
                }

                var lastSchedule = loanSchedule.ScheduleDetails.LastOrDefault();
                var scheduleDate = lastSchedule.OriginalScheduleDate.Date;
                if (automaticFeeRequest.OutStandingFeeAmount > 0)
                {
                    var installmentAmount = loanSchedule.OriginalScheduleDetails.OrderByDescending(x => x.ScheduleDate)
                        .Skip(1).FirstOrDefault().PaymentAmount;

                    var loanRequest = new LoanScheduleRequest { PaymentFrequency = loanSchedule.PaymentFrequency };
                    while (automaticFeeRequest.OutStandingFeeAmount > 0)
                    {
                        if (automaticFeeRequest.OutStandingFeeAmount <= installmentAmount)
                        {
                            ILoanScheduleDetails newSchedule = new LoanScheduleDetails();
                            newSchedule.Installmentnumber = lastSchedule.Installmentnumber + 1;
                            newSchedule.InterestAmount = 0;
                            newSchedule.PrincipalAmount = 0;
                            newSchedule.PaymentAmount = automaticFeeRequest.OutStandingFeeAmount;
                            newSchedule.FeeAmount = automaticFeeRequest.OutStandingFeeAmount;
                            newSchedule.ScheduleDate =
                                GetFirstPaymentDateBasedOnFrequency(
                                    loanRequest, new TimeBucket(scheduleDate)).Value;
                            newSchedule.OriginalScheduleDate = loanRequest.OriginalFirstPaymentDate.Date;
                            automaticFeeRequest.OutStandingFeeAmount = 0;
                            loanSchedule.ScheduleDetails.Add(newSchedule);
                        }
                        else
                        {
                            ILoanScheduleDetails newSchedule = new LoanScheduleDetails();
                            newSchedule.Installmentnumber = lastSchedule.Installmentnumber + 1;
                            newSchedule.InterestAmount = 0;
                            newSchedule.PrincipalAmount = 0;
                            newSchedule.PaymentAmount = installmentAmount;
                            newSchedule.FeeAmount = installmentAmount;
                            newSchedule.ScheduleDate =
                                GetFirstPaymentDateBasedOnFrequency(
                                    loanRequest, new TimeBucket(scheduleDate)).Value;
                            newSchedule.OriginalScheduleDate = loanRequest.OriginalFirstPaymentDate.Date;
                            //  scheduleDate = newSchedule.ScheduleDate;
                            automaticFeeRequest.OutStandingFeeAmount = RoundOff.Round(
                                automaticFeeRequest.OutStandingFeeAmount - installmentAmount,
                                Convert.ToInt32(roundingDigit), roundingMethod);
                            loanSchedule.ScheduleDetails.Add(newSchedule);
                        }

                        scheduleDate = loanRequest.OriginalFirstPaymentDate.Date;
                    }
                }

                await LoanScheduleRepository.UpdatePaidDetails(loanNumber, loanSchedule);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<List<IPayOffFeeDiscount>> GetPayOffFeeDiscountSchedule(string loanNumber,
            string feeType = null)
        {
            if (string.IsNullOrWhiteSpace(loanNumber))
                throw new ArgumentNullException($"{nameof(loanNumber)} cannot be null");

            return await PayOffFeeDiscountRepository.GetPayOffFeeDiscountSchedule(loanNumber, feeType);
        }

        public async Task EnsureInitializeValid(string loanNumber, ILoanInformation loanInformation = null,
            ILoanSchedule loanSchedule = null)
        {
            if (loanInformation == null)
            {
                loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            }

            if (loanSchedule == null)
            {
                var loanSchedules = await LoanScheduleRepository.GetLoanScheduleByLoanNumber(loanNumber);
                loanSchedule = loanSchedules.FirstOrDefault(l => l.IsCurrent);
            }

            if (loanSchedule == null)
                throw new ArgumentNullException($"{nameof(loanSchedule)} cannot be null");

            if (loanInformation == null)
                throw new ArgumentNullException($"{nameof(loanInformation)} cannot be null");

            if (loanInformation.PrimaryApplicantDetails == null && loanInformation.PrimaryBusinessDetails == null)
                throw new ArgumentNullException(
                    $"{nameof(loanInformation.PrimaryApplicantDetails)} or {nameof(loanInformation.PrimaryBusinessDetails)} both of them cannot be null");

            if (loanInformation.PrimaryApplicantDetails != null)
            {
                EnsureValidApplicant(loanInformation.PrimaryApplicantDetails);
            }

            if (loanInformation.PrimaryBusinessDetails != null)
            {
                EnsureValidBusiness(loanInformation.PrimaryBusinessDetails);
            }

            if (loanInformation.ApplicantDetails != null)
            {
                foreach (var applicant in loanInformation.ApplicantDetails)
                {
                    EnsureValidApplicant(applicant);
                }
            }

            if (loanInformation.BusinessDetails != null)
            {
                foreach (var business in loanInformation.BusinessDetails)
                {
                    EnsureValidBusiness(business);
                }
            }

            var expectedFirstPaymentDate = GetFirstPaymentDateBasedOnFrequency(
                new LoanScheduleRequest { PaymentFrequency = loanSchedule.PaymentFrequency }, loanSchedule.FundedDate);
            if (expectedFirstPaymentDate == null)
                throw new ArgumentNullException($"Invalid value for {nameof(loanSchedule.FundedDate)}");

            var todayInfo = CalendarService.GetDate(expectedFirstPaymentDate.Value.Year,
                expectedFirstPaymentDate.Value.Month, expectedFirstPaymentDate.Value.Day);
            if (todayInfo.Type == DateType.Holiday || todayInfo.Type == DateType.Weekend)
            {
                expectedFirstPaymentDate = todayInfo.NextBusinessDay.Date;
            }

            if ((loanInformation.ProductPortfolioType != ProductPortfolioType.MCA &&
                 loanInformation.ProductPortfolioType != ProductPortfolioType.MCALOC) && loanSchedule.AnnualRate <= 0)
                throw new ArgumentNullException(
                    $"{nameof(loanSchedule.AnnualRate)} cannot be {loanSchedule.AnnualRate}");

            if ((loanInformation.ProductPortfolioType == ProductPortfolioType.MCA ||
                 loanInformation.ProductPortfolioType == ProductPortfolioType.MCALOC) && loanSchedule.FactorRate <= 0)
                throw new ArgumentNullException(
                    $"{nameof(loanSchedule.FactorRate)} cannot be {loanSchedule.FactorRate}");

            if (loanSchedule.FundedAmount <= 0)
                throw new ArgumentNullException(
                    $"{nameof(loanSchedule.FundedAmount)} cannot be {loanSchedule.FundedAmount}");

            if (loanSchedule.FundedDate == null)
                throw new ArgumentNullException($"{nameof(loanSchedule.FundedDate)} cannot be null");

            if (loanSchedule.LoanAmount <= 0)
                throw new ArgumentNullException(
                    $"{nameof(loanSchedule.LoanAmount)} cannot be {loanSchedule.LoanAmount}");

            if (loanSchedule.Tenure <= 0)
                throw new ArgumentNullException($"{nameof(loanSchedule.Tenure)} cannot be {loanSchedule.Tenure}");

            if (loanSchedule.ScheduleDetails == null || !loanSchedule.ScheduleDetails.Any())
                throw new ArgumentNullException($"{nameof(loanSchedule.ScheduleDetails)} cannot be null or blank");

            if (loanInformation.ProductPortfolioType != ProductPortfolioType.SCF &&
                loanSchedule.Tenure != loanSchedule.ScheduleDetails.Where(s => s.Installmentnumber > 0).Count())
            {
                throw new ArgumentException($"Amortization is invalid for {loanNumber}");
            }

            var bank = await BankDetailsRepository.GetBankDetails(loanNumber, null);
            if (bank == null)
                throw new ArgumentException($"No active banks found");
        }

        public async Task<List<ILoanScheduleDetails>> GetLoanModification(string loanNumber,
            ILoanModificationRequest loanModificationRequest)
        {
            if (string.IsNullOrWhiteSpace(loanNumber))
                throw new ArgumentNullException($"{nameof(loanNumber)} cannot be null");

            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new ArgumentException($"Loan details not found for loan number: {loanNumber}");

            var loanSchedules = await LoanScheduleRepository.GetLoanScheduleByLoanNumber(loanNumber);

            if (loanSchedules == null || !loanSchedules.Any())
                throw new NotFoundException($"Loan schedule not found for loan number: {loanNumber}");

            var currentLoanSchedule = loanSchedules.FirstOrDefault(s => s.IsCurrent);
            if (currentLoanSchedule == null)
                throw new NotFoundException($"No active loan schedule found for loan number: {loanNumber}");

            var loanAccounting =
                await AccrualBalanceService.GetLoanByScheduleVersion(loanNumber, currentLoanSchedule.ScheduleVersionNo);
            if (loanAccounting == null)
                throw new NotFoundException($"Loan accounting details not found for loan number: {loanNumber}");

            EnsureLoanModificationRequest(loanModificationRequest, loanInformation, currentLoanSchedule,
                loanAccounting);

            var loanScheduleRequest = new LoanScheduleRequest();
            loanScheduleRequest.LoanNumber = loanNumber;
            loanScheduleRequest.LoanAmount = loanModificationRequest.LoanAmount;
            loanScheduleRequest.AnnualRate = loanModificationRequest.AnnualRate;
            loanScheduleRequest.FactorRate = loanModificationRequest.FactorRate;
            loanScheduleRequest.PaybackTiers = loanModificationRequest.PaybackTiers;
            loanScheduleRequest.PaymentFrequency = loanModificationRequest.PaymentFrequency;
            loanScheduleRequest.Tenure = loanModificationRequest.Term;
            loanScheduleRequest.FirstPaymentDate = LoanConfiguration.ScheduleHolidays
                ? loanModificationRequest.ScheduleStartDate
                : GetNextScheduleForMCA(loanModificationRequest.PaymentFrequency,
                    loanModificationRequest.ScheduleStartDate.Date); // TODO: Need to calculate based on which date
            loanScheduleRequest.OriginalFirstPaymentDate = loanModificationRequest.ScheduleStartDate;
            loanScheduleRequest.InterestAdjustmentType = loanModificationRequest.InterestAdjustmentType;
            loanScheduleRequest.FundedDate =
                loanModificationRequest.ScheduleStartDate.AddDays(
                    -GetFrequencyDays(loanModificationRequest.PaymentFrequency));

            var loanScheduleDetails = new List<ILoanScheduleDetails>();
            var remainingAmount = (loanAccounting.PBOT.TotalPrincipalOutStanding);
            if (loanInformation.ProductPortfolioType == ProductPortfolioType.Installment)
            {
                loanScheduleRequest.Tenure = Convert.ToInt32(
                    remainingAmount / (loanModificationRequest.PaymentAmount > 0
                        ? loanModificationRequest.PaymentAmount
                        : RoundOff.Truncate(
                            PMT.Calculate_PMT(loanModificationRequest.LoanAmount, loanModificationRequest.Term,
                                InterestRate.Calculate_InterestRate(loanModificationRequest.PaymentFrequency.ToString(),
                                    loanModificationRequest.AnnualRate)), 2)));
                if (loanModificationRequest.PaymentAmount <= 0)
                {
                    loanModificationRequest.PaymentAmount = RoundOff.Truncate(
                        PMT.Calculate_PMT(loanModificationRequest.LoanAmount, loanModificationRequest.Term,
                            InterestRate.Calculate_InterestRate(loanModificationRequest.PaymentFrequency.ToString(),
                                loanModificationRequest.AnnualRate)), 2);
                }
            }

            if (loanModificationRequest.PaymentStreams != null)
            {
                loanScheduleRequest.PaymentStreams = loanModificationRequest.PaymentStreams;
                loanScheduleDetails.AddRange((await Amortize(loanScheduleRequest, loanModificationRequest.LoanAmount, 0,
                    0, loanModificationRequest.InterestToRecover, true)).Item1);
            }
            else
            {
                loanScheduleDetails.AddRange((await Amortize(loanScheduleRequest,
                    loanInformation.ProductPortfolioType == ProductPortfolioType.Installment
                        ? remainingAmount
                        : loanModificationRequest.LoanAmount, 0, loanModificationRequest.PaymentAmount,
                    loanModificationRequest.InterestToRecover, true)).Item1);
            }

            if (loanInformation.ProductPortfolioType == ProductPortfolioType.Installment)
            {
                loanScheduleDetails = loanScheduleDetails
                    .Where(s => s.Installmentnumber <= loanModificationRequest.Term).ToList();
                if (loanScheduleDetails.Select(p => p.CumulativePrincipalAmount).LastOrDefault() !=
                    loanModificationRequest.LoanAmount)
                {
                    var roundingParameters =
                        await GetParametersValues(loanInformation.LoanNumber, loanInformation.LoanProductId);
                    var last = loanScheduleDetails.LastOrDefault();
                    var diffAmount = RoundOff.Round(loanModificationRequest.LoanAmount - last.CumulativePrincipalAmount,
                        roundingParameters.Item2, roundingParameters.Item1);
                    last.PaymentAmount += diffAmount;
                    last.PrincipalAmount = last.PaymentAmount - last.InterestAmount;
                    last.CumulativePrincipalAmount += diffAmount;
                    last.CumulativePaymentAmount += diffAmount;
                    last.ClosingPrincipalOutStandingBalance -= diffAmount;
                }
            }

            return loanScheduleDetails;
        }

        public async Task<ILoanSchedule> SelectLoanModification(string loanNumber,
            ILoanModificationActivationRequest loanModificationActivationRequest)
        {
            var details = await EnsureValidLoanModificationRequest(loanNumber, loanModificationActivationRequest);
            var loanInformation = details.Item1;
            var currentLoanSchedule = details.Item2;
            var loanSchedules = details.Item3;
            var loanAccounting = details.Item4;
            var loanSchedule = GenerateLoanModSchedule(loanNumber, null, loanModificationActivationRequest,
                loanInformation, currentLoanSchedule, loanSchedules, loanAccounting);
            var parametersValues = await GetParametersValues(loanNumber, loanInformation.LoanProductId);
            if ((loanSchedule.RemainderDetails.RemainderAmount + parametersValues.Item3) < 0 ||
                (loanSchedule.RemainderDetails.RemainderInterest + parametersValues.Item3) < 0 ||
                loanSchedule.RemainderDetails.RemainderPrincipal < 0 ||
                (loanSchedule.RemainderDetails.RemainderFee + parametersValues.Item3) < 0)
                throw new ArgumentException($"Invalid remainder amount");

            await Task.Run(() => LoanScheduleRepository.Add(loanSchedule));

            loanInformation.CollectionStage = loanModificationActivationRequest.CollectionStage;
            await LoanOnboardingRepository.UpdateLoanInformation(loanInformation.LoanNumber, loanInformation);

            // Publish event.
            await EventHubClient.Publish(new LoanModCreated
            {
                EntityType = "loan",
                LoanNumber = loanSchedule.LoanNumber,
                ProductId = loanInformation.LoanProductId,
                LoanResponse = loanInformation,
                ScheduleResponse = loanSchedule,
                AccountingResponse = loanAccounting
            });

            // await UpdateCollectionStage(loanModificationActivationRequest, loanInformation);

            return loanSchedule;
        }

        public async Task SetLoanModActive(string loanNumber, string loanScheduleId)
        {
            if (string.IsNullOrWhiteSpace(loanNumber))
                throw new ArgumentNullException($"{nameof(loanNumber)} cannot be null");

            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
                throw new ArgumentNullException($"{nameof(loanInformation)} cannot be null");

            var loanSchedules = await LoanScheduleRepository.GetLoanScheduleByLoanNumber(loanInformation.LoanNumber);
            if (loanSchedules == null || !loanSchedules.Any())
                throw new NotFoundException($"Loan schedule not found for loan number: {loanNumber}");

            var currentLoanSchedule = loanSchedules.FirstOrDefault(s => s.IsCurrent);
            if (currentLoanSchedule == null)
                throw new NotFoundException($"No active loan schedule found for loan number: {loanNumber}");

            var loanModSchedule = loanSchedules.FirstOrDefault(s => s.Id == loanScheduleId);
            if (loanModSchedule == null)
                throw new NotFoundException($"No loan mod schedule found for loan number: {loanNumber}");

            var loanAccounting =
                await AccrualBalanceService.GetLoanByScheduleVersion(loanNumber, currentLoanSchedule.ScheduleVersionNo);
            if (loanAccounting == null)
                throw new NotFoundException($"Loan accounting details not found for loan number: {loanNumber}");

            if (loanModSchedule.RemainderDetails != null)
            {
                if (loanModSchedule.RemainderDetails.RemainderAmount > 0)
                {
                    if (loanAccounting.PBOT == null)
                        throw new ArgumentException($"Outstanding information not found");

                    if (loanAccounting.PBOT.TotalPrincipalOutStanding < loanModSchedule.ScheduleDetails
                            .Select(s => s.CumulativePrincipalAmount).LastOrDefault())
                        throw new ArgumentException($"Principal outstanding is less than schedule amount in loan mod");
                    else
                    {
                        if ((loanModSchedule.ScheduleDetails.Select(s => s.CumulativePrincipalAmount).LastOrDefault() +
                             loanModSchedule.RemainderDetails.RemainderPrincipal) >
                            loanAccounting.PBOT.TotalPrincipalOutStanding)
                        {
                            // Re-calculate the schedule and remainder amount
                            var loanScheduleRequest = new LoanScheduleRequest();
                            loanScheduleRequest.LoanNumber = loanNumber;
                            loanScheduleRequest.AnnualRate = loanModSchedule.AnnualRate;
                            loanScheduleRequest.FactorRate = loanModSchedule.FactorRate;
                            loanScheduleRequest.PaybackTiers = loanModSchedule.PaybackTiers;
                            loanScheduleRequest.PaymentFrequency = loanModSchedule.PaymentFrequency;
                            loanScheduleRequest.Tenure = loanModSchedule.Tenure;
                            loanScheduleRequest.FirstPaymentDate =
                                loanModSchedule.FirstPaymentDate.Time
                                    .Date; // TODO: Need to calculate based on which date
                            loanScheduleRequest.LoanAmount = loanModSchedule.LoanAmount;
                            loanScheduleRequest.InterestAdjustmentType = loanModSchedule.InterestAdjustmentType;
                            loanScheduleRequest.FundedDate = loanModSchedule.FundedDate.Time.Date;
                            loanScheduleRequest.OriginalFirstPaymentDate = loanModSchedule.ScheduleDetails != null
                                ? loanModSchedule.ScheduleDetails.Select(s => s.OriginalScheduleDate).FirstOrDefault()
                                : loanModSchedule.FirstPaymentDate.Time.Date;
                            var paymentStreams = loanModSchedule.PaymentStreams.ConvertAll(p => (PaymentStream)p);
                            loanScheduleRequest.PaymentStreams = paymentStreams;
                            var loanScheduleDetails = await Amortize(loanScheduleRequest, loanModSchedule.LoanAmount,
                                isLoanMod: true);
                            loanModSchedule.ScheduleDetails = loanScheduleDetails.Item1;

                            if (loanInformation.ProductPortfolioType != ProductPortfolioType.MCA &&
                                loanInformation.ProductPortfolioType != ProductPortfolioType.MCALOC)
                                loanModSchedule.OriginalScheduleDetails = loanScheduleDetails.Item1;
                            loanModSchedule.RemainderDetails.AsOfDate = TenantTime.Now;
                            loanModSchedule.RemainderDetails.RemainderPrincipal = loanAccounting.PBOT != null
                                ? (loanAccounting.PBOT.TotalPrincipalOutStanding - loanModSchedule.ScheduleDetails
                                       .Select(s => s.CumulativePrincipalAmount).LastOrDefault())
                                : 0;
                            loanModSchedule.RemainderDetails.RemainderInterest =
                                loanInformation.ProductPortfolioType == ProductPortfolioType.MCA ||
                                loanInformation.ProductPortfolioType == ProductPortfolioType.MCALOC
                                    ? loanAccounting.PBOT != null
                                        ? (loanAccounting.PBOT.TotalInterestOutstanding - loanModSchedule
                                               .ScheduleDetails
                                               .Select(s => s.CumulativeInterestAmount).LastOrDefault())
                                        : 0
                                    : 0;
                            loanModSchedule.RemainderDetails.RemainderFee = loanAccounting.PBOT != null
                                ? loanAccounting.PBOT.TotalFeeOutstanding
                                : 0;
                            loanModSchedule.RemainderDetails.RemainderAmount =
                                loanModSchedule.RemainderDetails.RemainderPrincipal +
                                loanModSchedule.RemainderDetails.RemainderInterest +
                                loanModSchedule.RemainderDetails.RemainderFee;
                        }
                    }
                }
            }

            if (loanInformation.ProductPortfolioType == ProductPortfolioType.MCA ||
                loanInformation.ProductPortfolioType == ProductPortfolioType.MCALOC)
                loanModSchedule.OriginalScheduleDetails = currentLoanSchedule.OriginalScheduleDetails;
            // Update other loan schedule IsCurrent as false.
            loanSchedules.ForEach(l => l.IsCurrent = false);
            foreach (var schedule in loanSchedules)
            {
                await Task.Run(() => LoanScheduleRepository.Update(schedule));
            }

            loanModSchedule.ModStatus = ModStatus.Active;
            loanModSchedule.IsCurrent = true;
            await Task.Run(() => LoanScheduleRepository.Update(loanModSchedule));

            // Loan accrual initiation.
            await AccrualBalanceService.InitializationAccrual(loanInformation.LoanNumber);

            // Publish event.
            await EventHubClient.Publish(new LoanScheduleCreated
            {
                EntityType = "loan",
                LoanNumber = loanInformation.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            // Publish event.
            await EventHubClient.Publish("LoanModActivated", new
            {
                EntityType = "loan",
                LoanNumber = loanInformation.LoanNumber,
                ProductId = loanInformation.LoanProductId,
                LoanResponse = loanInformation
            });
        }

        public async Task DeleteLoanMod(string loanNumber, string loanScheduleId)
        {
            if (string.IsNullOrWhiteSpace(loanNumber))
                throw new ArgumentNullException($"{nameof(loanNumber)} cannot be null");

            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
                throw new ArgumentNullException($"{nameof(loanInformation)} cannot be null");

            var loanSchedule = await LoanScheduleRepository.GetLoanScheduleById(loanNumber, loanScheduleId);
            if (loanSchedule == null)
                throw new ArgumentException($"No loan mod schedule found for {loanScheduleId}");

            if (loanSchedule.IsCurrent || loanSchedule.ModStatus == ModStatus.Active)
                throw new ArgumentException($"Cannot delete loan mod as it is already active");

            loanSchedule.ModStatus = ModStatus.Deleted;
            await Task.Run(() => LoanScheduleRepository.Update(loanSchedule));
            // Publish event.
            await EventHubClient.Publish(new LoanModDeleted
            {
                EntityType = "loan",
                LoanNumber = loanInformation.LoanNumber,
                ProductId = loanInformation.LoanProductId,
                LoanResponse = loanInformation,
                ScheduleResponse = loanSchedule
            });
        }

        public async Task<ILoanSchedule> UpdateLoanModification(string loanNumber, string loanScheduleId,
            ILoanModificationActivationRequest loanModificationActivationRequest)
        {
            var details =
                await EnsureValidLoanModificationRequest(loanNumber, loanModificationActivationRequest, loanScheduleId);
            var loanInformation = details.Item1;
            var currentLoanSchedule = details.Item2;
            var loanSchedules = details.Item3;
            var loanAccounting = details.Item4;
            var loanSchedule = GenerateLoanModSchedule(loanNumber, loanScheduleId, loanModificationActivationRequest,
                loanInformation, currentLoanSchedule, loanSchedules, loanAccounting);
            var parametersValues = await GetParametersValues(loanNumber, loanInformation.LoanProductId);
            if ((loanSchedule.RemainderDetails.RemainderAmount + parametersValues.Item3) < 0 ||
                (loanSchedule.RemainderDetails.RemainderInterest + parametersValues.Item3) < 0 ||
                loanSchedule.RemainderDetails.RemainderPrincipal < 0 ||
                (loanSchedule.RemainderDetails.RemainderFee + parametersValues.Item3) < 0)
                throw new ArgumentException($"Invalid remainder amount");

            await LoanScheduleRepository.UpdateLoanSchedule(loanNumber, loanScheduleId, loanSchedule);
            loanInformation.CollectionStage = loanModificationActivationRequest.CollectionStage;
            await LoanOnboardingRepository.UpdateLoanInformation(loanInformation.LoanNumber, loanInformation);

            // Publish event.
            await EventHubClient.Publish(new LoanModUpdated
            {
                EntityType = "loan",
                LoanNumber = loanSchedule.LoanNumber,
                ProductId = loanInformation.LoanProductId,
                LoanResponse = loanInformation,
                ScheduleResponse = loanSchedule,
                AccountingResponse = loanAccounting
            });

            return loanSchedule;
        }

        public async Task CloseOldLoan(LoanInformationRequest loanInformationRequest,
            ILoanInformation previousLoanDetails)
        {
            var bankDetails = await BankDetailsRepository.GetBankDetails(previousLoanDetails.LoanNumber, null);
            IFundingRequest request = new FundingRequest();
            request.PaymentAmount = loanInformationRequest.SettlementAmount;
            request.BankAccountNumber = bankDetails.AccountNumber;
            request.BankAccountType = bankDetails.AccountType.ToString();
            request.BankRTN = bankDetails.RoutingNumber;
            request.BorrowersName = bankDetails.BankHolderName;
            request.PaymentScheduleDate = TenantTime.Now;
            request.RequestedDate = TenantTime.Now;
            request.Status = Lookup.GetLookupEntry("requeststatus", "paymentsuccess").FirstOrDefault().Key;
            request.PaymentType = LMS.Payment.Processor.PaymentType.Scheduled;
            request.PaymentPurpose = "payoff";
            request.PaymentRail = LMS.Payment.Processor.PaymentRail.ACH;
            request.TransactionType = LMS.Payment.Processor.TransactionType.Credit;
            request.PaymentAccountDate = TenantTime.Now;
            var fundingData =
                await PaymentProcessorService.AddFundingRequest("loan", previousLoanDetails.LoanNumber, request);

            await PaymentProcessorService.AddFundingRequestAttempts("loan", previousLoanDetails.LoanNumber,
                new PaymentRequestAttempts
                {
                    ReferenceId = fundingData.ReferenceId,
                    AttemptDate = TenantTime.Now,
                    FundingRequestAttemptId = Guid.NewGuid().ToString("N"),
                    FundingRequestID = fundingData.Id,
                    LoanNumber = fundingData.LoanNumber,
                    BankAccountNumber = request.BankAccountNumber,
                    BankAccountType = request.BankAccountType,
                    BankRTN = request.BankRTN,
                    BorrowersName = request.BorrowersName,
                    ProviderSubmissionDate = TenantTime.Now,
                    ProviderResponseStatus =
                        Lookup.GetLookupEntry("requeststatus", "paymentsuccess").FirstOrDefault().Key,
                    AttemptStatus = Lookup.GetLookupEntry("requeststatus", "paymentsuccess").FirstOrDefault().Key
                });
            IPaymentReceivedRequest paymentReq = new PaymentReceivedRequest();
            paymentReq.ProcessingDate = TenantTime.Now;
            paymentReq.EffectiveDate = TenantTime.Now;
            paymentReq.Action = "payoff";
            paymentReq.TotalAmount = loanInformationRequest.SettlementAmount;
            paymentReq.Description = "Renewal Settlement";
            paymentReq.IsRenewal = true;
            await AccrualBalanceService.ApplyPaymentWithTotalAmount(previousLoanDetails.LoanNumber, paymentReq);
        }

        public async Task<IPayOffFeeDiscount> AddPayOffDiscount(IPayOffFeeDiscount payoffDiscount)
        {
            if (payoffDiscount == null)
                throw new ArgumentException($"{payoffDiscount} can not be null");
            var loanInformation =
                await LoanOnboardingRepository.GetLoanInformationByLoanNumber(payoffDiscount.LoanNumber);
            if (loanInformation == null)
                throw new ArgumentException($"Loan details not found for loan number: {payoffDiscount.LoanNumber}");
            var product = await GetProductParameters(loanInformation.LoanProductId, payoffDiscount.LoanNumber);
            if (product == null)
                throw new ArgumentException(
                    $"Product details not found for product id : {loanInformation.LoanProductId} and loan number : {payoffDiscount.LoanNumber}");
            var payoffDiscountParameters = product.ProductFeeParameters.FirstOrDefault(f =>
                string.Equals(f.FeeType.ToLower(), "payoffdiscount", StringComparison.InvariantCultureIgnoreCase));
            var payoffSchedule =
                await PayOffFeeDiscountRepository.GetPayOffFeeDiscountSchedule(payoffDiscount.LoanNumber,
                    "payoffdiscount");

            if (payoffDiscountParameters != null && (payoffSchedule == null || payoffSchedule.Count == 0))
            {
                payoffDiscount.FeeDiscountId = payoffDiscountParameters.FeeId;
                payoffDiscount.FeeType = payoffDiscountParameters.FeeType;
                PayOffFeeDiscountRepository.Add(payoffDiscount);
            }
            else if (payoffSchedule != null && payoffSchedule.Count > 0)
            {
                var payoffDiscountObjcet = payoffSchedule.FirstOrDefault();
                payoffDiscountObjcet.PayOffFeeScheduleDetails = payoffDiscount.PayOffFeeScheduleDetails;
                PayOffFeeDiscountRepository.Update(payoffDiscountObjcet);
            }

            return payoffDiscount;
        }

        public async Task<List<ILoanSchedule>> GetLoanSchedules(List<string> loanNumbers)
        {
            return await LoanScheduleRepository.GetLoanSchedules(loanNumbers);
        }

        #region Private Methods

        private async Task UpdateLoanEndDate(string loanNumber, TimeBucket loanEndDate,
            ILoanInformation loanInformation = null)
        {
            if (loanInformation == null)
            {
                loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            }

            if (loanInformation == null)
                throw new NotFoundException($"Loan information does not exist for loan number: {loanNumber}");

            loanInformation.LoanEndDate = loanEndDate;
            await LoanOnboardingRepository.UpdateLoanInformation(loanNumber, loanInformation);

            // Publish event.
            await EventHubClient.Publish(new LoanDetailsUpdated()
            {
                EntityType = "loan",
                LoanNumber = loanInformation.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });
        }

        private IMCAScheduleInputDetails GetMCAParameters(ILoanAmortizationRequest loanAmortizationRequest)
        {
            var param = new MCAScheduleInputDetails();
            param.PaymentFrequency = loanAmortizationRequest.PaymentFrequency;
            param.PaymentStartDate = loanAmortizationRequest.PaymentStartDate;
            param.RoundingDigit = loanAmortizationRequest.RoundingDigit;
            param.RoundingMethod = loanAmortizationRequest.RoundingMethod;
            param.PrincipalOutStanding = loanAmortizationRequest.LoanAmount * loanAmortizationRequest.FactorRate;
            param.LoanAmount = loanAmortizationRequest.LoanAmount;
            param.Term = loanAmortizationRequest.Term;
            if (loanAmortizationRequest.PaybackTier == null && loanAmortizationRequest.PaybackTier.Count == 0)
                throw new ArgumentNullException("PaybackTier is required");
            param.PaybackTier = loanAmortizationRequest.PaybackTier;
            return param;
        }

        private List<ILoanScheduleDetail> GetMCASchedule(IMCAScheduleInputDetails scheduleInputDetails,
            List<int> missedInstallments, ILoanScheduleDetails missedScheduleDetail, ILoanSchedule loanSchedule)
        {
            var ScheduleList = new List<LoanScheduleDetail>();
            var lastScheduleDetail = new LastScheduleDetails();
            InitializeMCAScheduleDetails(scheduleInputDetails, lastScheduleDetail);
            var RepayAmount = scheduleInputDetails.PrincipalOutStanding;
            var FinanceCharge = scheduleInputDetails.PrincipalOutStanding - scheduleInputDetails.LoanAmount;
            Dictionary<int, Foundation.Amortization.IPaybackTier> data =
                new Dictionary<int, Foundation.Amortization.IPaybackTier>();
            foreach (var payback in scheduleInputDetails.PaybackTier)
            {
                for (int i = 0; i < payback.NoOfPayments; i++)
                {
                    var scheduleDetails = new LoanScheduleDetail();
                    scheduleDetails.Installmentnumber = lastScheduleDetail.InstallmentNumber;
                    scheduleDetails.ScheduleDate = lastScheduleDetail.InstallmentNumber == 1
                        ? lastScheduleDetail.LastScheduleDate
                        : GetNextScheduleForMCA(scheduleInputDetails.PaymentFrequency,
                            lastScheduleDetail.LastScheduleDate);

                    //Payment details
                    if (missedInstallments.Any(m => m == scheduleDetails.Installmentnumber))
                    {
                        var missedDetails = loanSchedule.ScheduleDetails
                            .Where(c => c.Installmentnumber == scheduleDetails.Installmentnumber).FirstOrDefault();
                        if (missedDetails != null && missedDetails.PaidAmount > 0)
                        {
                            if (missedDetails.PaidAmount > missedDetails.InterestAmount)
                            {
                                scheduleDetails.InterestAmount = missedDetails.InterestAmount;
                                scheduleDetails.PrincipalAmount = RoundOff.Round(
                                    missedDetails.PaidAmount - missedDetails.InterestAmount,
                                    scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                            }
                            else
                            {
                                scheduleDetails.InterestAmount = missedDetails.PaidAmount;
                                scheduleDetails.PrincipalAmount = 0;
                            }
                        }
                        else
                        {
                            scheduleDetails.InterestAmount = 0;
                            scheduleDetails.PrincipalAmount = 0;
                        }

                        data.Add(scheduleDetails.Installmentnumber, payback);
                    }
                    else
                    {
                        //scheduleDetails.InterestAmount = lastScheduleDetail.InstallmentNumber == scheduleInputDetails.Term ? RoundOff.Round((FinanceCharge - lastScheduleDetail.CumulativeInterestAmount - missedScheduleDetail.InterestAmount), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod) : RoundOff.Round(payback.IsPercent ? FinanceCharge * (payback.FinanceCharge / payback.NoOfPayments) / 100 : (payback.FinanceCharge / payback.NoOfPayments), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                        //scheduleDetails.PrincipalAmount = lastScheduleDetail.InstallmentNumber == scheduleInputDetails.Term ? RoundOff.Round((scheduleInputDetails.LoanAmount - lastScheduleDetail.CumulativePrincipalAmount - missedScheduleDetail.PrincipalAmount), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod) : RoundOff.Round(payback.IsPercent ? scheduleInputDetails.LoanAmount * (payback.Principal / payback.NoOfPayments) / 100 : (payback.Principal / payback.NoOfPayments), scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                        scheduleDetails.InterestAmount =
                            lastScheduleDetail.InstallmentNumber == scheduleInputDetails.Term
                                ? loanSchedule.OriginalScheduleDetails.Select(s => s.InterestAmount).LastOrDefault()
                                : RoundOff.Round(
                                    payback.IsPercent
                                        ? FinanceCharge * (payback.FinanceCharge / payback.NoOfPayments) / 100
                                        : (payback.FinanceCharge / payback.NoOfPayments),
                                    scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                        scheduleDetails.PrincipalAmount =
                            lastScheduleDetail.InstallmentNumber == scheduleInputDetails.Term
                                ? loanSchedule.OriginalScheduleDetails.Select(s => s.PrincipalAmount).LastOrDefault()
                                : RoundOff.Round(
                                    payback.IsPercent
                                        ? scheduleInputDetails.LoanAmount * (payback.Principal / payback.NoOfPayments) /
                                          100
                                        : (payback.Principal / payback.NoOfPayments),
                                    scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    }

                    scheduleDetails.PaymentAmount =
                        RoundOff.Round(scheduleDetails.InterestAmount + scheduleDetails.PrincipalAmount,
                            scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    //Cumulative details
                    scheduleDetails.CumulativeInterestAmount = RoundOff.Round(
                        lastScheduleDetail.CumulativeInterestAmount + scheduleDetails.InterestAmount,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.CumulativePrincipalAmount = RoundOff.Round(
                        lastScheduleDetail.CumulativePrincipalAmount + scheduleDetails.PrincipalAmount,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.CumulativePaymentAmount = RoundOff.Round(
                        lastScheduleDetail.CumulativePaymentAmount + scheduleDetails.PaymentAmount,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    scheduleDetails.OpeningPrincipalOutStandingBalance = RoundOff.Round(
                        lastScheduleDetail.LastPrincipalOutStandingBalance, scheduleInputDetails.RoundingDigit,
                        scheduleInputDetails.RoundingMethod);
                    scheduleDetails.ClosingPrincipalOutStandingBalance = RoundOff.Round(
                        scheduleDetails.OpeningPrincipalOutStandingBalance - scheduleDetails.PrincipalAmount,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    ScheduleList.Add(scheduleDetails);
                    UpdateLastScheduleInfo(lastScheduleDetail, scheduleDetails);
                }
            }

            // Add if missed installment number is more than 1.
            if (missedInstallments.Count > 1)
            {
                foreach (var installment in missedInstallments)
                {
                    if (installment == missedScheduleDetail.Installmentnumber)
                        continue;

                    var details = loanSchedule.ScheduleDetails.Where(c => c.RefernceInstallmentnumber == installment)
                        .FirstOrDefault();
                    var missedDetails = loanSchedule.ScheduleDetails.Where(c => c.Installmentnumber == installment)
                        .FirstOrDefault();
                    var scheduleDetails = new LoanScheduleDetail();
                    scheduleDetails.Installmentnumber = details.Installmentnumber;
                    scheduleDetails.ScheduleDate = details.ScheduleDate;
                    scheduleDetails.OriginalScheduleDate = details.OriginalScheduleDate;
                    var payback = data[installment];
                    if (missedDetails.PaidAmount > 0)
                    {
                        var paidAmount = missedDetails.PaidAmount;
                        if (paidAmount > missedDetails.InterestAmount)
                        {
                            scheduleDetails.InterestAmount = 0;
                            paidAmount = RoundOff.Round(paidAmount - missedDetails.InterestAmount,
                                scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                            if (paidAmount > missedDetails.PrincipalAmount)
                            {
                                scheduleDetails.PrincipalAmount = RoundOff.Round(
                                    paidAmount - missedDetails.PrincipalAmount, scheduleInputDetails.RoundingDigit,
                                    scheduleInputDetails.RoundingMethod);
                            }
                            else
                            {
                                scheduleDetails.PrincipalAmount = RoundOff.Round(
                                    missedDetails.PrincipalAmount - paidAmount, scheduleInputDetails.RoundingDigit,
                                    scheduleInputDetails.RoundingMethod);
                            }
                        }
                        else
                        {
                            scheduleDetails.InterestAmount = RoundOff.Round(missedDetails.InterestAmount - paidAmount,
                                scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                            scheduleDetails.PrincipalAmount = missedDetails.PrincipalAmount;
                        }
                    }
                    else
                    {
                        scheduleDetails.InterestAmount = RoundOff.Round(
                            payback.IsPercent
                                ? FinanceCharge * (payback.FinanceCharge / payback.NoOfPayments) / 100
                                : (payback.FinanceCharge / payback.NoOfPayments), scheduleInputDetails.RoundingDigit,
                            scheduleInputDetails.RoundingMethod);
                        scheduleDetails.PrincipalAmount = RoundOff.Round(
                            payback.IsPercent
                                ? scheduleInputDetails.LoanAmount * (payback.Principal / payback.NoOfPayments) / 100
                                : (payback.Principal / payback.NoOfPayments), scheduleInputDetails.RoundingDigit,
                            scheduleInputDetails.RoundingMethod);
                    }

                    scheduleDetails.PaymentAmount =
                        RoundOff.Round(scheduleDetails.InterestAmount + scheduleDetails.PrincipalAmount,
                            scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    scheduleDetails.CumulativeInterestAmount = RoundOff.Round(
                        ScheduleList.LastOrDefault().CumulativeInterestAmount + scheduleDetails.InterestAmount,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.CumulativePrincipalAmount = RoundOff.Round(
                        ScheduleList.LastOrDefault().CumulativePrincipalAmount + scheduleDetails.PrincipalAmount,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);
                    scheduleDetails.CumulativePaymentAmount = RoundOff.Round(
                        ScheduleList.LastOrDefault().CumulativePaymentAmount + scheduleDetails.PaymentAmount,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    scheduleDetails.OpeningPrincipalOutStandingBalance =
                        ScheduleList.LastOrDefault().ClosingPrincipalOutStandingBalance;
                    scheduleDetails.ClosingPrincipalOutStandingBalance = RoundOff.Round(
                        scheduleDetails.OpeningPrincipalOutStandingBalance - scheduleDetails.PrincipalAmount,
                        scheduleInputDetails.RoundingDigit, scheduleInputDetails.RoundingMethod);

                    ScheduleList.Add(scheduleDetails);
                }
            }

            return new List<ILoanScheduleDetail>(ScheduleList);
        }

        private DateTime GetNextScheduleForMCA(PaymentFrequency paymentFrequency, DateTime lastScheduleDate)
        {
            var todayInfo =
                CalendarService.GetDate(lastScheduleDate.Year, lastScheduleDate.Month, lastScheduleDate.Day);
            if (todayInfo.Type == DateType.Holiday || todayInfo.Type == DateType.Weekend)
            {
                lastScheduleDate = todayInfo.NextBusinessDay.Date;
            }

            return lastScheduleDate;
        }

        private static void InitializeMCAScheduleDetails(IMCAScheduleInputDetails scheduleInputDetails,
            LastScheduleDetails lastSchdeuleDetail)
        {
            lastSchdeuleDetail.LastPrincipalOutStandingBalance = scheduleInputDetails.LoanAmount;
            lastSchdeuleDetail.InstallmentNumber = 1;
            lastSchdeuleDetail.LastScheduleDate = scheduleInputDetails.PaymentStartDate;
            lastSchdeuleDetail.CumulativeInterestAmount = 0;
            lastSchdeuleDetail.CumulativePrincipalAmount = 0;
            lastSchdeuleDetail.CumulativePaymentAmount = 0;
        }

        private static void UpdateLastScheduleInfo(ILastScheduleDetails lastSchdeuleDetail,
            ILoanScheduleDetail scheduleDetails)
        {
            lastSchdeuleDetail.CumulativePrincipalAmount = scheduleDetails.ClosingPrincipalOutStandingBalance;
            lastSchdeuleDetail.CumulativeInterestAmount = scheduleDetails.CumulativeInterestAmount;
            lastSchdeuleDetail.CumulativePrincipalAmount = scheduleDetails.CumulativePrincipalAmount;
            lastSchdeuleDetail.CumulativePaymentAmount = scheduleDetails.CumulativePaymentAmount;
            lastSchdeuleDetail.LastPrincipalOutStandingBalance = scheduleDetails.ClosingPrincipalOutStandingBalance;
            lastSchdeuleDetail.LastScheduleDate = scheduleDetails.ScheduleDate;
            lastSchdeuleDetail.InstallmentNumber++;
        }


        private DateTime GetNextSchedule(PaymentFrequency paymentFrequency, DateTime lastSchduleDate)
        {
            var nextSchdeuleDate = lastSchduleDate;
            switch (paymentFrequency)
            {
                case PaymentFrequency.Daily:
                    nextSchdeuleDate = lastSchduleDate.AddDays(1);
                    break;
                case PaymentFrequency.Weekly:
                    nextSchdeuleDate = lastSchduleDate.AddDays(Period_Constants.Weekly_Period_per_days);
                    break;
                case PaymentFrequency.BiWeekly:
                    nextSchdeuleDate = lastSchduleDate.AddDays(Period_Constants.BiWeekly_Period_per_days);
                    break;
                case PaymentFrequency.Monthly:
                    nextSchdeuleDate = lastSchduleDate.AddMonths(1);
                    break;
                case PaymentFrequency.Quarterly:
                    nextSchdeuleDate = lastSchduleDate.AddMonths(Period_Constants.Quarterly_Period_per_year);
                    break;
                case PaymentFrequency.SemiYearly:
                    nextSchdeuleDate = lastSchduleDate.AddMonths(Period_Constants.Semi_Yearly_Period_per_year);
                    break;
                case PaymentFrequency.Yearly:
                    nextSchdeuleDate = lastSchduleDate.AddYears(1);
                    break;
                default:
                    throw new InvalidOperationException("Invalid Payment Frequency");
            }

            return nextSchdeuleDate;
        }

        private async Task<Tuple<string, int, double, bool, string, string, int, Tuple<ScheduleType, int, bool, string, string>>> GetParametersValues(string loanNumber, string productId)
        {
            // Based on below lookup values get product parameter values and pass it to LoanAmortizationRequest.
            var rounding = Lookup.GetLookupEntry("parametername", "rounding");
            var roundingdigits = Lookup.GetLookupEntry("parametername", "roundingdigits");

            var productDetails = await GetProductParameters(productId, loanNumber);
            if (productDetails == null)
                throw new ArgumentException($"Product details not set for loan number: {loanNumber}");
            if (productDetails.ProductParameters == null)
                throw new ArgumentException($"Product parameters not set for loan number: {loanNumber}");

            var productParameter = productDetails.ProductParameters.ToList();
            var roundingMethod = productParameter.Where(p => p.Name == Convert.ToString(rounding["rounding"]))
                .Select(r => r.Value).FirstOrDefault();
            var roundingDigit = productParameter
                .Where(p => p.Name == Convert.ToString(roundingdigits["roundingdigits"])).Select(r => r.Value)
                .FirstOrDefault();
            var isDoubleForHolidayStr = productParameter.Where(r => r.Name == "IsDoubleScheduleForHoliday")
                .Select(r => r.Value).FirstOrDefault();
            var actualAmortizationType = productParameter.Where(r => r.Name == "ActualAmortizationType")
                .Select(r => r.Value).FirstOrDefault();
            var dailyAccrualMethod = productParameter.Where(r => r.Name == "DailyAccrualMethod").Select(r => r.Value)
                .FirstOrDefault();
            var maturityTermObj = productParameter.Where(r => r.Name == "MaturityTerm").FirstOrDefault();
            var ignoreDigit = productParameter.Where(p => p.Name == "IgnoreDigits").Select(r => r.Value)
                .FirstOrDefault();
            var numberOfDaysInYear = productParameter.Where(p => p.Name == "NumberOfDaysInYear").Select(r => r.Value)
                .FirstOrDefault();
            var brokenPeriodHandlingType = productParameter.Where(p => p.Name == "BrokenPeriodHandlingType").Select(r => r.Value)
                .FirstOrDefault();
            var brokenPeriodInterestType = productParameter.Where(p => p.Name == "BrokenPeriodInterestType").Select(r => r.Value)
                .FirstOrDefault();
            if (string.IsNullOrEmpty(ignoreDigit))
            {
                throw new ArgumentException($"Product parameters IgnoreDigits not set for  for product {productDetails.ProductId}");
            }

            bool IsDoubleForholiday = false;
            if (!string.IsNullOrWhiteSpace(isDoubleForHolidayStr) && isDoubleForHolidayStr.ToLower() == "true")
            {
                IsDoubleForholiday = true;
            }

            var scheduleTypeStr = productParameter.Where(p => p.Name == "ScheduleType").Select(r => r.Value).FirstOrDefault();

            ScheduleType scheduleType;
            if (string.IsNullOrWhiteSpace(scheduleTypeStr) || !Enum.TryParse(scheduleTypeStr, false, out scheduleType))
            {
                scheduleType = ScheduleType.None;
            }

            var rollPrincipal = productParameter.Where(p => p.Name == "Schedule_RollPrincipal").Select(r => r.Value).FirstOrDefault();

            bool scheduleRollPrincipal = false;
            if (!string.IsNullOrWhiteSpace(rollPrincipal) && rollPrincipal.ToLower() == "true")
            {
                scheduleRollPrincipal = true;
            }

            var noOfDaysInYear = 365;
            if (dailyAccrualMethod == "30/360")
            {
                noOfDaysInYear = 360;
            }

            if (!string.IsNullOrEmpty(numberOfDaysInYear))
            {
                noOfDaysInYear = Convert.ToInt32(numberOfDaysInYear);
            }

            return new Tuple<string, int, double, bool, string, string, int, Tuple<ScheduleType, int, bool, string, string>>(
                roundingMethod,
                Convert.ToInt32(roundingDigit),
                Convert.ToDouble(ignoreDigit),
                IsDoubleForholiday,
                actualAmortizationType,
                dailyAccrualMethod,
                maturityTermObj != null ? Convert.ToInt32(maturityTermObj.Value) : 0,
                new Tuple<ScheduleType, int, bool, string, string>(scheduleType, noOfDaysInYear, scheduleRollPrincipal, brokenPeriodHandlingType, brokenPeriodInterestType));
        }

        private async Task AddHistory(string loanNumber, string version)
        {
            var loanSchedule = await LoanScheduleRepository.GetLoanSchedule(loanNumber, version);
            var loanScheduleHistory = new LoanScheduleHistory(loanSchedule);
            loanScheduleHistory.UpdatedDateTime = TenantTime.Now.DateTime;
            await LoanScheduleHistoryRepository.AddHistory(loanScheduleHistory);
        }


        private Tuple<DateTime, DateTime, bool> GetNextSchedule(PaymentFrequency paymentFrequency,
            DateTime lastSchduleDate, bool? IsDoubleOnHoliday = false)
        {
            var nextSchdeuleDate = lastSchduleDate;
            switch (paymentFrequency)
            {
                case PaymentFrequency.Daily:
                    nextSchdeuleDate = lastSchduleDate.AddDays(1);
                    break;
                case PaymentFrequency.Weekly:
                    nextSchdeuleDate = lastSchduleDate.AddDays(Period_Constants.Weekly_Period_per_days);
                    break;
                case PaymentFrequency.BiWeekly:
                    nextSchdeuleDate = lastSchduleDate.AddDays(Period_Constants.BiWeekly_Period_per_days);
                    break;
                case PaymentFrequency.Monthly:
                    nextSchdeuleDate = lastSchduleDate.AddMonths(1);
                    break;
                case PaymentFrequency.Quarterly:
                    nextSchdeuleDate = lastSchduleDate.AddMonths(Period_Constants.Quarterly_Period_per_year);
                    break;
                case PaymentFrequency.SemiYearly:
                    nextSchdeuleDate = lastSchduleDate.AddMonths(Period_Constants.Semi_Yearly_Period_per_year);
                    break;
                case PaymentFrequency.Yearly:
                    nextSchdeuleDate = lastSchduleDate.AddYears(1);
                    break;
                default:
                    throw new InvalidOperationException("Invalid Payment Frequency");
            }

            var StoredDate = nextSchdeuleDate;
            bool IsHoliday = false;
            var todayInfo =
                CalendarService.GetDate(nextSchdeuleDate.Year, nextSchdeuleDate.Month, nextSchdeuleDate.Day);
            if (todayInfo.Type == DateType.Holiday || todayInfo.Type == DateType.Weekend)
            {
                IsStored = true;
                nextSchdeuleDate = todayInfo.NextBusinessDay.Date;
                if (IsDoubleOnHoliday.Value == true)
                {
                    var term = (todayInfo.NextBusinessDay.Date - todayInfo.Date).Days + 1;
                    var holidayDate = CalendarService.GetPeriodicDates(todayInfo.Date.Date, term, Periodicity.Daily);

                    foreach (var item in holidayDate)
                    {
                        if (item.Type == DateType.Holiday)
                        {
                            IsHoliday = true;
                            StoredDate = item.Date.Date;
                            break;
                        }
                    }
                }
            }

            return Tuple.Create(nextSchdeuleDate, StoredDate, IsHoliday);
        }

        private void ShiftScheduleForDoublePayment(ILoanSchedule loanSchedule, double nextScheduleInterest,
            double nextSchedulePrincipal, double nextScheduleFee, ILoanScheduleDetails schedule,
            ILoanScheduleDetails newSchedule)
        {
            var needToUpdateSchedules = loanSchedule.ScheduleDetails.Where(i =>
                    i.Installmentnumber >= schedule.Installmentnumber && i.IsMissed == false && i.IsPaid == false &&
                    i.IsProcessed == false && i.ScheduleDate > schedule.ScheduleDate.Date).OrderBy(i => i.ScheduleDate)
                .ToList();

            newSchedule.Installmentnumber = needToUpdateSchedules != null && needToUpdateSchedules.Count > 0
                ? needToUpdateSchedules.FirstOrDefault().Installmentnumber
                : schedule.Installmentnumber + 1;
            newSchedule.RefernceInstallmentnumber = schedule.Installmentnumber;
            newSchedule.InterestAmount = nextScheduleInterest;
            newSchedule.PrincipalAmount = nextSchedulePrincipal;
            newSchedule.FeeAmount = nextScheduleFee;
            newSchedule.PaymentAmount = nextScheduleInterest + nextSchedulePrincipal + nextScheduleFee;
            var nextInitiatedPayment = loanSchedule.ScheduleDetails
                .Where(x => x.Installmentnumber > schedule.Installmentnumber && x.IsMissed == false &&
                            x.IsProcessed == true).OrderByDescending(i => i.ScheduleDate).FirstOrDefault();
            if (nextInitiatedPayment == null)
            {
                nextInitiatedPayment = loanSchedule.ScheduleDetails
                    .Where(x => x.Installmentnumber > schedule.Installmentnumber && x.IsMissed == false &&
                                x.IsPaid == true).OrderByDescending(i => i.ScheduleDate).FirstOrDefault();
            }

            // var loanRequest =new LoanScheduleRequest { PaymentFrequency = loanSchedule.PaymentFrequency };
            // newSchedule.ScheduleDate = GetFirstPaymentDateBasedOnFrequency (loanRequest, new TimeBucket (nextInitiatedPayment != null ? nextInitiatedPayment.OriginalScheduleDate:schedule.OriginalScheduleDate)).Value;
            // //  newSchedule.ScheduleDate = GetNextSchedule(loanSchedule.PaymentFrequency, schedule.ScheduleDate);
            // newSchedule.OriginalScheduleDate =  loanRequest.OriginalFirstPaymentDate.Date;
            var sameDaySchedule = loanSchedule.ScheduleDetails.Where(x => x.ScheduleDate == schedule.ScheduleDate.Date)
                .ToList();
            var latestSchedule = needToUpdateSchedules.FirstOrDefault();
            int installment = newSchedule.Installmentnumber;
            if (sameDaySchedule.Count > 1 &&
                sameDaySchedule.Any(x => x.Installmentnumber == latestSchedule.RefernceInstallmentnumber))
            {
                newSchedule.ScheduleDate = latestSchedule.ScheduleDate;
                newSchedule.OriginalScheduleDate = latestSchedule.OriginalScheduleDate;
                //update installment number of rest of the schedule
                foreach (var item in needToUpdateSchedules)
                {
                    item.Installmentnumber = installment + 1;
                    installment = item.Installmentnumber;
                }
            }
            else
            {
                var nextScheduleDate = nextInitiatedPayment != null
                    ? nextInitiatedPayment.OriginalScheduleDate
                    : (sameDaySchedule.Count > 1
                        ? sameDaySchedule.LastOrDefault().ScheduleDate
                        : schedule.OriginalScheduleDate);
                var scheduleDate = GetNextSchedule(loanSchedule.PaymentFrequency, nextScheduleDate, true);

                newSchedule.ScheduleDate = scheduleDate.Item1;
                newSchedule.OriginalScheduleDate = scheduleDate.Item3 ? scheduleDate.Item2 : newSchedule.ScheduleDate;
                DateTime updateDateTime = newSchedule.OriginalScheduleDate;
                int updateInstallment = newSchedule.Installmentnumber;
                // if (needToUpdateSchedules != null && needToUpdateSchedules.Count > 0) // need to ask
                // {
                //     if (newSchedule.ScheduleDate.Date < needToUpdateSchedules.FirstOrDefault().ScheduleDate.Date)
                //     {
                //         newSchedule.ScheduleDate = needToUpdateSchedules.FirstOrDefault().ScheduleDate;
                //         newSchedule.OriginalScheduleDate = needToUpdateSchedules.FirstOrDefault().OriginalScheduleDate;
                //         updateDateTime = newSchedule.OriginalScheduleDate;
                //     }
                // }
                foreach (var item in needToUpdateSchedules)
                {
                    // var loanDetails = new LoanScheduleRequest { PaymentFrequency = loanSchedule.PaymentFrequency };
                    // item.ScheduleDate = GetFirstPaymentDateBasedOnFrequency (loanDetails, new TimeBucket (updateDateTime)).Value;
                    // item.OriginalScheduleDate = loanSchedule.PaymentFrequency == PaymentFrequency.Daily?item.ScheduleDate :  loanDetails.OriginalFirstPaymentDate.Date;
                    var nextDate = GetNextSchedule(loanSchedule.PaymentFrequency, updateDateTime, true);
                    item.ScheduleDate = nextDate.Item1;
                    item.OriginalScheduleDate = nextDate.Item3 ? nextDate.Item2 : item.ScheduleDate;
                    item.Installmentnumber = updateInstallment + 1;

                    updateDateTime = item.OriginalScheduleDate.Date;
                    updateInstallment = item.Installmentnumber;
                }
            }

            loanSchedule.ScheduleDetails.Add(newSchedule);
        }

        private void EnsureValidApplicant(IApplicantDetails applicantDetails)
        {
            if (string.IsNullOrWhiteSpace(applicantDetails.PositionOfLoan))
                throw new ArgumentNullException($"{nameof(applicantDetails.PositionOfLoan)} cannot be null");

            if (applicantDetails.Addresses != null)
            {
                foreach (var address in applicantDetails.Addresses)
                {
                    EnsureValidAddress(address);
                }
            }

            if (applicantDetails.Emails != null)
            {
                foreach (var email in applicantDetails.Emails)
                {
                    EnsureValidEmail(email);
                }
            }

            if (applicantDetails.Phones != null)
            {
                foreach (var phone in applicantDetails.Phones)
                {
                    EnsureValidPhone(phone);
                }
            }

            EnsureValidName(applicantDetails.Name);
            EnsureValidPII(applicantDetails.PII);
        }

        private void EnsureValidBusiness(IBusinessDetails businessDetails)
        {
            if (string.IsNullOrWhiteSpace(businessDetails.BusinessName))
                throw new ArgumentNullException($"{nameof(businessDetails.BusinessName)} cannot be null");

            if (businessDetails.EstablishedDate.Time > TenantTime.Now)
                throw new ArgumentException($"{nameof(businessDetails.EstablishedDate)} cannot be future date");

            EnsureValidAddress(businessDetails.Address);
            EnsureValidEmail(businessDetails.Email);
            EnsureValidPhone(businessDetails.Phone);
        }

        private void EnsureValidAddress(IAddress address)
        {
            if (address == null)
                return;

            if (string.IsNullOrWhiteSpace(address.AddressLine1))
                throw new ArgumentNullException($"{nameof(address.AddressLine1)} cannot be null");

            if (string.IsNullOrWhiteSpace(address.City))
                throw new ArgumentNullException($"{nameof(address.City)} cannot be null");

            if (string.IsNullOrWhiteSpace(address.State))
                throw new ArgumentNullException($"{nameof(address.State)} cannot be null");

            if (string.IsNullOrWhiteSpace(address.Zip))
                throw new ArgumentNullException($"{nameof(address.Zip)} cannot be null");

            if (!Enum.IsDefined(typeof(AddressType), address.AddressType))
                throw new ArgumentNullException($"Invalid {nameof(address.AddressType)} value");
        }

        private void EnsureValidEmail(IEmail email)
        {
            if (email == null)
                return;

            if (string.IsNullOrWhiteSpace(email.EmailAddress))
                throw new ArgumentNullException($"{nameof(email.EmailAddress)} cannot be null");

            if (!Enum.IsDefined(typeof(EmailType), email.EmailType))
                throw new ArgumentNullException($"Invalid {nameof(email.EmailType)} value");
        }

        private void EnsureValidPhone(IPhone phone)
        {
            if (phone == null)
                return;

            if (string.IsNullOrWhiteSpace(phone.PhoneNo))
                throw new ArgumentNullException($"{nameof(phone.PhoneNo)} cannot be null");

            if (!Enum.IsDefined(typeof(PhoneType), phone.PhoneType))
                throw new ArgumentNullException($"Invalid {nameof(phone.PhoneType)} value");
        }

        private void EnsureValidName(IName name)
        {
            if (name == null)
                throw new ArgumentNullException($"{nameof(name)} cannot be null");

            if (string.IsNullOrWhiteSpace(name.First))
                throw new ArgumentNullException($"{nameof(name.First)} cannot be null");

            if (string.IsNullOrWhiteSpace(name.Last))
                throw new ArgumentNullException($"{nameof(name.Last)} cannot be null");

            if (string.IsNullOrWhiteSpace(name.Salutation))
                throw new ArgumentNullException($"{nameof(name.Salutation)} cannot be null");
        }

        private void EnsureValidPII(IPII pii)
        {
            if (pii == null)
                throw new ArgumentNullException($"{nameof(pii)} cannot be null");

            if (pii.DOB == null)
                throw new ArgumentNullException($"{nameof(pii.DOB)} cannot be null");

            if (pii.DOB >= TenantTime.Now)
                throw new ArgumentException($"{nameof(pii.DOB)} cannot future or current date");
        }

        private async Task<Tuple<List<ILoanSchedule>, ILoanInformation, string>> EnsureValidInput(
            ILoanScheduleRequest loanScheduleRequest, bool isUpdate = false)
        {
            loanScheduleRequest.ExpectedFirstPaymentDate = null;

            if (loanScheduleRequest == null)
                throw new ArgumentNullException($"{nameof(loanScheduleRequest)} cannot be null");

            if (string.IsNullOrWhiteSpace(loanScheduleRequest.LoanNumber))
                throw new ArgumentNullException($"{nameof(loanScheduleRequest.LoanNumber)} cannot be null");

            var loanInformation =
                await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanScheduleRequest.LoanNumber);

            if (loanInformation == null)
                throw new NotFoundException(
                    $"Loan information for {nameof(loanScheduleRequest.LoanNumber)} is not available");

            var loanSchedule = await LoanScheduleRepository.GetLoanScheduleByLoanNumber(loanScheduleRequest.LoanNumber);
            if (string.IsNullOrWhiteSpace(loanScheduleRequest.ScheduleVersionNo))
            {
                loanScheduleRequest.ScheduleVersionNo = "v1";
                try
                {
                    int scheduleCount = loanSchedule != null ? loanSchedule.Count() : 0;
                    loanScheduleRequest.ScheduleVersionNo = "v" + (scheduleCount + 1);
                }
                catch
                { }
            }


            if (isUpdate)
            {
                if (loanSchedule == null ||
                    loanSchedule.Any(l => l.ScheduleVersionNo != loanScheduleRequest.ScheduleVersionNo))
                    throw new NotFoundException(
                        $"Loan schedule for {nameof(loanScheduleRequest.LoanNumber)} is not available with provided schedule version number {loanScheduleRequest.ScheduleVersionNo}.");
            }
            else
            {
                if (loanSchedule != null && loanSchedule.Any() &&
                    loanSchedule.Any(l => l.ScheduleVersionNo == loanScheduleRequest.ScheduleVersionNo))
                    throw new ArgumentNullException(
                        $"Loan schedule for {nameof(loanScheduleRequest.LoanNumber)} is available with provided schedule version number {loanScheduleRequest.ScheduleVersionNo}. ");
            }

            if (string.IsNullOrWhiteSpace(loanScheduleRequest.ScheduledCreatedReason))
                throw new ArgumentNullException($"{nameof(loanScheduleRequest.ScheduledCreatedReason)} cannot be null");

            loanScheduleRequest.ScheduledCreatedReason = loanScheduleRequest.ScheduledCreatedReason.ToLower();
            var reason = Lookup.GetLookupEntry("schedulereasons", loanScheduleRequest.ScheduledCreatedReason);
            if (reason == null)
                throw new ArgumentException($"{loanScheduleRequest.ScheduledCreatedReason} is not a valid schedule reason");

            if (loanScheduleRequest.LoanAmount <= 0)
                throw new ArgumentNullException($"Invalid value for {nameof(loanScheduleRequest.LoanAmount)}");

            if (loanScheduleRequest.FundedAmount <= 0)
                throw new ArgumentNullException($"Invalid value for {nameof(loanScheduleRequest.FundedAmount)}");

            await EnsureValidFees(loanScheduleRequest.Fees, loanInformation.LoanNumber, loanInformation.LoanProductId);
            var productDetails =
                await GetProductParameters(loanInformation.LoanProductId, loanScheduleRequest.LoanNumber);
            if (productDetails == null)
                throw new ArgumentException(
                    $"Product details not set for loan number: {loanScheduleRequest.LoanNumber}");
            if (productDetails.ProductParameters == null)
                throw new ArgumentException(
                    $"Product parameters not set for loan number: {loanScheduleRequest.LoanNumber}");

            var productParameter = productDetails.ProductParameters.ToList();

            var scheduleType = productParameter.Where(r => r.Name == "IsDoubleScheduleForHoliday").Select(r => r.Value)
                .FirstOrDefault();
            bool IsDoubleForholiday = false;
            if (scheduleType != null && scheduleType.ToLower() == "true")
                IsDoubleForholiday = true;

            // Added Portfolio SCF
            switch (loanInformation.ProductPortfolioType)
            {
                case ProductPortfolioType.Installment:
                case ProductPortfolioType.Mortgage:
                case ProductPortfolioType.Open:
                case ProductPortfolioType.Revolving:
                case ProductPortfolioType.SCF:
                    EnsureValidInputForInstallment(loanInformation, loanScheduleRequest, loanSchedule, productDetails);
                    break;
                case ProductPortfolioType.MCA:
                    EnsureValidInputForMCA(loanInformation, loanScheduleRequest, loanSchedule, IsDoubleForholiday);
                    break;
                case ProductPortfolioType.LineOfCredit:
                    await EnsureValidInputForLOC(loanInformation, loanScheduleRequest, loanSchedule, productDetails, isUpdate);
                    break;
                case ProductPortfolioType.MCALOC:
                    await EnsureValidInputForMCALOC(loanInformation, loanScheduleRequest, loanSchedule, isUpdate);
                    break;
                default:
                    break;
            }

            if (loanScheduleRequest.FundedDate >= loanScheduleRequest.FirstPaymentDate.Value)
                throw new ArgumentException($"Invalid value for {nameof(loanScheduleRequest.FundedDate)}. Value of funded date needs to be less than first payment date.");

            var scheduleReason = string.Empty;
            reason.TryGetValue(loanScheduleRequest.ScheduledCreatedReason, out scheduleReason);
            return Tuple.Create(loanSchedule, loanInformation, scheduleReason);
        }

        private DateTime? GetFirstPaymentDateBasedOnFrequency(ILoanScheduleRequest loanScheduleRequest,
            TimeBucket onBoardedDate)
        {
            DateTime? firstPaymentDateTime = null;
            switch (loanScheduleRequest.PaymentFrequency)
            {
                case PaymentFrequency.Daily:
                    firstPaymentDateTime = onBoardedDate.Time.DateTime.AddDays(1);
                    break;
                case PaymentFrequency.Weekly:
                    firstPaymentDateTime = onBoardedDate.Time.DateTime.AddDays(7);
                    break;
                case PaymentFrequency.BiWeekly:
                    firstPaymentDateTime = onBoardedDate.Time.DateTime.AddDays(14);
                    break;
                case PaymentFrequency.Monthly:
                    firstPaymentDateTime = onBoardedDate.Time.DateTime.AddMonths(1);
                    break;
                case PaymentFrequency.Quarterly:
                    firstPaymentDateTime = onBoardedDate.Time.DateTime.AddMonths(3);
                    break;
                case PaymentFrequency.Yearly:
                    firstPaymentDateTime = onBoardedDate.Time.DateTime.AddYears(1);
                    break;
                case PaymentFrequency.SemiYearly:
                    firstPaymentDateTime = onBoardedDate.Time.DateTime.AddMonths(6);
                    break;
                default:
                    break;
            }

            var todayInfo = CalendarService.GetDate(firstPaymentDateTime.Value.Year, firstPaymentDateTime.Value.Month,
                firstPaymentDateTime.Value.Day);
            loanScheduleRequest.OriginalFirstPaymentDate = firstPaymentDateTime.Value;
            if (!LoanConfiguration.ScheduleHolidays)
            {
                if (todayInfo.Type == DateType.Holiday || todayInfo.Type == DateType.Weekend)
                {
                    firstPaymentDateTime = todayInfo.NextBusinessDay.Date;
                }
            }
            else
            {
                firstPaymentDateTime = firstPaymentDateTime.Value;
            }

            return firstPaymentDateTime;
        }

        private async Task EnsureValidInputForLOC(ILoanInformation loanInformation, ILoanScheduleRequest loanScheduleRequest,
                                        List<ILoanSchedule> loanSchedules, ProductDetails productDetails, bool isUpdate = false)
        {
            if (loanScheduleRequest.CreditLimit <= 0)
                throw new ArgumentNullException($"Invalid value for {nameof(loanScheduleRequest.CreditLimit)}");

            double BusinessMaxLimit = loanInformation.PrimaryBusinessDetails.BusinessMaxLimitAmount;
            if (!LoanConfiguration.PrimaryBusinessMaxLimit)
            {
                BusinessMaxLimit = BusinessMaxLimit + loanInformation.BusinessDetails.Select(b => b.BusinessMaxLimitAmount).Sum();
            }

            if (BusinessMaxLimit > 0 && BusinessMaxLimit < loanScheduleRequest.CreditLimit)
            {
                loanScheduleRequest.CreditLimit = BusinessMaxLimit;
            }

            if (string.IsNullOrWhiteSpace(loanScheduleRequest.DrawDownNumber))
            {
                throw new ArgumentException($"{nameof(loanScheduleRequest.DrawDownNumber)} is mandatory");
            }

            if (loanScheduleRequest.ScheduledCreatedReason != LoanConfiguration.ScheduleReamortizeReason)
            {
                if (!isUpdate && loanInformation.DrawDownDetails != null && loanInformation.DrawDownDetails.Any(d => d.DrawDownNumber == loanScheduleRequest.DrawDownNumber))
                {
                    throw new ArgumentException($"Drawdown details for drawdown number : {loanScheduleRequest.DrawDownNumber} is already available.");
                }
            }

            var loanStatus = await GetLoanStatus(loanInformation.LoanNumber, loanInformation);
            if (isUpdate)
            {
                if (loanStatus != null && loanStatus.Code != LoanConfiguration.Statuses["Created"])
                    throw new ArgumentException($"Loan is in InService status, hence cannot update any loan schedule.");
            }
            else
            {
                if (loanSchedules != null && loanSchedules.Any())
                {
                    if (loanStatus != null && loanStatus.Code != LoanConfiguration.Statuses["InService"])
                        throw new ArgumentException($"Previous loan schedule is not yet started as Loan is in Created status.");
                }
            }

            var expectedFirstPaymentDate = GetFirstPaymentDateBasedOnFrequency(
                loanScheduleRequest,
                new TimeBucket(loanScheduleRequest.FundedDate));
            if (expectedFirstPaymentDate == null)
                throw new ArgumentNullException($"Invalid value for {nameof(loanScheduleRequest.FundedDate)}");

            Tuple<DateTimeOffset?, DateTimeOffset?> firstPaymentDate = null;
            if (!string.IsNullOrEmpty(loanInformation.FunderId))
            {
                firstPaymentDate = GetFirstPaymentDateBasedOnBillingDate(loanInformation, loanScheduleRequest, productDetails);
            }

            if (loanScheduleRequest.FirstPaymentDate.HasValue)
            {
                loanScheduleRequest.OriginalFirstPaymentDate = loanScheduleRequest.FirstPaymentDate.Value;
            }
            else
            {
                if (firstPaymentDate != null && firstPaymentDate.Item1 != null && firstPaymentDate.Item2 != null)
                {
                    loanScheduleRequest.OriginalFirstPaymentDate = firstPaymentDate.Item1.Value;
                    loanScheduleRequest.FirstPaymentDate = firstPaymentDate.Item1.Value;
                    loanScheduleRequest.ExpectedFirstPaymentDate = firstPaymentDate.Item2;

                    var todayInfo = CalendarService.GetDate(firstPaymentDate.Item1.Value.Year,
                        firstPaymentDate.Item1.Value.Month, firstPaymentDate.Item1.Value.Day);

                    if (!LoanConfiguration.ScheduleHolidays)
                    {
                        if (todayInfo.Type == DateType.Holiday || todayInfo.Type == DateType.Weekend)
                        {
                            loanScheduleRequest.FirstPaymentDate = todayInfo.NextBusinessDay.Date;
                        }
                    }
                }
                else
                {
                    loanScheduleRequest.FirstPaymentDate = expectedFirstPaymentDate.Value.Date;
                    if (loanScheduleRequest.ExpectedFirstPaymentDate == null)
                    {
                        loanScheduleRequest.ExpectedFirstPaymentDate = expectedFirstPaymentDate.Value.Date;
                    }
                }
            }
        }

        private void EnsureValidInputForInstallment(ILoanInformation loanInformation,
            ILoanScheduleRequest loanScheduleRequest, List<ILoanSchedule> loanSchedules, ProductDetails productDetails)
        {
            if ((loanInformation.ProductPortfolioType != ProductPortfolioType.MCA ||
                 loanInformation.ProductPortfolioType != ProductPortfolioType.MCALOC) &&
                loanScheduleRequest.AnnualRate <= 0)
                throw new ArgumentNullException($"Invalid value for {nameof(loanScheduleRequest.AnnualRate)}");

            var expectedFirstPaymentDate = GetFirstPaymentDateBasedOnFrequency(
                loanScheduleRequest,
                new TimeBucket(loanScheduleRequest.FundedDate));
            if (expectedFirstPaymentDate == null)
                throw new ArgumentNullException($"Invalid value for {nameof(loanScheduleRequest.FundedDate)}");
            Tuple<DateTimeOffset?, DateTimeOffset?> firstPaymentDate = null;
            if (!string.IsNullOrEmpty(loanInformation.FunderId))
            {
                firstPaymentDate =
                    GetFirstPaymentDateBasedOnBillingDate(loanInformation, loanScheduleRequest, productDetails);
            }

            if (loanScheduleRequest.FirstPaymentDate.HasValue)
            {
                loanScheduleRequest.OriginalFirstPaymentDate = loanScheduleRequest.FirstPaymentDate.Value;
            }
            else
            {
                if (firstPaymentDate != null && firstPaymentDate.Item1 != null && firstPaymentDate.Item2 != null)
                {
                    loanScheduleRequest.OriginalFirstPaymentDate = firstPaymentDate.Item1.Value;
                    loanScheduleRequest.FirstPaymentDate = firstPaymentDate.Item1.Value;
                    loanScheduleRequest.ExpectedFirstPaymentDate = firstPaymentDate.Item2;

                    var todayInfo = CalendarService.GetDate(firstPaymentDate.Item1.Value.Year, firstPaymentDate.Item1.Value.Month, firstPaymentDate.Item1.Value.Day);

                    if (loanInformation.ProductPortfolioType != ProductPortfolioType.SCF)
                    {
                        if (!LoanConfiguration.ScheduleHolidays)
                        {
                            if (todayInfo.Type == DateType.Holiday || todayInfo.Type == DateType.Weekend)
                            {
                                loanScheduleRequest.FirstPaymentDate = todayInfo.NextBusinessDay.Date;
                            }
                        }
                    }
                }
                else
                    loanScheduleRequest.FirstPaymentDate = expectedFirstPaymentDate.Value.Date;

                if (loanScheduleRequest.ExpectedFirstPaymentDate == null)
                    loanScheduleRequest.ExpectedFirstPaymentDate = expectedFirstPaymentDate.Value.Date;
            }
        }

        private void EnsureValidInputForMCA(ILoanInformation loanInformation, ILoanScheduleRequest loanScheduleRequest,
            List<ILoanSchedule> loanSchedules, bool IsDoubleForholiday)
        {
            if (loanInformation.ProductPortfolioType != ProductPortfolioType.MCA)
                return;

            if (loanScheduleRequest.FactorRate <= 0)
            {
                throw new ArgumentNullException($"Invalid value for {nameof(loanScheduleRequest.FactorRate)}");
            }

            if (loanScheduleRequest.PaybackTiers != null && loanScheduleRequest.PaybackTiers.Any())
            {
                //if (loanScheduleRequest.PaybackTiers.Count(p => p.IsPercent) != loanScheduleRequest.PaybackTiers.Count())
                //    throw new ArgumentException($"IsPercent value should match for all as same");

                if (loanScheduleRequest.PaybackTiers.Any(p => p.IsPercent) && loanScheduleRequest.PaybackTiers
                        .Where(p => p.IsPercent).Sum(p => p.FinanceCharge) != 100)
                {
                    throw new ArgumentException($"FinanceCharge should make sum up to 100 if IsPercent is true");
                }

                if (loanScheduleRequest.PaybackTiers.Any(p => p.IsPercent) &&
                    loanScheduleRequest.PaybackTiers.Where(p => p.IsPercent).Sum(p => p.Principal) != 100)
                {
                    throw new ArgumentException($"Principal should make sum up to 100 if IsPercent is true");
                }

                if (loanScheduleRequest.PaybackTiers.Sum(p => p.NoOfPayments) != loanScheduleRequest.Tenure)
                {
                    throw new ArgumentException($"NoOfPayments does not sum up to Tenure");
                }
            }

            var expectedFirstPaymentDate = GetFirstPaymentDateBasedOnFrequency(loanScheduleRequest,
                new TimeBucket(loanScheduleRequest.FundedDate));
            if (expectedFirstPaymentDate == null)
            {
                throw new ArgumentNullException($"Invalid value for {nameof(loanInformation.LoanFundedDate)}");
            }

            if (!loanScheduleRequest.FirstPaymentDate.HasValue)
            {
                if (!IsDoubleForholiday)
                {
                    loanScheduleRequest.FirstPaymentDate = expectedFirstPaymentDate;
                }
                else
                {
                    var scheduleDate = GetNextSchedule(loanScheduleRequest.PaymentFrequency,
                        loanScheduleRequest.FundedDate.Date, true);

                    loanScheduleRequest.FirstPaymentDate = scheduleDate.Item1;
                    loanScheduleRequest.OriginalFirstPaymentDate = scheduleDate.Item3
                        ? scheduleDate.Item2
                        : loanScheduleRequest.FirstPaymentDate.Value;
                }
            }
            else
            {
                var todayInfo = CalendarService.GetDate(loanScheduleRequest.FirstPaymentDate.Value.Year,
                    loanScheduleRequest.FirstPaymentDate.Value.Month, loanScheduleRequest.FirstPaymentDate.Value.Day);

                loanScheduleRequest.OriginalFirstPaymentDate = loanScheduleRequest.FirstPaymentDate.Value;
                if (!LoanConfiguration.ScheduleHolidays)
                {
                    if (todayInfo.Type == DateType.Holiday || todayInfo.Type == DateType.Weekend)
                    {
                        loanScheduleRequest.FirstPaymentDate = todayInfo.NextBusinessDay.Date;
                    }
                }
            }
        }

        private async Task EnsureValidInputForMCALOC(ILoanInformation loanInformation,
            ILoanScheduleRequest loanScheduleRequest, List<ILoanSchedule> loanSchedules, bool isUpdate = false)
        {
            if (loanInformation.ProductPortfolioType != ProductPortfolioType.MCALOC)
                return;

            if (loanScheduleRequest.FactorRate <= 0)
                throw new ArgumentNullException($"Invalid value for {nameof(loanScheduleRequest.FactorRate)}");

            if (loanScheduleRequest.PaybackTiers != null && loanScheduleRequest.PaybackTiers.Any())
            {

                if (loanScheduleRequest.PaybackTiers.Any(p => p.IsPercent) && loanScheduleRequest.PaybackTiers
                        .Where(p => p.IsPercent).Sum(p => p.FinanceCharge) != 100)
                    throw new ArgumentException($"FinanceCharge should make sum up to 100 if IsPercent is true");

                if (loanScheduleRequest.PaybackTiers.Any(p => p.IsPercent) &&
                    loanScheduleRequest.PaybackTiers.Where(p => p.IsPercent).Sum(p => p.Principal) != 100)
                    throw new ArgumentException($"Principal should make sum up to 100 if IsPercent is true");

                if (loanScheduleRequest.PaybackTiers.Sum(p => p.NoOfPayments) != loanScheduleRequest.Tenure)
                    throw new ArgumentException($"NoOfPayments does not sum up to Tenure");
            }

            if (string.IsNullOrWhiteSpace(loanScheduleRequest.DrawDownNumber))
            {
                throw new ArgumentException($"{nameof(loanScheduleRequest.DrawDownNumber)} is mandatory");
            }

            if (loanScheduleRequest.CreditLimit <= 0)
                throw new ArgumentNullException($"Invalid value for {nameof(loanScheduleRequest.CreditLimit)}");

            double BusinessMaxLimit = loanInformation.PrimaryBusinessDetails.BusinessMaxLimitAmount;
            if (!LoanConfiguration.PrimaryBusinessMaxLimit)
            {
                BusinessMaxLimit = BusinessMaxLimit +
                                   loanInformation.BusinessDetails.Select(b => b.BusinessMaxLimitAmount).Sum();
            }

            if (BusinessMaxLimit < loanScheduleRequest.CreditLimit)
            {
                loanScheduleRequest.CreditLimit = BusinessMaxLimit;
            }

            if (!isUpdate && loanInformation.DrawDownDetails != null && loanInformation.DrawDownDetails.Any(d => d.DrawDownNumber == loanScheduleRequest.DrawDownNumber))
                throw new ArgumentException($"Drawdown details for drawdown number : {loanScheduleRequest.DrawDownNumber} is already available.");

            var loanStatus = await GetLoanStatus(loanInformation.LoanNumber, loanInformation);
            if (isUpdate)
            {
                if (loanStatus != null && loanStatus.Code != LoanConfiguration.Statuses["Created"])
                    throw new ArgumentException($"Loan is in InService status, hence cannot update any loan schedule.");
            }
            else
            {
                if (loanSchedules != null && loanSchedules.Any())
                {
                    if (loanStatus != null && loanStatus.Code != LoanConfiguration.Statuses["InService"])
                        throw new ArgumentException($"Previous loan schedule is not yet started as Loan is in Created status.");
                }
            }

            var expectedFirstPaymentDate = GetFirstPaymentDateBasedOnFrequency(
                loanScheduleRequest,
                new TimeBucket(loanScheduleRequest.FundedDate));
            if (expectedFirstPaymentDate == null)
                throw new ArgumentNullException($"Invalid value for {nameof(loanScheduleRequest.FundedDate)}");
            if (!loanScheduleRequest.FirstPaymentDate.HasValue)
            {
                loanScheduleRequest.FirstPaymentDate = expectedFirstPaymentDate;
            }
            else
            {
                var todayInfo = CalendarService.GetDate(loanScheduleRequest.FirstPaymentDate.Value.Year,
                    loanScheduleRequest.FirstPaymentDate.Value.Month, loanScheduleRequest.FirstPaymentDate.Value.Day);

                loanScheduleRequest.OriginalFirstPaymentDate = loanScheduleRequest.FirstPaymentDate.Value;
                if (!LoanConfiguration.ScheduleHolidays)
                {
                    if (todayInfo.Type == DateType.Holiday || todayInfo.Type == DateType.Weekend)
                    {
                        loanScheduleRequest.FirstPaymentDate = todayInfo.NextBusinessDay.Date;
                    }
                }
            }
        }

        private async Task EnsureValidFees(List<Abstractions.IFee> fees, string loanNumber, string productId)
        {
            if (fees != null && fees.Any())
            {
                var productDetails = await GetProductParameters(productId, loanNumber);
                var productFeeParameters = productDetails.ProductFeeParameters.ToList();
                foreach (var fee in fees)
                {
                    if (string.IsNullOrWhiteSpace(fee.FeeName))
                        throw new ArgumentNullException($"{nameof(fee.FeeName)} cannot be null");

                    if (string.IsNullOrWhiteSpace(fee.FeeType))
                        throw new ArgumentNullException($"{nameof(fee.FeeType)} cannot be null");

                    fee.FeeType = fee.FeeType.ToLower();
                    var feeType = Lookup.GetLookupEntry("feetypes", fee.FeeType);

                    if (feeType == null)
                        throw new ArgumentException($"{fee.FeeType} is not a valid type of fee");

                    if (fee.FeeAmount <= 0)
                        throw new ArgumentNullException($"Invalid value for {nameof(fee.FeeAmount)}");

                    if (fee.Applied && fee.Accrue)
                        throw new ArgumentException(
                            $"{nameof(fee.Applied)} and {nameof(fee.Accrue)} cannot be both true, As fee is already applied and now you cannot accrue it. In case of invalid input please change the respective values");

                    if (fee.Accrue && fee.PaymentFrequencyAccrualAmount <= 0)
                        throw new ArgumentNullException(
                            $"Invalid value for {nameof(fee.PaymentFrequencyAccrualAmount)} when you have {nameof(fee.Accrue)} as {fee.Accrue}");

                    if (fee.Accrue && fee.DailyFrequencyAccrualAmount <= 0)
                        throw new ArgumentNullException(
                            $"Invalid value for {nameof(fee.DailyFrequencyAccrualAmount)} when you have {nameof(fee.Accrue)} as {fee.Accrue}");

                    // Ensure FeeName is exist in Product fees parameters
                    if (productFeeParameters.Where(f => f.Name == fee.FeeName).Count() <= 0)
                        throw new ArgumentException($"{nameof(fee.FeeName)} not matched with product fee parameters");

                    fee.Id = productFeeParameters.Where(f => f.Name == fee.FeeName).Select(r => r.Id).FirstOrDefault();
                }
            }
        }

        private async Task<Tuple<ILoanSchedule, IAccrual>> GenerateLoanSchedule(string loanNumber,
            ProductPortfolioType productPortfolioType, ILoanScheduleRequest loanScheduleRequest,
            List<ILoanSchedule> loanSchedules, ILoanInformation loanInformation, bool isUpdate = false)
        {
            ILoanSchedule loanSchedule = null;
            loanSchedule = loanScheduleRequest != null ? new LoanSchedule(loanScheduleRequest) : null;
            loanSchedule.LoanScheduleNumber =
                loanScheduleRequest.LoanNumber + "_" + loanScheduleRequest.ScheduleVersionNo;
            loanSchedule.ScheduleVersionDate = new TimeBucket(TenantTime.Now);
            loanSchedule.IsCurrent = true;
            IAccrual loanAccounting = null;
            double newFactorRate = loanSchedule.FactorRate;
            var parameters = await GetParametersValues(loanNumber, loanInformation.LoanProductId);
            List<ILoanScheduleDetails> loanScheduleDetails = new List<ILoanScheduleDetails>();
            double paymentAdded = 0;
            double principalAmount = 0;
            DateTime? calculatedFundedDate = null;
            switch (productPortfolioType)
            {
                case ProductPortfolioType.Installment:
                    if (loanInformation.LoanType != LoanType.Renewal &&
                        !string.IsNullOrEmpty(loanInformation.FunderId) && !loanScheduleRequest.IsMigration &&
                        loanScheduleRequest.FundedDate.Day != loanScheduleRequest.BillingDate)
                    {
                        var funder = AchService.GetFundingSourceById(loanInformation.FunderId);
                        if (funder != null && funder.BrokenPeriodTreatment ==
                            LendFoundry.MoneyMovement.Ach.Configuration.BrokenPeriodTreatment.Separate && loanScheduleRequest.PaymentFrequency != PaymentFrequency.Daily)
                        {
                            loanScheduleRequest.Tenure += 1;
                            loanSchedule.Tenure = loanScheduleRequest.Tenure;
                        }
                    }

                    break;
                case ProductPortfolioType.Mortgage:
                case ProductPortfolioType.Open:
                case ProductPortfolioType.Revolving:
                    break;
                case ProductPortfolioType.SCF:
                case ProductPortfolioType.LineOfCredit:
                    var schedule = loanSchedules.OrderByDescending(l => l.FundedDate.Time.DateTime).FirstOrDefault();
                    if (schedule != null)
                    {
                        loanAccounting = await AccrualBalanceService.GetLoanByScheduleVersion(loanNumber, schedule.ScheduleVersionNo);
                        ILoanScheduleDetails oldLoanSchedule = null;
                        var paymentInProcess = await PaymentProcessorService.GetInProgressPayments("loan", loanNumber);
                        if (paymentInProcess != null && paymentInProcess.Any())
                        {
                            var index = 0;
                            paymentInProcess = paymentInProcess.OrderBy(s => s.PaymentScheduleDate.Time.Date).ToList();
                            foreach (var payment in paymentInProcess)
                            {
                                if (payment.PaymentType != Payment.Processor.PaymentType.Scheduled)
                                    continue;

                                oldLoanSchedule = schedule.ScheduleDetails
                                    .Where(s => s.ScheduleDate.Date == payment.PaymentScheduleDate.Time.Date)
                                    .FirstOrDefault();
                                oldLoanSchedule.Installmentnumber = index--;
                                loanScheduleDetails.Add(oldLoanSchedule);
                                paymentAdded +=
                                    (payment.InterestAmount + payment.PrincipalAmount +
                                     payment.AdditionalInterestAmount) <= 0
                                        ? payment.PaymentAmount
                                        : (payment.InterestAmount + payment.PrincipalAmount +
                                           payment.AdditionalInterestAmount);
                                principalAmount += oldLoanSchedule.PrincipalAmount;
                            }
                        }

                        var nextPaymentSchedule = schedule.ScheduleDetails != null
                            ? schedule.ScheduleDetails.Where(s =>
                                s.ScheduleDate.Date > (oldLoanSchedule != null
                                    ? oldLoanSchedule.ScheduleDate.Date
                                    : loanScheduleRequest.FundedDate.Date)).FirstOrDefault()
                            : null;
                        if (nextPaymentSchedule != null)
                        {
                            loanSchedule.FirstPaymentDate = new TimeBucket(nextPaymentSchedule.ScheduleDate);
                            loanScheduleRequest.FirstPaymentDate = nextPaymentSchedule.ScheduleDate;
                            loanScheduleRequest.OriginalFirstPaymentDate = nextPaymentSchedule.OriginalScheduleDate;
                        }

                        var unPaidFees = await AccrualBalanceService.GetUnPaidFees(loanNumber, schedule.ScheduleVersionNo);
                        if (unPaidFees != null && unPaidFees.Any())
                        {
                            foreach (var fee in unPaidFees)
                            {
                                fee.ScheduleVersion = loanScheduleRequest.ScheduleVersionNo;
                                await AccrualBalanceService.UpdateFeeDetail(loanNumber, fee);
                            }
                        }
                    }

                    if (schedule != null && loanAccounting == null)
                        throw new ArgumentNullException($"Loan accounting not available for loan number: {loanNumber}");
                    if (loanAccounting != null)
                    {
                        if (loanAccounting.PBOT != null)
                        {
                            var loanAmount = parameters.Rest.Item3
                                ? loanScheduleRequest.LoanAmount + loanAccounting.PBOT.TotalPrincipalOutStanding -
                                  paymentAdded
                                : loanScheduleRequest.LoanAmount + loanAccounting.PBOT.TotalPrincipalOutStanding +
                                  loanAccounting.PBOT.TotalInterestOutstanding +
                                  loanAccounting.PBOT.AdditionalOutStandingInterest - paymentAdded;
                            if (loanScheduleRequest.ScheduledCreatedReason == LoanConfiguration.ScheduleReamortizeReason)
                            {
                                loanAmount = loanScheduleRequest.LoanAmount;
                                if(productPortfolioType == ProductPortfolioType.SCF)
                                {
                                    var lastSchedule = schedule.ScheduleDetails.OrderByDescending(s => s.ScheduleDate).FirstOrDefault();
                                    // TODO: Based on discussion of SCF maturity terms change this hardcoded value 30 to respective product parameter dynamic value
                                    calculatedFundedDate = lastSchedule.OriginalScheduleDate.AddDays(-(loanScheduleRequest.Tenure * 30));
                                }
                                
                            }
                            loanSchedule.LoanAmount = loanAmount;
                        }
                        else
                        {
                            loanSchedule.LoanAmount = loanScheduleRequest.LoanAmount;
                        }
                    }
                    else
                    {
                        loanSchedule.LoanAmount = loanScheduleRequest.LoanAmount;
                    }

                    loanSchedule.LoanAmount = RoundOff.Round(loanSchedule.LoanAmount, parameters.Item2, parameters.Item1);
                    if(productPortfolioType == ProductPortfolioType.LineOfCredit)
                    {
                        if (loanScheduleRequest.ScheduledCreatedReason == LoanConfiguration.ScheduleReamortizeReason)
                        {
                            loanSchedule.TotalDrawDown = RoundOff.Round(
                            (loanSchedules.Where(l => l.ScheduleVersionNo != loanScheduleRequest.ScheduleVersionNo)
                                 .Sum(l => l.LoanAmount)), parameters.Item2, parameters.Item1);
                        }
                        else
                        {
                            loanSchedule.TotalDrawDown = RoundOff.Round(
                            (loanSchedules.Where(l => l.ScheduleVersionNo != loanScheduleRequest.ScheduleVersionNo)
                                 .Sum(l => l.LoanAmount) + loanSchedule.LoanAmount), parameters.Item2,
                            parameters.Item1);
                        }
                    }
                    break;
                case ProductPortfolioType.MCA:
                    break;
                case ProductPortfolioType.MCALOC:
                    var latestschedule = loanSchedules.OrderByDescending(l => l.FundedDate.Time.DateTime)
                        .FirstOrDefault();
                    loanAccounting = null;
                    if (latestschedule != null)
                    {
                        loanAccounting =
                            await AccrualBalanceService.GetLoanByScheduleVersion(loanNumber,
                                latestschedule.ScheduleVersionNo);
                    }

                    if (latestschedule != null && loanAccounting == null)
                        throw new ArgumentNullException($"Loan accounting not available for loan number: {loanNumber}");
                    if (latestschedule != null && !string.IsNullOrWhiteSpace(latestschedule.ScheduleVersionNo))
                    {
                        var unPaidFeesForLOC =
                            await AccrualBalanceService.GetUnPaidFees(loanNumber, latestschedule.ScheduleVersionNo);
                        if (unPaidFeesForLOC != null && unPaidFeesForLOC.Any())
                        {
                            foreach (var fee in unPaidFeesForLOC)
                            {
                                fee.ScheduleVersion = loanScheduleRequest.ScheduleVersionNo;
                                await AccrualBalanceService.UpdateFeeDetail(loanNumber, fee);
                            }
                        }
                    }

                    if (loanAccounting != null)
                    {
                        loanSchedule.LoanAmount = loanAccounting.PBOT != null
                            ? (loanScheduleRequest.LoanAmount + loanAccounting.PBOT.TotalPrincipalOutStanding)
                            : loanScheduleRequest.LoanAmount;

                        var lastAmount = loanAccounting.PBOT.TotalPrincipalOutStanding;
                        var factorRate = latestschedule.FactorRate;
                        var remainingRepaymentAmount = lastAmount * factorRate;
                        var newRepayAmount = loanScheduleRequest.LoanAmount * loanScheduleRequest.FactorRate;
                        var totalAmount = remainingRepaymentAmount + newRepayAmount;
                        newFactorRate = (totalAmount / loanSchedule.LoanAmount);
                    }
                    else
                    {
                        loanSchedule.LoanAmount = loanScheduleRequest.LoanAmount;
                    }

                    loanSchedule.TotalDrawDown =
                        (loanSchedules.Where(l => l.ScheduleVersionNo != loanScheduleRequest.ScheduleVersionNo)
                             .Sum(l => l.LoanAmount) + loanSchedule.LoanAmount);
                    break;
                default:
                    break;
            }
            // incase of first interest only payment (mintifi)

            double interestAmount = 0;
            var originalFirstPaymentDate = loanScheduleRequest.OriginalFirstPaymentDate.Date;

            if (loanScheduleRequest.ExpectedFirstPaymentDate.HasValue)
                originalFirstPaymentDate = loanScheduleRequest.ExpectedFirstPaymentDate.Value.Date;

            if (loanScheduleRequest.InterestAdjustmentType != InterestAdjustmentType.None)
                interestAmount = CalculateInterestAmount(loanScheduleRequest.AnnualRate, loanScheduleRequest.LoanAmount,
                    loanScheduleRequest.FundedDate.Date, originalFirstPaymentDate,
                    loanScheduleRequest.PaymentFrequency, parameters.Item6, parameters.Item5);
            if (loanScheduleDetails != null && loanScheduleDetails.Any())
            {
                loanSchedule.ScheduleDetails = loanScheduleDetails;
                var amortization = await Amortize(loanScheduleRequest, loanSchedule.LoanAmount, newFactorRate);
                loanSchedule.ScheduleDetails.AddRange(amortization.Item1);
                loanSchedule.DailyScheduleDetails = amortization.Item2;
            }
            else
            {
                var remainingInterest = interestAmount;
                if (productPortfolioType == ProductPortfolioType.LineOfCredit || productPortfolioType == ProductPortfolioType.SCF)
                {
                    remainingInterest = (parameters.Rest.Item3 == true && loanAccounting != null && loanAccounting.PBOT != null)
                        ? loanAccounting.PBOT.TotalInterestOutstanding
                        : 0;
                }

                var amortization = await Amortize(loanScheduleRequest, loanSchedule.LoanAmount, newFactorRate,
                    remainingInterest: remainingInterest, calculatedFundedDate: calculatedFundedDate);
                loanSchedule.ScheduleDetails = amortization.Item1;
                loanSchedule.DailyScheduleDetails = amortization.Item2;
            }

            loanSchedule.LoanAmount += principalAmount;
            loanSchedule.FeeDetails = await BuildFeeSchedule(loanNumber, loanSchedule);
            await BuildPayoffSchedule(loanNumber, loanSchedule);

            // Set original loan schedule details.
            if (loanSchedule.OriginalScheduleDetails == null || !loanSchedule.OriginalScheduleDetails.Any())
                loanSchedule.OriginalScheduleDetails = loanSchedule.ScheduleDetails;
            return Tuple.Create(loanSchedule, loanAccounting);
        }

        private async Task<List<Abstractions.IFeeDetails>> BuildFeeSchedule(string loanNumber,
            ILoanSchedule loanSchedule)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
                throw new ArgumentException($"Loan details not found for loan number: {loanNumber}");

            var product = await GetProductParameters(loanInformation.LoanProductId, loanNumber);
            if (product == null)
                throw new ArgumentException(
                    $"Product details not found for product id : {loanInformation.LoanProductId} and loan number : {loanNumber}");

            var feeParameters = product.ProductFeeParameters.Where(f =>
                string.Equals(f.FeeType.ToLower(), "scheduled", StringComparison.InvariantCultureIgnoreCase));
            if (feeParameters == null)
                return null;

            var feeDetails = new List<Abstractions.IFeeDetails>();
            foreach (var feeParameter in feeParameters)
            {
                var ruleDefinition = await ProductRuleService.GetRuleDefinitionById(feeParameter.CalculationRule);
                if (ruleDefinition?.RuleDetail?.DERuleName == null)
                {
                    throw new ArgumentException($"Rule {feeParameter.CalculationRule} not found ");
                }

                var payload = new { LoanSchedule = loanSchedule };
                var executionResult =
                    DecisionEngineService.Execute<dynamic, List<Abstractions.FeeDetails>>(
                        ruleDefinition.RuleDetail.DERuleName, new { payload });
                feeDetails.AddRange(executionResult);
            }

            feeDetails.ForEach(f => f.Id = Guid.NewGuid().ToString("N"));
            return feeDetails;
        }

        private Tuple<DateTimeOffset?, DateTimeOffset?> GetFirstPaymentDateBasedOnBillingDate(ILoanInformation loan,
            ILoanScheduleRequest schedule, ProductDetails productDetails)
        {
            DateTimeOffset? firstPaymentDate = null;
            DateTimeOffset? originalPaymentDate = null;
            if (!string.IsNullOrWhiteSpace(LoanConfiguration.FirstPaymentDateRuleName))
            {
                dynamic payload = null;
                var businessDateType = GetBusinessDateType(schedule.PaymentFrequency);
                if (schedule.BillingDate.HasValue)
                {
                    payload = new
                    {
                        Schedule = schedule,
                        FunderId = loan.FunderId,
                        BillingDate = schedule.BillingDate,
                        BusinessDateType = businessDateType
                    };
                }
                else
                {
                    var billingProductParameter =
                        productDetails.ProductParameters != null && productDetails.ProductParameters.Any()
                            ? productDetails.ProductParameters.Where(p => p.Name == "BillingDate").FirstOrDefault()
                            : null;
                    if (billingProductParameter == null)
                        throw new ArgumentException(
                            $"Billing Date product parameter not found either billing date not found in onboarding details");
                    if (string.IsNullOrWhiteSpace(billingProductParameter.CalculationRuleId))
                        throw new ArgumentException(
                            $"Billing Date product parameter does not have required rule definition");
                    var ruleDefinition = ProductRuleService
                        .GetRuleDefinitionById(billingProductParameter.CalculationRuleId).Result;
                    if (ruleDefinition?.RuleDetail?.DERuleName == null)
                    {
                        throw new ArgumentException($"Rule {billingProductParameter.CalculationRuleId} not found ");
                    }

                    var executionResult =
                        DecisionEngineService.Execute<dynamic, JObject>(ruleDefinition.RuleDetail.DERuleName, new { });
                    JToken monthDate;
                    executionResult.TryGetValue("DayOfMonth", out monthDate);
                    JToken weekDate;
                    executionResult.TryGetValue("DayOfWeek", out weekDate);
                    var billingDate = businessDateType == "DayOfMonth" ? monthDate : weekDate;
                    schedule.BillingDate = Convert.ToInt32(billingDate);
                    payload = new
                    {
                        Schedule = schedule,
                        FunderId = loan.FunderId,
                        BillingDate = billingDate,
                        BusinessDateType = businessDateType
                    };
                }

                var firstPaymentDetails =
                    DecisionEngineService.Execute<dynamic, JObject>(LoanConfiguration.FirstPaymentDateRuleName,
                        new { payload });
                JToken brokenPeriodTreatment;
                firstPaymentDetails.TryGetValue("BrokenPeriodTreatment", out brokenPeriodTreatment);
                JToken paymentDate;
                firstPaymentDetails.TryGetValue("FirstPaymentDate", out paymentDate);
                JToken originalPaymentDateRaw;
                firstPaymentDetails.TryGetValue("OriginalPaymentDate", out originalPaymentDateRaw);

                var strBrokenPeriodTreatment = Convert.ToString(brokenPeriodTreatment);
                schedule.InterestAdjustmentType = InterestAdjustmentType.None;
                schedule.InterestHandlingType = InterestHandlingType.None;

                if (strBrokenPeriodTreatment != "")
                {
                    switch (strBrokenPeriodTreatment.ToLower())
                    {
                        case "notset":
                        case "ignore":
                            schedule.InterestAdjustmentType = InterestAdjustmentType.None;
                            schedule.InterestHandlingType = InterestHandlingType.None;
                            break;
                        case "firstemi":
                            schedule.InterestAdjustmentType = InterestAdjustmentType.FirstSchedule;
                            schedule.InterestHandlingType = InterestHandlingType.None;
                            break;
                        case "separate":
                            schedule.InterestAdjustmentType = InterestAdjustmentType.FirstInterestOnlySchedule;
                            schedule.InterestHandlingType = InterestHandlingType.InterestOnly;
                            break;
                    }
                }

                if (paymentDate != null)
                {
                    firstPaymentDate = Convert.ToDateTime(paymentDate);
                }

                if (originalPaymentDateRaw != null)
                {
                    originalPaymentDate = Convert.ToDateTime(originalPaymentDateRaw);
                }
            }

            return new Tuple<DateTimeOffset?, DateTimeOffset?>(firstPaymentDate, originalPaymentDate);
        }

        private async Task<ILoanSchedule> BuildPayoffSchedule(string loanNumber, ILoanSchedule loanSchedule)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
                throw new ArgumentException($"Loan details not found for loan number: {loanNumber}");
            var feeScheduleExists = await PayOffFeeDiscountRepository.GetPayOffFeeDiscountSchedule(loanNumber);
            if (feeScheduleExists != null && feeScheduleExists.Count > 0)
                return loanSchedule;
            var product = await GetProductParameters(loanInformation.LoanProductId, loanNumber);
            if (product == null)
                throw new ArgumentException(
                    $"Product details not found for product id : {loanInformation.LoanProductId} and loan number : {loanNumber}");

            var payoffFeeParameters = product.ProductFeeParameters.Where(f =>
                string.Equals(f.FeeType.ToLower(), "payofffee", StringComparison.InvariantCultureIgnoreCase));
            List<IPayOffFeeScheduleDetails> discountDetails = null;
            if (payoffFeeParameters != null)
            {
                foreach (var item in payoffFeeParameters)
                {
                    var payOffFeeDefinition = await ProductRuleService.GetRuleDefinitionById(item.CalculationRule);

                    if (payOffFeeDefinition?.RuleDetail?.DERuleName != null)
                    {
                        var payload = new { LoanSchedule = loanSchedule, LoanInformation = loanInformation };
                        var executionResult =
                            DecisionEngineService.Execute<dynamic, List<PayOffFeeScheduleDetails>>(
                                payOffFeeDefinition.RuleDetail.DERuleName, new { payload });
                        discountDetails = new List<IPayOffFeeScheduleDetails>();
                        discountDetails.AddRange(executionResult);

                        if (discountDetails != null && discountDetails.Count > 0)
                        {
                            IPayOffFeeDiscount discountSchedule = new PayOffFeeDiscount();
                            discountSchedule.LoanNumber = loanSchedule.LoanNumber;
                            discountSchedule.FeeDiscountId = item.FeeId;
                            discountSchedule.FeeType = item.FeeType;
                            discountSchedule.PayOffFeeScheduleDetails = discountDetails;
                            PayOffFeeDiscountRepository.Add(discountSchedule);
                        }
                    }
                }
            }

            var payoffDiscountParameters = product.ProductFeeParameters.Where(f =>
                string.Equals(f.FeeType.ToLower(), "payoffdiscount", StringComparison.InvariantCultureIgnoreCase));
            if (payoffDiscountParameters != null)
            {
                foreach (var item in payoffDiscountParameters)
                {
                    var ruleForDiscount = await ProductRuleService.GetRuleDefinitionById(item.CalculationRule);
                    if (ruleForDiscount?.RuleDetail?.DERuleName != null)
                    {
                        var payload = new { LoanSchedule = loanSchedule, LoanInformation = loanInformation };
                        var executionResult =
                            DecisionEngineService.Execute<dynamic, List<PayOffFeeScheduleDetails>>(
                                ruleForDiscount.RuleDetail.DERuleName, new { payload });
                        discountDetails = new List<IPayOffFeeScheduleDetails>();
                        discountDetails.AddRange(executionResult);
                        discountDetails.ForEach(x =>
                        {
                            x.PayOffFromDate = x.PayOffFromDate.Date;
                            x.PayOffToDate = x.PayOffToDate.Date;
                        });
                        if (discountDetails != null && discountDetails.Count > 0)
                        {
                            IPayOffFeeDiscount discountSchedule = new PayOffFeeDiscount();
                            discountSchedule.LoanNumber = loanSchedule.LoanNumber;
                            discountSchedule.FeeDiscountId = item.FeeId;
                            discountSchedule.FeeType = item.FeeType;
                            discountSchedule.PayOffFeeScheduleDetails = discountDetails;
                            PayOffFeeDiscountRepository.Add(discountSchedule);
                        }
                    }
                }
            }

            return loanSchedule;
        }

        private byte[] WriteToCsv<T>(IEnumerable<T> items)
        {
            try
            {
                using (var memoryStream = new MemoryStream())
                {
                    using (var streamWriter = new StreamWriter(memoryStream))
                    using (var csvWriter = new CsvWriter(streamWriter))
                    {
                        csvWriter.WriteRecords(items);
                    }

                    return memoryStream.ToArray();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void EnsureLoanModificationRequest(ILoanModificationRequest loanModificationRequest,
            ILoanInformation loanInformation, ILoanSchedule loanSchedule, IAccrual loanAccounting)
        {
            if (loanModificationRequest == null)
                throw new ArgumentNullException($"{nameof(loanModificationRequest)} cannot be null");

            if (!Enum.IsDefined(typeof(PaymentFrequency), loanModificationRequest.PaymentFrequency))
                throw new ArgumentNullException($"Invalid {nameof(loanModificationRequest.PaymentFrequency)} value");

            if (loanModificationRequest.Term <= 0)
                throw new ArgumentNullException($"{nameof(loanModificationRequest.Term)} cannot be null");

            if (loanModificationRequest.ScheduleStartDate == null)
                throw new ArgumentNullException($"{nameof(loanModificationRequest.ScheduleStartDate)} cannot be null");

            if (loanModificationRequest.PaybackTiers != null &&
                (loanInformation.ProductPortfolioType != ProductPortfolioType.MCALOC ||
                 loanInformation.ProductPortfolioType != ProductPortfolioType.MCA))
                throw new ArgumentException($"Payback tiers only works of MCA and MCALOC product");

            if (loanModificationRequest.PaymentAmount > 0 && loanModificationRequest.PaybackTiers != null)
                throw new ArgumentException($"Payback tiers won't work when payment amount is provided");

            if (loanModificationRequest.PaymentAmount > 0 && loanModificationRequest.PaymentStreams != null)
                throw new ArgumentException($"Payment stream won't work when payment amount is provided");

            if (loanModificationRequest.PaymentStreams != null && loanModificationRequest.PaybackTiers != null)
                throw new ArgumentException($"PaymentStream and PaybackTiers would not work together");

            if (loanModificationRequest.PaymentStreams != null)
            {
                if (loanModificationRequest.PaymentStreams.Sum(p => p.RepayablePercent) != 100)
                    throw new ArgumentException($"Repayable percent must total up to 100");

                if (loanModificationRequest.PaymentStreams.Sum(p => p.NoOfPayments) != loanModificationRequest.Term)
                    throw new ArgumentException($"Number of payment must total up to term provided");

                foreach (var paymentStream in loanModificationRequest.PaymentStreams)
                {
                    if (!Enum.IsDefined(typeof(PaymentFrequency), paymentStream.PaymentFrequency))
                        throw new ArgumentNullException($"Invalid {nameof(paymentStream.PaymentFrequency)} value");
                }
            }

            if (loanModificationRequest.FactorRate > 0 && loanModificationRequest.FinanceCharge > 0)
                throw new ArgumentException($"Factor rate and Finance charge cannot be provided together");

            if ((loanInformation.ProductPortfolioType == ProductPortfolioType.MCA ||
                 loanInformation.ProductPortfolioType == ProductPortfolioType.MCALOC) &&
                loanModificationRequest.InterestToRecover > 0)
                throw new ArgumentException(
                    $"{nameof(loanModificationRequest.InterestToRecover)} cannot be provided for {loanInformation.ProductPortfolioType}");

            if (loanModificationRequest.LoanAmount <= 0)
                loanModificationRequest.LoanAmount = loanAccounting.PBOT.TotalPrincipalOutStanding;

            if (loanModificationRequest.PaymentAmount > 0 &&
                (loanInformation.ProductPortfolioType == ProductPortfolioType.MCA ||
                 loanInformation.ProductPortfolioType == ProductPortfolioType.MCALOC))
            {
                if (loanModificationRequest.FactorRate > 0)
                    loanModificationRequest.LoanAmount =
                        (loanModificationRequest.PaymentAmount * loanModificationRequest.Term) /
                        loanModificationRequest.FactorRate;
                else
                    loanModificationRequest.LoanAmount =
                        (loanModificationRequest.PaymentAmount * loanModificationRequest.Term) -
                        loanModificationRequest.FinanceCharge;
            }

            if (loanModificationRequest.LoanAmount <= 0)
                throw new ArgumentException($"{nameof(loanModificationRequest.LoanAmount)} cannot be <= 0");

            if (loanInformation.ProductPortfolioType == ProductPortfolioType.MCA ||
                loanInformation.ProductPortfolioType == ProductPortfolioType.MCALOC)
            {
                if (loanModificationRequest.FactorRate <= 0 && loanModificationRequest.FinanceCharge <= 0 &&
                    loanModificationRequest.PaymentAmount <= 0)
                    loanModificationRequest.FactorRate = loanSchedule.FactorRate;
                else if (loanModificationRequest.FinanceCharge > 0)
                {
                    // var roundingParameters = await GetParametersValues(loanInformation.LoanNumber, loanInformation.LoanProductId);
                    //  loanModificationRequest.FactorRate = RoundOff.Round((loanModificationRequest.LoanAmount + loanModificationRequest.FinanceCharge) / loanModificationRequest.LoanAmount, roundingParameters.Item2, roundingParameters.Item1);
                    loanModificationRequest.FactorRate =
                        (loanModificationRequest.LoanAmount + loanModificationRequest.FinanceCharge) /
                        loanModificationRequest.LoanAmount;
                }
                else if (loanModificationRequest.PaymentAmount > 0 && loanModificationRequest.FinanceCharge <= 0 &&
                         loanModificationRequest.FactorRate <= 0)
                {
                    loanModificationRequest.FactorRate = 1;
                }
            }
            else
            {
                if (loanModificationRequest.AnnualRate <= 0)
                    loanModificationRequest.AnnualRate = loanSchedule.AnnualRate;
            }
        }

        private int CalculateTenure(int term, PaymentFrequency paymentFrequency)
        {
            switch (paymentFrequency)
            {
                case PaymentFrequency.Weekly:
                    return Convert.ToInt32(WeekConstant * term);
                case PaymentFrequency.BiWeekly:
                    return Convert.ToInt32((WeekConstant * term) / 2);
                default:
                    return term;
            }
        }

        private static int GetFrequencyDays(PaymentFrequency paymentFrequency)
        {
            return paymentFrequency == PaymentFrequency.Daily ? 1 :
                paymentFrequency == PaymentFrequency.Weekly ? 7 :
                paymentFrequency == PaymentFrequency.BiWeekly ? 14 :
                paymentFrequency == PaymentFrequency.Monthly ? 30 :
                0;
        }

        private async Task<Tuple<ILoanInformation, ILoanSchedule, List<ILoanSchedule>, IAccrual>>
            EnsureValidLoanModificationRequest(string loanNumber,
                ILoanModificationActivationRequest loanModificationActivationRequest, string loanScheduleId = null)
        {
            if (string.IsNullOrWhiteSpace(loanNumber))
                throw new ArgumentNullException($"{nameof(loanNumber)} cannot be null");

            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Loan details not found for loan number: {loanNumber}");

            if (loanModificationActivationRequest == null)
                throw new ArgumentNullException($"{nameof(loanModificationActivationRequest)} cannot be null");

            if (loanModificationActivationRequest.LoanModificationRequest == null)
                throw new ArgumentNullException(
                    $"{nameof(loanModificationActivationRequest.LoanModificationRequest)} cannot be null");

            if (loanModificationActivationRequest.LoanScheduleDetails == null ||
                !loanModificationActivationRequest.LoanScheduleDetails.Any())
                throw new ArgumentNullException(
                    $"{nameof(loanModificationActivationRequest.LoanScheduleDetails)} cannot be null");

            if (loanModificationActivationRequest.ActivationDate.HasValue)
            {
                if (loanModificationActivationRequest.ActivationDate.Value.Date > loanModificationActivationRequest
                        .LoanModificationRequest.ScheduleStartDate.Date)
                    throw new ArgumentException($"Invalid activation date");

                //if (loanInformation.ProductPortfolioType != ProductPortfolioType.MCA && loanInformation.ProductPortfolioType != ProductPortfolioType.MCALOC)
                //{
                //    var paymentFrequency = loanModificationActivationRequest.LoanModificationRequest.PaymentStreams != null ? loanModificationActivationRequest.LoanModificationRequest.PaymentStreams.Select(s => s.PaymentFrequency).FirstOrDefault() : loanModificationActivationRequest.LoanModificationRequest.PaymentFrequency;
                //    var expectedActivationDate = loanModificationActivationRequest.LoanModificationRequest.ScheduleStartDate.Date.AddDays(-GetFrequencyDays(paymentFrequency));
                //    if (expectedActivationDate.Date != loanModificationActivationRequest.ActivationDate.Value.Date)
                //        throw new ArgumentException($"Activation date should be based on {nameof(loanModificationActivationRequest.LoanModificationRequest.ScheduleStartDate)} and {nameof(loanModificationActivationRequest.LoanModificationRequest.PaymentFrequency)}");
                //}
            }

            loanModificationActivationRequest.ScheduledCreatedReason =
                loanModificationActivationRequest.ScheduledCreatedReason.ToLower();
            var reason = Lookup.GetLookupEntry("schedulereasons",
                loanModificationActivationRequest.ScheduledCreatedReason);
            if (reason == null)
                throw new ArgumentException(
                    $"{loanModificationActivationRequest.ScheduledCreatedReason} is not a valid schedule reason");

            loanModificationActivationRequest.ScheduledCreatedReason =
                reason[loanModificationActivationRequest.ScheduledCreatedReason];

            loanModificationActivationRequest.ModReason = loanModificationActivationRequest.ModReason.ToLower();
            var modReason = Lookup.GetLookupEntry("modreasons", loanModificationActivationRequest.ModReason);
            if (modReason == null)
                throw new ArgumentException($"{loanModificationActivationRequest.ModReason} is not a valid mod reason");

            loanModificationActivationRequest.ModReason = modReason[loanModificationActivationRequest.ModReason];

            if (loanModificationActivationRequest.LoanModificationRequest.LoanAmount > 0 &&
                loanModificationActivationRequest.LoanModificationRequest.LoanAmount !=
                loanModificationActivationRequest.LoanScheduleDetails.Select(s => s.CumulativePrincipalAmount)
                    .LastOrDefault())
                throw new ArgumentException($"Loan amount does not match with sum of schedule principal amount");

            var loanSchedules = await LoanScheduleRepository.GetLoanScheduleByLoanNumber(loanNumber);

            if (loanSchedules == null || !loanSchedules.Any())
                throw new NotFoundException($"Loan schedule not found for loan number: {loanNumber}");

            var currentLoanSchedule = loanSchedules.FirstOrDefault(s => s.IsCurrent);
            if (currentLoanSchedule == null)
                throw new NotFoundException($"No active loan schedule found for loan number: {loanNumber}");

            if (!string.IsNullOrWhiteSpace(loanScheduleId))
            {
                var exist = loanSchedules.FirstOrDefault(s => s.Id == loanScheduleId);
                if (exist == null)
                    throw new NotFoundException(
                        $"Loan schedule not found for loan number: {loanNumber} and schedule id: {loanScheduleId}");
            }
            else
            {
                var existingMods = loanSchedules.Where(s => s.ModStatus == ModStatus.Created).ToList();
                if (existingMods != null && existingMods.Any())
                    throw new ArgumentException(
                        $"Cannot create new mod as there is already one loan mod in created state");
            }

            var loanAccounting =
                await AccrualBalanceService.GetLoanByScheduleVersion(loanNumber, currentLoanSchedule.ScheduleVersionNo);
            if (loanAccounting == null)
                throw new ArgumentException($"Loan accounting details not found for loan number: {loanNumber}");

            EnsureLoanModificationRequest(loanModificationActivationRequest.LoanModificationRequest, loanInformation,
                currentLoanSchedule, loanAccounting);

            if (loanAccounting.PBOT.TotalPrincipalOutStanding < loanModificationActivationRequest.LoanScheduleDetails
                    .Select(s => s.CumulativePrincipalAmount).LastOrDefault())
                throw new ArgumentException(
                    $"Schedule is incorrect as outstanding information value is less than principal amount of schedule created");

            return Tuple.Create(loanInformation, currentLoanSchedule, loanSchedules, loanAccounting);
        }

        private ILoanSchedule GenerateLoanModSchedule(string loanNumber, string loanScheduleId,
            ILoanModificationActivationRequest loanModificationActivationRequest, ILoanInformation loanInformation,
            ILoanSchedule currentLoanSchedule, List<ILoanSchedule> loanSchedules, IAccrual loanAccounting)
        {
            ILoanSchedule updateModSchedule = null;
            if (!string.IsNullOrWhiteSpace(loanScheduleId))
            {
                updateModSchedule = loanSchedules.Where(s => s.Id == loanScheduleId).FirstOrDefault();
            }

            var loanSchedule = new LoanSchedule();
            loanSchedule.Id = loanScheduleId;
            loanSchedule.ScheduleDetails = loanModificationActivationRequest.LoanScheduleDetails;
            loanSchedule.AnnualRate = loanModificationActivationRequest.LoanModificationRequest.AnnualRate;
            loanSchedule.FactorRate = loanModificationActivationRequest.LoanModificationRequest.FactorRate;
            loanSchedule.PaymentFrequencyRate = InterestRate.Calculate_InterestRate(
                loanModificationActivationRequest.LoanModificationRequest.PaymentFrequency.ToString(),
                loanModificationActivationRequest.LoanModificationRequest.AnnualRate);
            loanSchedule.DailyRate =
                loanModificationActivationRequest.LoanModificationRequest.PaymentFrequency == PaymentFrequency.Monthly
                    ? (loanSchedule.PaymentFrequencyRate / 30)
                    : loanModificationActivationRequest.LoanModificationRequest.PaymentFrequency ==
                      PaymentFrequency.Weekly
                        ? (loanSchedule.PaymentFrequencyRate / 7)
                        : loanModificationActivationRequest.LoanModificationRequest.PaymentFrequency ==
                          PaymentFrequency.BiWeekly
                            ? (loanSchedule.PaymentFrequencyRate / 14)
                            : loanSchedule.PaymentFrequencyRate;
            loanSchedule.FeeDetails = currentLoanSchedule.FeeDetails;
            loanSchedule.Fees = null; // TODO: Do we need to take previous.
            loanSchedule.CreditLimit = currentLoanSchedule.CreditLimit;
            loanSchedule.FirstPaymentDate = new TimeBucket(loanModificationActivationRequest.LoanScheduleDetails
                .Select(c => c.ScheduleDate).FirstOrDefault());
            loanSchedule.FundedAmount = loanModificationActivationRequest.LoanModificationRequest.LoanAmount;
            loanSchedule.FundedDate = loanModificationActivationRequest.ActivationDate.HasValue
                ? new TimeBucket(loanModificationActivationRequest.ActivationDate.Value)
                : null;
            loanSchedule.LoanAmount = loanModificationActivationRequest.LoanModificationRequest.LoanAmount;
            loanSchedule.LoanNumber = loanInformation.LoanNumber;
            loanSchedule.OriginalScheduleDetails = loanModificationActivationRequest.LoanScheduleDetails;
            loanSchedule.PaybackTiers = loanModificationActivationRequest.LoanModificationRequest.PaybackTiers;
            loanSchedule.PaymentFrequency = loanModificationActivationRequest.LoanModificationRequest.PaymentFrequency;
            loanSchedule.ScheduledCreatedReason = loanModificationActivationRequest.ScheduledCreatedReason;
            loanSchedule.ModReason = loanModificationActivationRequest.ModReason;
            loanSchedule.ScheduleNotes = loanModificationActivationRequest.ScheduleNotes;
            loanSchedule.ScheduleVersionDate = new TimeBucket(TenantTime.Now);
            loanSchedule.ScheduleVersionNo = updateModSchedule != null ? updateModSchedule.ScheduleVersionNo :
                loanSchedules != null && loanSchedules.Any() ? "v" + (loanSchedules.Count() + 1) : "v1";
            loanSchedule.LoanScheduleNumber = updateModSchedule != null
                ? updateModSchedule.LoanScheduleNumber
                : loanSchedule.LoanNumber + "_" + loanSchedule.ScheduleVersionNo;
            loanSchedule.Tenure = loanModificationActivationRequest.LoanModificationRequest.Term;
            loanSchedule.TotalDrawDown = currentLoanSchedule.TotalDrawDown;
            loanSchedule.IsCurrent = false;
            loanSchedule.ModStatus = ModStatus.Created;
            loanSchedule.InterestAdjustmentType =
                loanModificationActivationRequest.LoanModificationRequest.InterestAdjustmentType;
            loanSchedule.PaymentFailure = loanModificationActivationRequest.PaymentFailure;
            loanSchedule.NextEscalationStage = loanModificationActivationRequest.NextEscalationStage;
            loanSchedule.RemainderDetails = new RemainderDetails();
            loanSchedule.RemainderDetails.AsOfDate = TenantTime.Now;
            loanSchedule.RemainderDetails.RemainderPrincipal = loanAccounting.PBOT != null
                ? (loanAccounting.PBOT.TotalPrincipalOutStanding - loanModificationActivationRequest.LoanScheduleDetails
                       .Select(s => s.CumulativePrincipalAmount).LastOrDefault())
                : 0;
            loanSchedule.RemainderDetails.RemainderInterest =
                loanInformation.ProductPortfolioType == ProductPortfolioType.MCA ||
                loanInformation.ProductPortfolioType == ProductPortfolioType.MCALOC
                    ? (loanAccounting.PBOT != null
                        ? (loanAccounting.PBOT.TotalInterestOutstanding - loanModificationActivationRequest
                               .LoanScheduleDetails.Select(s => s.CumulativeInterestAmount).LastOrDefault())
                        : 0)
                    : (loanAccounting.PBOT != null
                        ? (loanAccounting.PBOT.TotalInterestOutstanding - loanModificationActivationRequest
                               .LoanModificationRequest.InterestToRecover)
                        : 0);
            loanSchedule.RemainderDetails.RemainderFee =
                loanAccounting.PBOT != null ? loanAccounting.PBOT.TotalFeeOutstanding : 0;
            loanSchedule.RemainderDetails.RemainderAmount = loanSchedule.RemainderDetails.RemainderPrincipal +
                                                            loanSchedule.RemainderDetails.RemainderInterest +
                                                            loanSchedule.RemainderDetails.RemainderFee;
            loanSchedule.PaymentStreams =
                loanModificationActivationRequest.LoanModificationRequest.PaymentStreams != null
                    ? loanModificationActivationRequest.LoanModificationRequest.PaymentStreams.ToList<IPaymentStream>()
                    : null;
            loanSchedule.LoanModType = loanModificationActivationRequest.LoanModificationRequest.LoanModType;
            loanSchedule.InterestToRecover =
                loanModificationActivationRequest.LoanModificationRequest.InterestToRecover;
            return loanSchedule;
        }

        private double CalculateInterestAmount(double annualRate, double outstandingAmount, DateTime onBoardedDate,
            DateTime firstPaymentDate, PaymentFrequency frequency, string dailyAccrualMethod, string actualAmortizationType)
        {
            double daysToFirstPayment = 0;
            double dailyRate = 0;
            if (dailyAccrualMethod == "30/360")
            {
                while (onBoardedDate.Date < firstPaymentDate.Date)
                {
                    daysToFirstPayment += GetAccrualdays(onBoardedDate.Date, frequency);
                    onBoardedDate = onBoardedDate.AddDays(1);
                }

                dailyRate = InterestRate.Calculate_InterestRate("daily", annualRate);
            }
            else
            {
                daysToFirstPayment = (firstPaymentDate.Date - onBoardedDate.Date).TotalDays;
                if (actualAmortizationType == "VariableDailyRate")
                {
                    var previousScheduleDate = LMS.Foundation.Calculus.Helper.GetFirstPaymentDateBasedOnFrequency(frequency.ToString(), firstPaymentDate.Date);
                    int noOfDaysInMonth = DateTime.DaysInMonth(previousScheduleDate.Value.Year, previousScheduleDate.Value.Month);
                    var frequencyRate = InterestRate.Calculate_InterestRate(frequency.ToString(), annualRate);
                    dailyRate = frequencyRate / noOfDaysInMonth;
                }
                else
                {
                    var daysInYear = LMS.Foundation.Calculus.Helper.GetDaysInYearForProvidedDate(onBoardedDate.Date, firstPaymentDate.Date);
                    dailyRate = annualRate / daysInYear;
                }
            }

            var interestAmount = (dailyRate * daysToFirstPayment * outstandingAmount) / 100;
            return interestAmount;
        }

        private int GetAccrualdays(DateTime processingDate, PaymentFrequency loanFrequency)
        {
            if (loanFrequency == PaymentFrequency.Monthly)
            {
                bool isLeapYear = false;
                if ((processingDate.Year % 4 == 0 && processingDate.Year % 100 != 0) ||
                    (processingDate.Year % 400 == 0))
                    isLeapYear = true;

                if (isLeapYear == true && processingDate.Month == 2 && processingDate.Day == 29)
                    return 2;
                if (isLeapYear == false && processingDate.Month == 2 && processingDate.Day == 28)
                    return 3;
                else if (processingDate.Day == 31)
                    return 0;
                else
                    return 1;
            }
            else
                return 1;
        }

        private async Task<Tuple<string, int, string, string>> GetRoundingParameters(string loanNumber,
            string productId)
        {
            var rounding = Lookup.GetLookupEntry("parametername", "rounding");
            var roundingdigits = Lookup.GetLookupEntry("parametername", "roundingdigits");

            var productDetails = await GetProductParameters(productId, loanNumber);
            if (productDetails == null)
                throw new ArgumentException($"Product details not set for loan number: {loanNumber}");
            if (productDetails.ProductParameters == null)
                throw new ArgumentException($"Product parameters not set for loan number: {loanNumber}");

            var productParameter = productDetails.ProductParameters.ToList();
            var roundingMethod = productParameter.Where(p => p.Name == Convert.ToString(rounding["rounding"]))
                .Select(r => r.Value).FirstOrDefault();
            var roundingDigit = productParameter
                .Where(p => p.Name == Convert.ToString(roundingdigits["roundingdigits"])).Select(r => r.Value)
                .FirstOrDefault();
            var accrualMethod = productParameter.Where(p => p.Name == "DailyAccrualMethod").Select(r => r.Value)
                .FirstOrDefault();
            var actualAccrualType = productParameter.Where(p => p.Name == "ActualAmortizationType").Select(r => r.Value)
                .FirstOrDefault();

            return Tuple.Create(roundingMethod, Convert.ToInt32(roundingDigit), accrualMethod, actualAccrualType);
        }

        private string GetBusinessDateType(PaymentFrequency paymentFrequency)
        {
            switch (paymentFrequency)
            {
                case PaymentFrequency.Weekly:
                case PaymentFrequency.BiWeekly:
                    return "DayOfWeek";
                default:
                    return "DayOfMonth";
            }
        }

        private async Task<bool> GetScheduleRollPrincipalValue(string loanNumber, string productId)
        {
            var productDetails = await GetProductParameters(productId, loanNumber);
            if (productDetails == null)
                throw new ArgumentException($"Product details not set for loan number: {loanNumber}");
            if (productDetails.ProductParameters == null)
                throw new ArgumentException($"Product parameters not set for loan number: {loanNumber}");

            var productParameter = productDetails.ProductParameters.ToList();

            var rolePrincipal = productParameter.Where(p => p.Name == "Schedule_RollPrincipal").Select(r => r.Value)
                .FirstOrDefault();

            bool scheduleRollPrincipal = false;
            if (!string.IsNullOrWhiteSpace(rolePrincipal) && rolePrincipal.ToLower() == "true")
            {
                scheduleRollPrincipal = true;
            }

            return scheduleRollPrincipal;
        }

        #endregion
    }
}