﻿﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DataAttributes;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.NumberGenerator;
using LendFoundry.ProductConfiguration;
using LendFoundry.StatusManagement;
using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;
using LendFoundry.Flag;
using LMS.Payment.Processor;
 using IUnwindRequest = LMS.LoanAccounting.IUnwindRequest;
 using TransactionType = LMS.LoanAccounting.TransactionType;
 using UnwindRequest = LMS.LoanAccounting.UnwindRequest;
using LendFoundry.Email;

namespace LMS.LoanManagement
{
    public class LoanOnboardingService : ILoanOnboardingService
    {
        #region Constructor

        public LoanOnboardingService
        (
            LoanConfiguration loanConfiguration,
            ILoanScheduleService loanScheduleService,
            IAccrualBalanceService accrualBalanceService,
            ILoanOnboardingRepository loanOnboardingRepository,
            IBankDetailsRepository bankDetailsRepository,
            IGeneratorService numberGeneratorService,
            IEntityStatusService statusManagementService,
            IProductService productService,
            IEventHubClient eventHubClient,
            ITenantTime tenantTime,
            IBankDetailsHistoryRepository bankDetailsHistoryRepository,
            ILoanOnboardingHistoryRepository loanOnboardingHistoryRepository,
            ILookupService lookup,
            IDataAttributesEngine dataAttributesEngine,
            ILoanScheduleRepository loanScheduleRepository
        )
        {
            if (loanConfiguration == null)
                throw new ArgumentException("Configuration not set");

            LoanConfiguration = loanConfiguration;
            LoanScheduleService = loanScheduleService;
            LoanOnboardingRepository = loanOnboardingRepository;
            BankDetailsRepository = bankDetailsRepository;
            NumberGeneratorService = numberGeneratorService;
            ProductService = productService;
            StatusManagementService = statusManagementService;
            EventHubClient = eventHubClient;
            TenantTime = tenantTime;
            BankDetailsHistoryRepository = bankDetailsHistoryRepository;
            LoanOnboardingHistoryRepository = loanOnboardingHistoryRepository;
            Lookup = lookup;
            DataAttributesEngine = dataAttributesEngine;
            AccrualBalanceService = accrualBalanceService;
            LoanScheduleRepository = loanScheduleRepository;
          
        }

        #endregion

        #region Variables

        public LoanOnboardingService(IAccrualBalanceService accrualBalanceService, LoanConfiguration loanConfiguration,
            ILoanScheduleService loanScheduleService, ITenantTime tenantTime,
            ILoanOnboardingRepository loanOnboardingRepository, IGeneratorService numberGeneratorService,
            IBankDetailsRepository bankDetailsRepository, IEventHubClient eventHubClient,
            IEntityStatusService statusManagementService, IProductService productService,
            IBankDetailsHistoryRepository bankDetailsHistoryRepository,
            ILoanOnboardingHistoryRepository loanOnboardingHistoryRepository, ILookupService lookup,
            IDataAttributesEngine dataAttributesEngine, ILoanScheduleRepository loanScheduleRepository,
            IDecisionEngineService decisionEngine,
            IFlagService flagService,IPaymentProcessorService paymentProcessorService,
            IEmailService emailService
            )
        {
            this.AccrualBalanceService = accrualBalanceService;
            this.LoanConfiguration = loanConfiguration;
            this.LoanScheduleService = loanScheduleService;
            this.TenantTime = tenantTime;
            this.LoanOnboardingRepository = loanOnboardingRepository;
            this.NumberGeneratorService = numberGeneratorService;
            this.BankDetailsRepository = bankDetailsRepository;
            this.EventHubClient = eventHubClient;
            this.StatusManagementService = statusManagementService;
            this.ProductService = productService;
            this.BankDetailsHistoryRepository = bankDetailsHistoryRepository;
            this.LoanOnboardingHistoryRepository = loanOnboardingHistoryRepository;
            this.Lookup = lookup;
            this.DataAttributesEngine = dataAttributesEngine;
            this.LoanScheduleRepository = loanScheduleRepository;
            this.DecisionEngineService = decisionEngine;
            this.FlagService = flagService;
            this.PaymentProcessorService = paymentProcessorService;
            this.EmailService = emailService;

        }

        private IAccrualBalanceService AccrualBalanceService { get; set; }
        private LoanConfiguration LoanConfiguration { get; }
        private ILoanScheduleService LoanScheduleService { get; }
        private ITenantTime TenantTime { get; }
        private ILoanOnboardingRepository LoanOnboardingRepository { get; }
        private IGeneratorService NumberGeneratorService { get; }
        private IBankDetailsRepository BankDetailsRepository { get; }
        private IEventHubClient EventHubClient { get; }
        private IEntityStatusService StatusManagementService { get; }
        private IProductService ProductService { get; }
        private IBankDetailsHistoryRepository BankDetailsHistoryRepository { get; }
        private ILoanOnboardingHistoryRepository LoanOnboardingHistoryRepository { get; }
        private ILookupService Lookup { get; }
        private IDataAttributesEngine DataAttributesEngine { get; }
        private ILoanScheduleRepository LoanScheduleRepository { get; }
        private IDecisionEngineService DecisionEngineService { get; }
        private IFlagService FlagService { get; }
        private IPaymentProcessorService PaymentProcessorService { get; }
        private IEmailService EmailService { get; }

        #endregion

        public async Task<ILoanInformation> GetLoanInformationByLoanNumber(string loanNumber)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
                throw new NotFoundException($"Loan information for loan number: {loanNumber} is not available.");
            return loanInformation;
        }

        public async Task<ILoanInformation> GetLoanInformationByApplicationNumber(string applicationNumber)
        {
            var loanInformation =
                await LoanOnboardingRepository.GetLoanInformationByApplicationNumber(applicationNumber);
            if (loanInformation == null)
                throw new NotFoundException(
                    $"Loan information for application number: {applicationNumber} is not available.");
            return loanInformation;
        }

        /// <summary>
        /// Loan On Board Service
        /// </summary>
        /// <param name="loanInformationRequest"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public async Task<ILoanInformation> OnBoard(LoanInformationRequest loanInformationRequest)
        {
            await EnsureValidInput(loanInformationRequest);
            var loanNumber = string.Empty;
            ILoanInformation loanDetails = new LoanInformation();

            if (loanInformationRequest.LoanType == LoanType.Renewal)
            {
                loanDetails =
                    await LoanOnboardingRepository.GetLoanInformationByApplicationNumber(loanInformationRequest
                        .ParentApplicationNumber);
                if (loanDetails == null)
                {
                    throw new NotFoundException(
                        $"Loan information for parent application number: {loanInformationRequest.ParentApplicationNumber} is not available.");
                }

                var subLoanNumber = await NumberGeneratorService.TakeNext("subLoan",
                    new List<string>() {loanDetails.OriginalParentLoanNumber});
                loanNumber = loanDetails.OriginalParentLoanNumber + "-" + subLoanNumber;
                await LoanScheduleService.CloseOldLoan(loanInformationRequest, loanDetails);
            }
            else if (!string.IsNullOrEmpty(loanInformationRequest.LoanReferenceNumber))
            {
                var loanDetail =
                    await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanInformationRequest
                        .LoanReferenceNumber);
                if (loanDetail != null)
                {
                    throw new ArgumentException(
                        $"Loan information for loan  number: {loanInformationRequest.LoanReferenceNumber} is already available");
                }

                loanNumber = loanInformationRequest.LoanReferenceNumber;
            }
            else
            {
                loanNumber = await NumberGeneratorService.TakeNext("loanOnboard");
            }

            var loanInformation = await GenerateLoanInformation(loanNumber, loanInformationRequest, loanDetails);

            loanInformation.IsAutoPay = true;
            loanInformation.AutoPayHistory = new List<IAutoPayHistory>();
            loanInformation.IsRenewable = "false";
            loanInformation.AutoPayStartDate = loanInformation.LoanOnboardedDate;
            loanInformation.AutoPayStopDate = null;

            SetInvoiceDetailsForLoanOnBoard(loanInformation);

            await Task.Run(() => LoanOnboardingRepository.Add(loanInformation));

            // Publish event.
            await EventHubClient.Publish(new LoanOnboarded()
            {
                EntityType = "loan",
                LoanNumber = loanInformation.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            // Set product parameters for loanNumber.
            await SetProductParameters(loanInformation.LoanProductId, loanInformation.LoanNumber);

            await InitiateWorkFlow(loanInformation);

            // Set loan schedule headers.
            if (loanInformationRequest.LoanScheduleRequest != null)
            {
                loanInformationRequest.LoanScheduleRequest.LoanNumber = loanInformation.LoanNumber;
                await LoanScheduleService.LoanSchedule(loanInformationRequest.LoanScheduleRequest);
            }

            if (loanInformationRequest.PaymentInstruments != null)
            {
                foreach (var paymentInstrument in loanInformationRequest.PaymentInstruments)
                {
                    if (paymentInstrument.BankDetails == null)
                        continue;

                    paymentInstrument.BankDetails.Id = Guid.NewGuid().ToString("N");
                    paymentInstrument.BankDetails.PaymentInstrumentId = paymentInstrument.Id;
                    paymentInstrument.BankDetails.LoanNumber = loanNumber;
                    paymentInstrument.BankDetails.IsActive = paymentInstrument.IsActive;
                    paymentInstrument.BankDetails.IsVerified = paymentInstrument.BankDetails.IsVerified;
                    paymentInstrument.BankDetails.BranchIdentifiers = paymentInstrument.BankDetails.BranchIdentifiers;
                    await Task.Run(() => BankDetailsRepository.Add(paymentInstrument.BankDetails));
                }
            }

            if (loanInformationRequest.AutoInitialize)
            {
                await LoanScheduleService.InitializeLoan(loanInformation.LoanNumber,loanInformationRequest.AutoAccrualForPastDate);
            }

            loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanInformation.LoanNumber);
            return loanInformation;
        }

        /// <summary>
        /// Update renewal details
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="renewalDetail"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<ILoanInformation> UpdateRenewalDetails(string loanNumber,
            UpdateRenewalDetailRequest renewalDetail)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
            {
                throw new ArgumentException($"Loan details for loan number: {loanNumber} is not available.");
            }

            if (renewalDetail == null)
            {
                throw new ArgumentException($"Invalid input details");
            }

            return await LoanOnboardingRepository.UpdateRenewalDetails(loanNumber, renewalDetail);
        }

        /// <summary>
        /// Update Loan Information
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="loanInformationRequest"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task<ILoanInformation> UpdateLoanInformation(string loanNumber,
            LoanInformationRequest loanInformationRequest)
        {
            await EnsureValidInput(loanInformationRequest, true);

            var loanStatus = await LoanScheduleService.GetLoanStatus(loanNumber);
            if (loanStatus.Code != LoanConfiguration.Statuses["Created"])
            {
                throw new ArgumentException($"Status of loan is invalid");
            }

            ILoanInformation loanDetails = new LoanInformation();
            if (loanInformationRequest.LoanType == LoanType.Renewal)
            {
                loanDetails =
                    await LoanOnboardingRepository.GetLoanInformationByApplicationNumber(loanInformationRequest
                        .ParentApplicationNumber);
            }

            var loanInformation = await GenerateLoanInformation(loanNumber, loanInformationRequest, loanDetails);
            if (loanInformationRequest.AutoInitialize)
            {
                await LoanScheduleService.EnsureInitializeValid(loanNumber, loanInformation, null);
            }

            // Add into history
            await AddHistory(loanNumber);

            await Task.Run(() => LoanOnboardingRepository.UpdateLoanInformation(loanNumber, loanInformation));

            // Publish event.
            await EventHubClient.Publish(new LoanDetailsUpdated()
            {
                EntityType = "loan",
                LoanNumber = loanInformation.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            // Set loan schedule headers.
            if (loanInformationRequest.LoanScheduleRequest != null)
            {
                loanInformationRequest.LoanScheduleRequest.LoanNumber = loanNumber;
                var loanSchedule = await LoanScheduleService.GetLoanSchedule(loanInformation.LoanNumber,
                    loanInformationRequest.LoanScheduleRequest.ScheduleVersionNo);
                if (loanSchedule == null)
                {
                    await LoanScheduleService.LoanSchedule(loanInformationRequest.LoanScheduleRequest);
                }
                else
                {
                    await LoanScheduleService.UpdateLoanSchedule(loanInformation.LoanNumber, loanSchedule.Id,
                        loanInformationRequest.LoanScheduleRequest);
                }
            }

            if (loanInformationRequest.AutoInitialize)
            {
                await LoanScheduleService.InitializeLoan(loanInformation.LoanNumber,false);
            }

            return loanInformation;
        }

        /// <summary>
        /// Get Related Loan Information
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public async Task<List<RelatedLoanResponse>> GetRelatedLoanInformation(string loanNumber)
        {
            if (string.IsNullOrWhiteSpace(loanNumber))
                throw new ArgumentNullException($"{nameof(loanNumber)} cannot be null");
            var relatedLoanInformation = await LoanOnboardingRepository.GetRelatedLoanInformation(loanNumber);
            var RelatedLoanDetail = new List<RelatedLoanResponse>();

            foreach (var loans in relatedLoanInformation)
            {
                var statusInfo = await StatusManagementService.GetActiveStatusWorkFlow("loan", loans.LoanNumber);
                var statusName =
                    await StatusManagementService.GetStatusByEntity("loan", loans.LoanNumber,
                        statusInfo.StatusWorkFlowId);
                var applicationStatus =
                    await StatusManagementService.GetActiveStatusWorkFlow("application", loans.LoanApplicationNumber);
                var activeSchedule = await LoanScheduleService.GetLoanSchedule(loans.LoanNumber);
                var relatedLoan = new RelatedLoanResponse();
                relatedLoan.LoanNumber = loans.LoanNumber;
                relatedLoan.LoanApplicationNumber = loans.LoanApplicationNumber;
                relatedLoan.ProductId = loans.LoanProductId;
                relatedLoan.RenewedAmount = activeSchedule != null && loans.LoanNumber != loans.OriginalParentLoanNumber
                    ? activeSchedule.FundedAmount
                    : 0;
                relatedLoan.RenewedOn =
                    loans.LoanNumber != loans.OriginalParentLoanNumber ? loans.LoanOnboardedDate : null;
                relatedLoan.LoanStatusWorkFlowId = statusInfo != null ? statusInfo.StatusWorkFlowId : null;
                relatedLoan.Status = statusName != null ? statusName.Name : "";
                relatedLoan.ApplicationStatusWorkFlowId =
                    applicationStatus != null ? applicationStatus.StatusWorkFlowId : null;
                relatedLoan.ReasonOfStatus = "";
                relatedLoan.SequenceNumber = loans.SequenceNumber;
                RelatedLoanDetail.Add(relatedLoan);
            }

            return RelatedLoanDetail;
        }

        /// <summary>
        /// Change status
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="status"></param>
        /// <param name="reasons"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public async Task ChangeStatus(string loanNumber, string status, List<string> reasons = null)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
                throw new NotFoundException($"Loan information for loan number: {loanNumber} is not available.");

            var statusCode = LoanConfiguration.Statuses[status];
            if (statusCode == null)
                throw new ArgumentException($"Invalid status {status}");

            var statusWorkFlow = await StatusManagementService.GetActiveStatusWorkFlow("loan", loanInformation.LoanNumber);

            RequestModel requestmodel = new RequestModel();
            requestmodel.reasons = reasons;
            // Update status as provided.
            await StatusManagementService.ChangeStatus("loan", loanInformation.LoanNumber,
                statusWorkFlow != null
                    ? Convert.ToString(statusWorkFlow.StatusWorkFlowId)
                    : LoanConfiguration.DefaultStatusFlow, statusCode, requestmodel);
            if (statusCode == LoanConfiguration.Statuses["PaidOff"])
            {
                var activeSchedule = await LoanScheduleService.GetLoanSchedule(loanNumber);
                var loanDetails =
                    await AccrualBalanceService.GetLoanByScheduleVersion(loanNumber, activeSchedule.ScheduleVersionNo);
                await EventHubClient.Publish("LoanPaidOff", new
                {
                    EntityType = "loan",
                    LoanNumber = loanNumber,
                    LoanResponse = loanInformation,
                    ScheduleResponse = activeSchedule,
                    AccountingResponse = loanDetails
                });
            }
        }

        /// <summary>
        /// Change status to close
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="reasons"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public async Task ChangeStatusToClose(string loanNumber, List<string> reasons = null)
        {
            if(string.IsNullOrWhiteSpace(loanNumber))
            {
                throw new ArgumentNullException($"{nameof(loanNumber)} is mandatory");
            }
            var activeSchedule = await LoanScheduleService.GetLoanSchedule(loanNumber);
            if(activeSchedule == null)
            {
                throw new ArgumentException($"No active schedule found for loanNumber:{loanNumber}");
            }
            var statusWorkFlow = await StatusManagementService.GetActiveStatusWorkFlow("loan", loanNumber);
            if(statusWorkFlow == null)
            {
                throw new ArgumentException($"No active status workflow found for loanNumber:{loanNumber}");
            }
            var loanDetails = await AccrualBalanceService.GetLoanByScheduleVersion(loanNumber, activeSchedule.ScheduleVersionNo);
            if(loanDetails == null)
            {
                throw new ArgumentException($"No accounting details found for loanNumber:{loanNumber} with ScheduleVersionNo: {activeSchedule.ScheduleVersionNo}");
            }
            if(statusWorkFlow.Code != LoanConfiguration.Statuses["UnWind"])
            {
                if (loanDetails != null && loanDetails.ExcessMoney.Amount <= 0)
                {
                    if (loanDetails.ChargeOff != null)
                    {
                        if (loanDetails.PaymentInfo.PaidPrincipalAmount + loanDetails.ChargeOff.ChargeOffAmount < loanDetails.PrincipalAmount)
                        {
                            throw new InvalidOperationException("Action not allowed: Principal Amount is not fully recovered");
                        }
                    }
                    else
                    {
                        if (loanDetails.SellOff == null)
                        {
                            double ignoreDigits = 0;
                            var productDetails = await LoanScheduleService.GetProductParameters(loanDetails.ProductId, loanNumber);
                            if (productDetails != null)
                            {
                                if (productDetails.ProductParameters != null)
                                {
                                    var productParameter = productDetails.ProductParameters.ToList();
                                    try
                                    {
                                        ignoreDigits = Convert.ToDouble(productParameter.Where(p => p.Name == "IgnoreDigits").Select(r => r.Value).FirstOrDefault());
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }
                            }

                            if (loanDetails.PaymentInfo.PaidPrincipalAmount + ignoreDigits < loanDetails.PrincipalAmount)
                            {
                                throw new InvalidOperationException("Action not allowed: Principal Amount is not fully recovered");
                            }
                        }
                    }
                }
                else
                {
                    throw new InvalidOperationException("Refund excess amount before closing the Loan");
                }
            }

            await ChangeStatus(loanNumber, "Closed", reasons);
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);

            // Publish event.
            await EventHubClient.Publish("LoanClosed", new
            {
                EntityType = "loan",
                EntityId = loanInformation != null ? loanInformation.LoanApplicationNumber : null,
                Response = "application",
                LoanNumber = loanNumber,
                LoanResponse = loanInformation,
                ScheduleResponse = activeSchedule,
                AccountingResponse = loanDetails
            });
        }

        /// <summary>
        /// Get Loan List
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task<List<ILoanDetails>> GetLoanList(DateTime startDate, DateTime endDate)
        {
            var loaninfo = await LoanOnboardingRepository.GetLoanInformationListByOnboardingDate(startDate, endDate);
            var bankdetails = await BankDetailsRepository.GetAllBankList();
            //var accrualdetails = await AccrualBalanceService.GetLoanAcrual();
            List<ILoanDetails> loandetailsList = new List<ILoanDetails>();

            foreach (var item in loaninfo)
            {
                try
                {
                    ILoanDetails loandetail = new LoanDetails();
                    loandetail.BankDetails = bankdetails.Where(x => x.LoanNumber == item.LoanNumber).ToList();
                    loandetail.ApplicantDetails = item.ApplicantDetails;
                    loandetail.AutoPayStartDate = item.AutoPayStartDate;
                    loandetail.AutoPayStopDate = item.AutoPayStopDate;
                    loandetail.AutoPayHistory = item.AutoPayHistory;
                    loandetail.BusinessDetails = item.BusinessDetails;
                    loandetail.Currency = item.Currency;
                    loandetail.DrawDownDetails = item.DrawDownDetails;
                    loandetail.Id = item.Id;
                    loandetail.IsAutoPay = item.IsAutoPay;
                    loandetail.LoanApplicationNumber = item.LoanApplicationNumber;
                    loandetail.LoanEndDate = item.LoanEndDate;
                    loandetail.LoanFundedDate = item.LoanFundedDate;
                    loandetail.LoanNumber = item.LoanNumber;
                    loandetail.LoanOnboardedDate = item.LoanOnboardedDate;
                    loandetail.LoanProductId = item.LoanProductId;
                    loandetail.LoanStartDate = item.LoanStartDate;
                    loandetail.OtherContacts = item.OtherContacts;
                    loandetail.PaymentInstruments = item.PaymentInstruments;
                    loandetail.PortFolioId = item.PortFolioId;
                    loandetail.PrimaryApplicantDetails = item.PrimaryApplicantDetails;
                    loandetail.PrimaryBusinessDetails = item.PrimaryBusinessDetails;
                    loandetail.ProductCategory = item.ProductCategory;
                    loandetail.ProductPortfolioType = item.ProductPortfolioType;
                    loandetail.TenantId = item.TenantId;

                    var schedules = await LoanScheduleRepository.GetLoanScheduleByLoanNumber(item.LoanNumber);
                    loandetail.LoanSchedules = schedules;

                    loandetailsList.Add(loandetail);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return loandetailsList;
        }

        /// <summary>
        /// Update Collection Stage
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="collectionStage"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public async Task<ILoanInformation> UpdateCollectionStage(string loanNumber, string collectionStage)
        {
            if (string.IsNullOrWhiteSpace(loanNumber))
                throw new ArgumentNullException($"{nameof(loanNumber)} cannot be null");

            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new ArgumentException($"Loan does not exist for {loanNumber}");
            if (loanInformation.CollectionStage != collectionStage)
            {
                var oldCollectionStage = loanInformation.CollectionStage;
                loanInformation.CollectionStage = collectionStage;

                await LoanOnboardingRepository.UpdateLoanInformation(loanNumber, loanInformation);

                // Publish event.
                await EventHubClient.Publish(new CollectionStageUpdated
                {
                    EntityType = "loan",
                    LoanNumber = loanInformation.LoanNumber,
                    ProductId = loanInformation.LoanProductId,
                    LoanResponse = loanInformation,
                    OldCollectionStage = oldCollectionStage
                });
            }

            return loanInformation;
        }

        #region Update Loan Details

        public async Task<ILoanInformation> UpdateAddress(string type, string loanNumber, string id, IAddress address)
        {
            var loanStatus = await LoanScheduleService.GetLoanStatus(loanNumber);
            if (loanStatus.Code != LoanConfiguration.Statuses["Created"])
                throw new ArgumentException($"Status of loan is invalid");

            var loanInformation = await LoanOnboardingRepository.UpdateAddress(type, loanNumber, id, address);

            // Publish event.
            await EventHubClient.Publish(new LoanDetailsUpdated()
            {
                EntityType = "loan",
                LoanNumber = loanInformation.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            return loanInformation;
        }

        public async Task<ILoanInformation> UpdateEmail(string type, string loanNumber, string id, Abstractions.IEmail email)
        {
            var loanStatus = await LoanScheduleService.GetLoanStatus(loanNumber);
            if (loanStatus.Code != LoanConfiguration.Statuses["Created"])
                throw new ArgumentException($"Status of loan is invalid");

            var loanInformation = await LoanOnboardingRepository.UpdateEmail(type, loanNumber, id, email);

            // Publish event.
            await EventHubClient.Publish(new LoanDetailsUpdated()
            {
                EntityType = "loan",
                LoanNumber = loanInformation.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            return loanInformation;
        }

        public async Task<ILoanInformation> UpdatePhone(string type, string loanNumber, string id, IPhone phone)
        {
            var loanStatus = await LoanScheduleService.GetLoanStatus(loanNumber);
            if (loanStatus.Code != LoanConfiguration.Statuses["Created"])
                throw new ArgumentException($"Status of loan is invalid");

            var loanInformation = await LoanOnboardingRepository.UpdatePhone(type, loanNumber, id, phone);

            // Publish event.
            await EventHubClient.Publish(new LoanDetailsUpdated()
            {
                EntityType = "loan",
                LoanNumber = loanInformation.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            return loanInformation;
        }

        public async Task<ILoanInformation> UpdateApplicant(string loanNumber, string applicantId,
            IApplicantRequest applicantRequest)
        {
            //var loanStatus = await LoanScheduleService.GetLoanStatus(loanNumber);
            //if (loanStatus.Code != LoanConfiguration.Statuses["Created"])
            //    throw new ArgumentException($"Status of loan is invalid");

            var applicantDetails = new ApplicantDetails(applicantRequest);
            applicantDetails.Id = applicantId;
            var loanInformation =
                await LoanOnboardingRepository.UpdateApplicant(loanNumber, applicantId, applicantDetails);

            // Publish event.
            await EventHubClient.Publish(new LoanDetailsUpdated()
            {
                EntityType = "loan",
                LoanNumber = loanInformation.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            return loanInformation;
        }

        public async Task<ILoanInformation> UpdateBusiness(string loanNumber, string businessId,
            IBusinessDetails businessDetails)
        {
            var loanStatus = await LoanScheduleService.GetLoanStatus(loanNumber);
            if (loanStatus.Code != LoanConfiguration.Statuses["Created"])
                throw new ArgumentException($"Status of loan is invalid");

            var loanInformation =
                await LoanOnboardingRepository.UpdateBusiness(loanNumber, businessId, businessDetails);

            // Publish event.
            await EventHubClient.Publish(new LoanDetailsUpdated()
            {
                EntityType = "loan",
                LoanNumber = loanInformation.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            return loanInformation;
        }

        public async Task<ILoanInformation> UpdatePaymentInstrument(string loanNumber, string paymentInstrumentId,
            IPaymentInstrument paymentInstrument)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
                throw new NotFoundException($"Loan information for loan number: {loanNumber} is not available.");

            loanInformation =
                await LoanOnboardingRepository.UpdatePaymentInstrument(loanNumber, paymentInstrumentId,
                    paymentInstrument);

            return loanInformation;
        }

        public async Task<ILoanInformation> AddApplicantDetails(string loanApplicationNumber,
            List<ApplicantRequest> ApplicantRequest)
        {
            var applicantDetails = new List<IApplicantDetails>();
            var inputRequest = new List<IApplicantRequest>();
            if (ApplicantRequest != null && ApplicantRequest.Any())
            {
                foreach (var applicantRequest in ApplicantRequest)
                {
                    if (applicantRequest != null)
                    {
                        var applicantDetail = new ApplicantDetails(applicantRequest);
                        applicantDetails.Add(applicantDetail);
                        inputRequest.Add(applicantRequest);
                    }
                }
            }

            await EnsureValidApplicantDatails(inputRequest);
            var loanInformation = await LoanOnboardingRepository.AddApplicant(loanApplicationNumber, applicantDetails);

            return loanInformation;
        }

        #endregion

        #region Bank

        public async Task<IBankDetails> AddBankDetails(string loanNumber, IBankDetails bankDetails)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Loan Information for {loanNumber} not found");
            if (!loanInformation.PaymentInstruments.Any(x => x.PaymentInstrumentType == PaymentInstrumentType.Ach))
                throw new ArgumentNullException($"Payment instrument not found for ACH instrument");

            bankDetails.PaymentInstrumentId = loanInformation.PaymentInstruments
                .FirstOrDefault(x => x.PaymentInstrumentType == PaymentInstrumentType.Ach).Id;

            EnsureValidBankDetails(bankDetails, loanInformation);

            if (string.IsNullOrWhiteSpace(bankDetails.LoanNumber))
            {
                bankDetails.LoanNumber = loanNumber;
            }

            if (bankDetails.IsActive)
            {
                var allBanks = await BankDetailsRepository.GetAllBankDetails(loanNumber);
                if (allBanks != null && allBanks.Any())
                {
                    allBanks.ForEach(b => b.IsActive = false);
                    foreach (var bank in allBanks)
                    {
                        await Task.Run(() => BankDetailsRepository.Update(bank));
                    }
                }
            }

            await Task.Run(() => BankDetailsRepository.Add(bankDetails));

            // Publish event.
            await EventHubClient.Publish(new BankDetailsAdded()
            {
                EntityType = "loan",
                LoanNumber = bankDetails.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            return bankDetails;
        }

        public async Task<IBankDetails> UpdateBankDetails(string loanNumber, string bankId, IBankDetails bankDetails)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Loan Information for {loanNumber} not found");

            var exist = await Task.Run(() => BankDetailsRepository.Get(bankId));
            if (exist == null)
                throw new ArgumentException("Bank details is not available for this loan number and bank id");

            if (exist.IsVerified)
                throw new ArgumentException("Cannot update already verified bank details which is added from LOS");

            if (string.IsNullOrWhiteSpace(bankDetails.Id))
            {
                bankDetails.Id = bankId;
            }

            bankDetails.PaymentInstrumentId = loanInformation.PaymentInstruments
                .FirstOrDefault(x => x.PaymentInstrumentType == PaymentInstrumentType.Ach)?.Id;

            EnsureValidBankDetails(bankDetails, loanInformation);

            await AddHistory(loanNumber, bankId);

            if (string.IsNullOrWhiteSpace(bankDetails.LoanNumber))
            {
                bankDetails.LoanNumber = loanNumber;
            }

            if (bankDetails.IsActive)
            {
                var allBanks = await BankDetailsRepository.GetAllBankDetails(loanNumber);
                if (allBanks != null && allBanks.Any())
                {
                    allBanks.ForEach(b => b.IsActive = false);
                    foreach (var bank in allBanks)
                    {
                        await Task.Run(() => BankDetailsRepository.Update(bank));
                    }
                }
            }

            await BankDetailsRepository.UpdateBankDetails(loanNumber, bankId, bankDetails);

            // Publish event.
            await EventHubClient.Publish(new BankDetailsAdded()
            {
                EntityType = "loan",
                LoanNumber = bankDetails.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            return bankDetails;
        }

        public async Task<IBankDetails> SetBankActive(string loanNumber, string bankId)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Loan Information for {loanNumber} not found");

            var exist = await Task.Run(() => BankDetailsRepository.Get(bankId));
            if (exist == null)
                throw new ArgumentException("Bank details is not available for this loan number and bank id");

            await AddHistory(loanNumber, bankId);

            if (!exist.IsActive)
            {
                var allBanks = await BankDetailsRepository.GetAllBankDetails(loanNumber);
                if (allBanks != null && allBanks.Any())
                {
                    allBanks.ForEach(b => b.IsActive = false);
                    foreach (var bank in allBanks)
                    {
                        await Task.Run(() => BankDetailsRepository.Update(bank));
                    }
                }
            }

            exist.IsActive = !exist.IsActive;
            await BankDetailsRepository.UpdateBankDetails(loanNumber, bankId, exist);

            // Publish event.
            await EventHubClient.Publish(new BankDetailsAdded()
            {
                EntityType = "loan",
                LoanNumber = exist.LoanNumber,
                ProductId = loanInformation.LoanProductId
            });

            return exist;
        }

        public async Task<IBankDetails> GetBankDetails(string loanNumber, string bankId = null)
        {
            var bankDetails = await BankDetailsRepository.GetBankDetails(loanNumber, bankId);

            return bankDetails;
        }

        public async Task<List<IBankDetails>> GetAllBankDetails(string loanNumber)
        {
            var bankDetails = await BankDetailsRepository.GetAllBankDetails(loanNumber);

            return bankDetails;
        }

        public async Task<ILoanInformation> UpdateAutoPayDetail(string loanNumber, IAutoPayRequest autoPayRequest)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            var latestSchedule = await LoanScheduleService.GetLoanSchedule(loanNumber);
            if (loanInformation == null)
                throw new NotFoundException($"Loan information for loan number: {loanNumber} is not available.");
            if (autoPayRequest == null)
                throw new ArgumentNullException($"{nameof(autoPayRequest)} cannot be null");
            if (autoPayRequest == null)
                throw new ArgumentNullException($"{nameof(autoPayRequest)} cannot be null");
            if (autoPayRequest.IsAutopay && autoPayRequest.AutoPayStartDate == null)
                throw new ArgumentNullException($"{nameof(autoPayRequest.AutoPayStartDate)} cannot be null");
            if (!autoPayRequest.IsAutopay && autoPayRequest.AutoPayStopDate == null)
                throw new ArgumentNullException($"{nameof(autoPayRequest.AutoPayStopDate)} cannot be null");
            if (autoPayRequest.AutoPayStartDate != null && latestSchedule != null && autoPayRequest.AutoPayStartDate >
                latestSchedule.ScheduleDetails.LastOrDefault().ScheduleDate)
                throw new InvalidArgumentException(
                    $"{nameof(autoPayRequest.AutoPayStartDate)} cannot be greater than last payment");
            if (autoPayRequest.AutoPayStopDate != null && latestSchedule != null && autoPayRequest.AutoPayStopDate >
                latestSchedule.ScheduleDetails.LastOrDefault().ScheduleDate)
                throw new InvalidArgumentException(
                    $"{nameof(autoPayRequest.AutoPayStopDate)} cannot be greater than last payment");

            loanInformation.AutoPayHistory = loanInformation.AutoPayHistory == null
                ? new List<IAutoPayHistory>()
                : loanInformation.AutoPayHistory;
            var lastHistoryData = loanInformation.AutoPayHistory.Count > 0
                ? loanInformation.AutoPayHistory.LastOrDefault()
                : null;
            //var autoPayDate = !autoPayRequest.IsAutopay ? new TimeBucket (autoPayRequest.AutoPayStopDate.Value) : new TimeBucket (autoPayRequest.AutoPayStartDate.Value);
            //var lastHistoryFlag = loanInformation.AutoPayStartDate != null && loanInformation.AutoPayStopDate != null && autoPayDate.Time.Date >= lastHistoryData.StopDate.Time.Date && autoPayDate.Time.Date <= lastHistoryData.ResumeDate.Time.Date ? lastHistoryData.IsAutoPay : loanInformation.IsAutoPay;

            if (lastHistoryData != null && lastHistoryData.StopDate == null && autoPayRequest.AutoPayStopDate == null &&
                lastHistoryData.IsAutoPay == autoPayRequest.IsAutopay)
                throw new ArgumentException(
                    $"Please note that Auto-pay is not paused, hence this action will not result in anything.");

            var accrualDate =
                await AccrualBalanceService.GetLoanByScheduleVersion(loanNumber, latestSchedule.ScheduleVersionNo);
            //when last history is range
            if (lastHistoryData != null && lastHistoryData.StopDate != null && lastHistoryData.ResumeDate != null)
            {
                //TODO: need to take todays date in future

                //when we are already in autopay stop zone and resume date is future date then update resume date 
                if (accrualDate != null && accrualDate.PBOT != null && autoPayRequest.AutoPayType == AutoPayType.Auto &&
                    lastHistoryData.ResumeDate.Time.Date >= accrualDate.PBOT.AsOfDatePBOT.AddDays(1).Date &&
                    lastHistoryData.StopDate.Time.Date < accrualDate.PBOT.AsOfDatePBOT.AddDays(1).Date)
                {
                    lastHistoryData.ResumeDate = new TimeBucket(accrualDate.PBOT.AsOfDatePBOT);
                }
                // when we are already in autopay stop zone for the first date and resume date is future date then update resume date
                else if (accrualDate != null && accrualDate.PBOT != null &&
                         autoPayRequest.AutoPayType == AutoPayType.Auto &&
                         lastHistoryData.ResumeDate.Time.Date >= accrualDate.PBOT.AsOfDatePBOT.AddDays(1).Date &&
                         lastHistoryData.StopDate.Time.Date == accrualDate.PBOT.AsOfDatePBOT.AddDays(1).Date)
                {
                    lastHistoryData.ResumeDate = new TimeBucket(accrualDate.PBOT.AsOfDatePBOT.AddDays(1));
                }
                else if (autoPayRequest.AutoPayStopDate == null && autoPayRequest.AutoPayType == AutoPayType.Manual &&
                         lastHistoryData.ResumeDate.Time.Date >= accrualDate.PBOT.AsOfDatePBOT.AddDays(1).Date)
                {
                    autoPayRequest.AutoPayStopDate = loanInformation.AutoPayStopDate.Time;
                    loanInformation.AutoPayHistory.RemoveAt(loanInformation.AutoPayHistory.Count - 1);
                }
                //else if last autopay range is future then remove
                else if (lastHistoryData.ResumeDate.Time.Date >= accrualDate.PBOT.AsOfDatePBOT.AddDays(1).Date &&
                         lastHistoryData.StopDate.Time.Date >= accrualDate.PBOT.AsOfDatePBOT.AddDays(1).Date)
                {
                    loanInformation.AutoPayHistory.RemoveAt(loanInformation.AutoPayHistory.Count - 1);
                }
            }
            else if (lastHistoryData != null && lastHistoryData.IsAutoPay == false &&
                     lastHistoryData.ResumeDate == null && lastHistoryData.StopDate.Time.Date >=
                     accrualDate.PBOT.AsOfDatePBOT.AddDays(1).Date)
            {
                loanInformation.AutoPayHistory.RemoveAt(loanInformation.AutoPayHistory.Count - 1);
            }

            loanInformation.IsAutoPay = autoPayRequest.IsAutopay;
            loanInformation.AutoPayStartDate = autoPayRequest.AutoPayStartDate != null
                ? new TimeBucket(autoPayRequest.AutoPayStartDate.Value)
                : null;
            loanInformation.AutoPayStopDate = autoPayRequest.AutoPayStopDate != null
                ? new TimeBucket(autoPayRequest.AutoPayStopDate.Value)
                : null;
            loanInformation.AutoPayType = autoPayRequest.AutoPayType;
            if (autoPayRequest.AutoPayStartDate != null && autoPayRequest.AutoPayStopDate != null)
                autoPayRequest.AutoPayStartDate = autoPayRequest.AutoPayStartDate.Value.AddDays(-1);

            loanInformation.AutoPayHistory.Add(new AutoPayHistory()
            {
                IsAutoPay = autoPayRequest.AutoPayStartDate != null && autoPayRequest.AutoPayStopDate != null
                    ? false
                    : autoPayRequest.IsAutopay,
                StopDate = autoPayRequest.AutoPayStopDate != null
                    ? new TimeBucket(autoPayRequest.AutoPayStopDate.Value)
                    : null,
                ResumeDate = autoPayRequest.AutoPayStartDate != null
                    ? new TimeBucket(autoPayRequest.AutoPayStartDate.Value)
                    : null
            });

            if (autoPayRequest.IsAutopay && autoPayRequest.AutoPayStartDate != null &&
                autoPayRequest.AutoPayStopDate != null)
            {
                await EventHubClient.Publish("AutoPayRangeEntered", new
                {
                    EntityType = "loan",
                    LoanNumber = loanInformation.LoanNumber,
                    ProductId = loanInformation.LoanProductId,
                    LoanInformation = loanInformation
                });
            }
            else if (!autoPayRequest.IsAutopay)
            {
                await EventHubClient.Publish(new AutoPayStop()
                {
                    EntityType = "loan",
                    LoanNumber = loanInformation.LoanNumber,
                    ProductId = loanInformation.LoanProductId,
                    LoanInformation = loanInformation
                });
            }
            else
            {
                await EventHubClient.Publish(new AutoPayStarted()
                {
                    EntityType = "loan",
                    LoanNumber = loanInformation.LoanNumber,
                    ProductId = loanInformation.LoanProductId,
                    LoanInformation = loanInformation
                });
            }

            return await LoanOnboardingRepository.UpdateLoanInformation(loanNumber, loanInformation);
        }

        #endregion

        #region OtherContacts

        public async Task<ILoanInformation> AddOtherContact(string loanNumber, IOtherContact otherContact)
        {
            EnsureValidOtherContact(otherContact);

            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Loan Information for {loanNumber} not found");

            if (loanInformation.OtherContacts == null)
            {
                loanInformation.OtherContacts = new List<IOtherContact>();
                otherContact = otherContact != null ? new OtherContact(otherContact) : null;
                loanInformation.OtherContacts.Add(otherContact);
            }
            else
            {
                otherContact = otherContact != null ? new OtherContact(otherContact) : null;
                loanInformation.OtherContacts.Add(otherContact);
            }

            await Task.Run(() => LoanOnboardingRepository.UpdateLoanInformation(loanNumber, loanInformation));

            return loanInformation;
        }

        public async Task<ILoanInformation> UpdateOtherContact(string loanNumber, string otherContactId,
            IOtherContact otherContact)
        {
            EnsureValidOtherContact(otherContact);
            return await LoanOnboardingRepository.UpdateOtherContact(loanNumber, otherContactId, otherContact);
        }

        public async Task<ILoanInformation> UpdateInvoiceDetails(string loanNumber, string invoiceId,
            List<ILoanInvoiceDetails> invoiceDetails)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
            {
                throw new NotFoundException($"Loan information for loan number: {loanNumber} is not available.");
            }

            if (invoiceDetails == null && !invoiceDetails.Any())
            {
                throw new NotFoundException($"InvoiceDetails for loan number: {loanNumber} is not available.");
            }
            if(loanInformation.ProductPortfolioType!=ProductPortfolioType.SCF)
            {
                return null;
            }

            loanInformation =
                await LoanOnboardingRepository.UpdateInvoiceDetails(loanNumber, invoiceId, invoiceDetails);
            return loanInformation;
        }

        public async Task<string> GetMADFlagStatus(string applicationNumber)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByApplicationNumber(applicationNumber);
            if (loanInformation == null)
            {
                throw new NotFoundException($"Loan information for application number: {applicationNumber} is not available.");
            }

            var flagData = FlagService.GetAllFlag("loan",loanInformation.LoanNumber); 
            var madFlag = flagData.Where(f => f.Status == FlagStatus.Active && f.FlagTags.Contains("MinimumAmountDueNotMet"));
            if (madFlag != null && madFlag.Any())
            {
                return "MinimumAmountDue Not Met";
            }

            return "MinimumAmountDue Met";
        }

        #endregion

        #region Private Methods

        private async Task<List<IProductParameters>> GetProductParameters(string productId)
        {
            return await ProductService.GetAllProductParameters(productId);
        }

        private async Task<List<IProductParameters>> GetProductParametersByGroupName(string productId, string groupName)
        {
            return await ProductService.GetProductParameterByGroupName(productId, groupName);
        }

        private async Task SetProductParameters(string productId, string loanNumber)
        {
            var product = await ProductService.Get(productId);
            var productGroup = await ProductService.GetAllProductGroup(productId);
            var feeParameters = await ProductService.GetAllProductFeeParameters(productId);
            var productParameters = await ProductService.GetAllProductParameters(productId);
            dynamic productData = new ExpandoObject();
            productData.ProductId = productId;
            productData.Product = product;
            productData.ProductParameterGroup = productGroup;
            productData.ProductParameters = productParameters;
            productData.ProductFeeParameters = feeParameters;

            await DataAttributesEngine.SetAttribute("loan", loanNumber, "product", (object) productData);
        }

        private async Task<ILoanInformation> GenerateLoanInformation(string loanNumber,
            LoanInformationRequest loanInformationRequest, ILoanInformation previousLoanDetails)
        {
            var loanInformation = new LoanInformation();
            loanInformation.LoanNumber = loanNumber;
            loanInformation.LoanFundedDate = new TimeBucket(loanInformationRequest.LoanFundedDate);
            loanInformation.LoanApplicationNumber = loanInformationRequest.LoanApplicationNumber;
            loanInformation.LoanOnboardedDate = loanInformationRequest.LoanOnboardedDate != null
                ? new TimeBucket(loanInformationRequest.LoanOnboardedDate)
                : new TimeBucket(TenantTime.Now);
            loanInformation.LoanProductId = loanInformationRequest.LoanProductId;
            loanInformation.LoanStartDate = new TimeBucket(loanInformationRequest.LoanStartDate);
            loanInformation.LoanEndDate = new TimeBucket(TenantTime.Now); // Updated when loan schedule is created.
            loanInformation.ProductPortfolioType = loanInformationRequest.ProductPortfolioType;
            loanInformation.ProductCategory = loanInformationRequest.ProductCategory;
            loanInformation.ParentLoanNumber = string.IsNullOrWhiteSpace(previousLoanDetails.LoanNumber)
                ? loanNumber
                : previousLoanDetails.LoanNumber;
            loanInformation.OriginalParentLoanNumber =
                string.IsNullOrWhiteSpace(previousLoanDetails.OriginalParentLoanNumber)
                    ? loanNumber
                    : previousLoanDetails.OriginalParentLoanNumber;
            loanInformation.LoanType = loanInformationRequest.LoanType;
            loanInformation.FunderId = loanInformationRequest.FunderId;
            loanInformation.AdditionalRefId = loanInformationRequest.AdditionalRefId;
            loanInformation.AggregatorName = loanInformationRequest.AggregatorName;
            loanInformation.Beneficiary = loanInformationRequest.Beneficiary;
            loanInformation.LoanReferenceNumber = loanInformationRequest.LoanReferenceNumber;
            loanInformation.BorrowerId = loanInformationRequest.BorrowerId;
            loanInformation.SequenceNumber = loanInformationRequest.LoanType == LoanType.Renewal
                ? (previousLoanDetails.SequenceNumber + 1)
                : 0;
            loanInformation.PaymentInstruments = new List<IPaymentInstrument>();
            if (loanInformationRequest.PaymentInstruments != null)
            {
                loanInformation.PaymentInstruments = new List<IPaymentInstrument>();
                foreach (var paymentInstrument in loanInformationRequest.PaymentInstruments)
                {
                    if (paymentInstrument == null)
                        continue;
                    var payment = paymentInstrument != null ? new PaymentInstrument(paymentInstrument) : null;
                    paymentInstrument.Id = payment.Id;
                    loanInformation.PaymentInstruments.Add(payment);
                }
            }

            loanInformation.PortFolioId = loanInformationRequest.PortFolioId;
            Currency currency;
            Enum.TryParse(LoanConfiguration.Currency, out currency);
            loanInformation.Currency = currency;

            if (loanInformationRequest.ApplicantRequest != null && loanInformationRequest.ApplicantRequest.Any())
            {
                var primaryApplicantDetails =
                    loanInformationRequest.ApplicantRequest.Where(x => x.IsPrimary).FirstOrDefault();
                if (primaryApplicantDetails != null)
                {
                    loanInformation.PrimaryApplicantDetails = new ApplicantDetails(primaryApplicantDetails);
                }

                loanInformation.ApplicantDetails = new List<IApplicantDetails>();
                foreach (var applicantRequest in loanInformationRequest.ApplicantRequest.Where(x => !x.IsPrimary))
                {
                    if (applicantRequest != null)
                    {
                        var applicantDetail = new ApplicantDetails(applicantRequest);
                        loanInformation.ApplicantDetails.Add(applicantDetail);
                    }
                }
            }

            if (loanInformationRequest.BusinessRequest != null && loanInformationRequest.BusinessRequest.Any())
            {
                var primaryBusinessDetails =
                    loanInformationRequest.BusinessRequest.Where(x => x.IsPrimary).FirstOrDefault();
                if (primaryBusinessDetails != null)
                {
                    loanInformation.PrimaryBusinessDetails = new BusinessDetails(primaryBusinessDetails);
                }

                loanInformation.BusinessDetails = new List<IBusinessDetails>();
                foreach (var businessRequest in loanInformationRequest.BusinessRequest.Where(x => !x.IsPrimary))
                {
                    if (businessRequest != null)
                    {
                        var businessDetail = new BusinessDetails(businessRequest);
                        await Task.Run(() => loanInformation.BusinessDetails.Add(businessDetail));
                    }
                }
            }

            PopulateInvoiceDetails(loanInformationRequest, loanInformation);
            return loanInformation;
        }

        private static void PopulateInvoiceDetails(LoanInformationRequest from, LoanInformation to)
        {
            to.InvoiceDetails = new List<ILoanInvoiceDetails>();

            // validateInput
            // If there are no invoice details provided in input 
            // OR it's not an supply chain financing on boarding, return
            if (!(from.InvoiceDetails?.Any() ?? false) ||
                to.ProductPortfolioType != ProductPortfolioType.SCF)
            {
                return;
            }

            // populateData
            foreach (var inv in from.InvoiceDetails)
            {
                var invoiceDetail = new LoanInvoiceDetails
                {
                    Id = Guid.NewGuid().ToString("N"),
                    InvoiceID = inv.InvoiceID,
                    IssuerName = inv.IssuerName,
                    InvoiceAmount = inv.InvoiceAmount,
                    DateOfInvoice = inv.DateOfInvoice,
                    DueDateOfInvoice = inv.DueDateOfInvoice,
                    CreditDaysOfInvoice = inv.CreditDaysOfInvoice
                };
                to.InvoiceDetails.Add(invoiceDetail);
            }
        }


        private async Task AddHistory(string loanNumber, string bankId)
        {
            var bankDetails = await BankDetailsRepository.GetBankDetails(loanNumber, bankId);
            IBankDetailsHistory bankHistoryDetails = new BankDetailsHistory
            {
                AccountNumber = bankDetails.AccountNumber,
                AccountType = bankDetails.AccountType,
                BankHolderName = bankDetails.BankHolderName,
                IsActive = bankDetails.IsActive,
                LoanNumber = bankDetails.LoanNumber,
                Name = bankDetails.Name,
                RoutingNumber = bankDetails.RoutingNumber,
                UpdatedDateTime = TenantTime.Now.DateTime,
                BankId = bankDetails.BankId
            };

            await BankDetailsHistoryRepository.AddHistory(bankHistoryDetails);
        }

        private async Task AddHistory(string loanNumber)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            var loanHistory = new LoanInformationHistory(loanInformation);
            loanHistory.UpdatedDateTime = TenantTime.Now.DateTime;
            await LoanOnboardingHistoryRepository.AddHistory(loanHistory);
        }

        private async Task InitiateWorkFlow(ILoanInformation loanInformation)
        {
            // Get Workflow's by productId.
            var workFlows = await ProductService.GetAllStatusWorkFlow(loanInformation.LoanProductId);
            if (workFlows == null || !workFlows.Any(w => w.WorkFlowType == WorkFlowType.LoanServicing))
                throw new ArgumentException(
                    $"Workflow for type {WorkFlowType.LoanServicing} not defined in product: {loanInformation.LoanProductId}");

            var statusFlow = workFlows.FirstOrDefault(w => w.WorkFlowType == WorkFlowType.LoanServicing);

            // Initiate status workflow.
            await StatusManagementService.ProcessStatusWorkFlow("loan", loanInformation.LoanNumber,
                loanInformation.LoanProductId,
                statusFlow != null ? Convert.ToString(statusFlow.WorkFlowName) : LoanConfiguration.DefaultStatusFlow,
                WorkFlowStatus.Initiated);
        }

        private void EnsureValidBankDetails(IBankDetails bankDetails, ILoanInformation loanInformation)
        {
            if (loanInformation.PaymentInstruments == null || !loanInformation.PaymentInstruments.Any())
                throw new ArgumentException(
                    $"Payment instruments not defined for loan number {loanInformation.LoanNumber}");

            var paymentInstrument = loanInformation.PaymentInstruments
                .Where(p => p.Id == bankDetails.PaymentInstrumentId).FirstOrDefault();
            if (paymentInstrument == null)
                throw new ArgumentNullException($"Payment instrument not found for {bankDetails.PaymentInstrumentId}");

            //if (bankDetails.IsActive == false)
            //{
            //    if (!loanInformation.PaymentInstruments.Where(p => p.Id != bankDetails.PaymentInstrumentId && p.IsActive == true).Any())
            //        throw new ArgumentException($"Cannot change {bankDetails.IsActive} as it is necessary to have one payment instrument type as true.");
            //}

            if (paymentInstrument.PaymentInstrumentType == PaymentInstrumentType.Ach)
            {
                if (LoanConfiguration.IsCashBalanceLinked && string.IsNullOrWhiteSpace(bankDetails.BankId))
                    throw new ArgumentNullException($"Invalid value for {nameof(bankDetails.BankId)}");

                if (string.IsNullOrWhiteSpace(bankDetails.AccountNumber))
                    throw new ArgumentNullException($"Invalid value for {nameof(bankDetails.AccountNumber)}");

                if (string.IsNullOrWhiteSpace(bankDetails.BankHolderName))
                    throw new ArgumentNullException($"{nameof(bankDetails.BankHolderName)} cannot be null");

                if (string.IsNullOrWhiteSpace(bankDetails.Name))
                    throw new ArgumentNullException($"{nameof(bankDetails.Name)} cannot be null");

                if (string.IsNullOrWhiteSpace(bankDetails.RoutingNumber))
                    throw new ArgumentNullException($"Invalid value for {nameof(bankDetails.RoutingNumber)}");

                if (!Enum.IsDefined(typeof(LMS.LoanManagement.Abstractions.AccountType), bankDetails.AccountType))
                    throw new ArgumentNullException($"Invalid {nameof(bankDetails.AccountType)} value");
            }
        }

        private async Task EnsureValidInput(LoanInformationRequest loanInformationRequest, bool IsUpdate = false)
        {
            if (loanInformationRequest == null)
                throw new ArgumentNullException($"{nameof(loanInformationRequest)} cannot be null");

            if (string.IsNullOrWhiteSpace(loanInformationRequest.LoanApplicationNumber))
                throw new ArgumentNullException(
                    $"{nameof(loanInformationRequest.LoanApplicationNumber)} cannot be null");

            var exist = await LoanOnboardingRepository.GetLoanInformationByApplicationNumber(loanInformationRequest
                .LoanApplicationNumber);

            if (exist != null && !IsUpdate)
                throw new ArgumentException("Loan is already on-boarded for this application number");

            if (loanInformationRequest.LoanStartDate == null)
                throw new ArgumentNullException($"{nameof(loanInformationRequest.LoanStartDate)} cannot be null");

            if (loanInformationRequest.LoanFundedDate == null)
                throw new ArgumentNullException($"{nameof(loanInformationRequest.LoanFundedDate)} cannot be null");

            if (string.IsNullOrWhiteSpace(loanInformationRequest.LoanProductId))
                throw new ArgumentNullException($"{nameof(loanInformationRequest.LoanProductId)} cannot be null");

            var product = await ProductService.Get(loanInformationRequest.LoanProductId);
            if (product == null)
                throw new ArgumentException("Product does not exist");

            if (!Enum.IsDefined(typeof(ProductPortfolioType), loanInformationRequest.ProductPortfolioType))
                throw new ArgumentException($"Invalid {nameof(loanInformationRequest.ProductPortfolioType)} value");

            if (Convert.ToString(product.PortfolioType) !=
                Convert.ToString(loanInformationRequest.ProductPortfolioType))
                throw new ArgumentException(
                    $"{nameof(loanInformationRequest.ProductPortfolioType)} value does not match with portfolio type of product: {loanInformationRequest.LoanProductId}");

            if (Convert.ToString(product.ProductCategory) != Convert.ToString(loanInformationRequest.ProductCategory))
                throw new ArgumentException(
                    $"{nameof(loanInformationRequest.ProductCategory)} value does not match with portfolio type of product: {loanInformationRequest.LoanProductId}");

            if (loanInformationRequest.ProductCategory == Abstractions.ProductCategory.Business &&
                (loanInformationRequest.BusinessRequest == null || !loanInformationRequest.BusinessRequest.Any()))
                throw new ArgumentException(
                    $"{nameof(loanInformationRequest.BusinessRequest)} cannot be null when {nameof(loanInformationRequest.ProductCategory)} is {loanInformationRequest.ProductCategory}");

            if (loanInformationRequest.ProductCategory == Abstractions.ProductCategory.Individual &&
                (loanInformationRequest.ApplicantRequest == null || !loanInformationRequest.ApplicantRequest.Any()))
                throw new ArgumentException(
                    $"{nameof(loanInformationRequest.ApplicantRequest)} cannot be null when {nameof(loanInformationRequest.ProductCategory)} is {loanInformationRequest.ProductCategory}");

            if (loanInformationRequest.ApplicantRequest != null &&
                loanInformationRequest.ProductCategory == Abstractions.ProductCategory.Individual &&
                loanInformationRequest.ApplicantRequest.Where(x => x.IsPrimary).Count() != 1)
                throw new ArgumentException($"IsPrimary cannot be set for multiple applicant details");

            if (loanInformationRequest.ApplicantRequest != null &&
                loanInformationRequest.ApplicantRequest.Where(x => x.IsPrimary).Count() < 1)
                throw new ArgumentException($"Primary applicant must be available in request");

            if (loanInformationRequest.BusinessRequest != null &&
                loanInformationRequest.ProductCategory == Abstractions.ProductCategory.Business &&
                loanInformationRequest.BusinessRequest.Where(x => x.IsPrimary).Count() != 1)
                throw new ArgumentException($"IsPrimary cannot be set for multiple business details");

            if (loanInformationRequest.ProductCategory == Abstractions.ProductCategory.Individual &&
                loanInformationRequest.BusinessRequest != null)
                throw new ArgumentException(
                    $"Cannot provide business details for {loanInformationRequest.ProductCategory}");

            if (loanInformationRequest.LoanType == LoanType.Renewal &&
                string.IsNullOrEmpty(loanInformationRequest.ParentApplicationNumber))
                throw new ArgumentException(
                    $"{nameof(loanInformationRequest.LoanProductId)} cannot be null for Renewal Loans");

            if (!string.IsNullOrWhiteSpace(loanInformationRequest.AdditionalRefId) &&
                !Regex.IsMatch(loanInformationRequest.AdditionalRefId, "^[a-zA-Z0-9]*$"))
                throw new ArgumentException($"{nameof(loanInformationRequest.AdditionalRefId)} must be alphanumeric ");

            if (!string.IsNullOrWhiteSpace(loanInformationRequest.BorrowerId) &&
                !Regex.IsMatch(loanInformationRequest.BorrowerId, "^[a-zA-Z0-9]*$"))
                throw new ArgumentException($"{nameof(loanInformationRequest.BorrowerId)} must be alphanumeric ");

            EnsureValidApplicant(loanInformationRequest.ApplicantRequest);
            EnsureValidBusiness(loanInformationRequest.BusinessRequest);
            EnsureValidPaymentInstruments(loanInformationRequest.PaymentInstruments);
            EnsureValidInvoiceDetails(loanInformationRequest.LoanApplicationNumber,
                loanInformationRequest.ProductPortfolioType, loanInformationRequest.InvoiceDetails);
        }

        private void EnsureValidPaymentInstruments(List<IPaymentInstrumentRequest> paymentInstruments)
        {
            if (paymentInstruments != null)
            {
                if (paymentInstruments.Count(p => p.IsActive == true) > 1)
                    throw new ArgumentException($"Only one can be active at a time in payment instrument type");

                foreach (var paymentInstrument in paymentInstruments)
                {
                    if (!Enum.IsDefined(typeof(PaymentInstrumentType), paymentInstrument.PaymentInstrumentType))
                        throw new ArgumentException($"Invalid {nameof(paymentInstrument.PaymentInstrumentType)} value");
                }
            }
        }

        private void EnsureValidApplicant(List<IApplicantRequest> applicantRequest)
        {
            if (applicantRequest != null)
            {
                foreach (var applicant in applicantRequest)
                {
                    if (applicant.Addresses != null)
                    {
                        foreach (var address in applicant.Addresses)
                        {
                            if (address == null)
                                continue;

                            if (!Enum.IsDefined(typeof(AddressType), address.AddressType))
                                throw new ArgumentException($"Invalid AddressType value");
                        }
                    }

                    if (applicant.Emails != null)
                    {
                        foreach (var email in applicant.Emails)
                        {
                            if (email == null)
                                continue;

                            if (!Enum.IsDefined(typeof(EmailType), email.EmailType))
                                throw new ArgumentException($"Invalid EmailType value");
                        }
                    }

                    if (applicant.Phones != null)
                    {
                        foreach (var phone in applicant.Phones)
                        {
                            if (phone == null)
                                continue;

                            if (!Enum.IsDefined(typeof(PhoneType), phone.PhoneType))
                                throw new ArgumentException($"Invalid PhoneType value");
                        }
                    }

                    if (applicant.Name == null)
                        throw new ArgumentNullException($"{nameof(applicant.Name)} cannot be null");

                    if (string.IsNullOrWhiteSpace(applicant.Name.First) ||
                        string.IsNullOrWhiteSpace(applicant.Name.Last))
                        throw new ArgumentNullException(
                            $"{nameof(applicant.Name.First)} and {nameof(applicant.Name.Last)} cannot be null");

                    if (applicant.PII == null)
                        throw new ArgumentNullException($"{nameof(applicant.PII)} cannot be null");

                    string personalIdentifierRule = null;
                    LoanConfiguration.ValidationRules.TryGetValue("PersonalIdentifiers", out personalIdentifierRule);
                    if (!string.IsNullOrWhiteSpace(personalIdentifierRule))
                    {
                        var result =
                            DecisionEngineService.Execute<dynamic, ValidationResult>(personalIdentifierRule,
                                applicant.PII);

                        if (!result.Result)
                            throw new ArgumentException($"{result.Details}");
                    }

                    if (string.IsNullOrWhiteSpace(applicant.PositionOfLoan))
                        throw new ArgumentNullException($"{nameof(applicant.PositionOfLoan)} cannot be null");
                }
            }
        }

        private void EnsureValidBusiness(List<IBusinessRequest> businessRequest)
        {
            if (businessRequest != null)
            {
                foreach (var business in businessRequest)
                {
                    if (string.IsNullOrWhiteSpace(business.BusinessName))
                        throw new ArgumentNullException($"{nameof(business.BusinessName)} cannot be null");

                    if (business.Address != null && !Enum.IsDefined(typeof(AddressType), business.Address.AddressType))
                        throw new ArgumentException($"Invalid {nameof(business.Address.AddressType)} value");

                    if (business.Email != null && !Enum.IsDefined(typeof(EmailType), business.Email.EmailType))
                        throw new ArgumentException($"Invalid {nameof(business.Email.EmailType)} value");

                    if (business.Phone != null && !Enum.IsDefined(typeof(PhoneType), business.Phone.PhoneType))
                        throw new ArgumentException($"Invalid {nameof(business.Phone.PhoneType)} value");


                    string businessIdentifierRule = null;
                    LoanConfiguration.ValidationRules.TryGetValue("BusinessIdentifiers", out businessIdentifierRule);
                    if (!string.IsNullOrWhiteSpace(businessIdentifierRule))
                    {
                        var result = DecisionEngineService.Execute<dynamic, ValidationResult>(businessIdentifierRule,
                            new {business.BusinessIdentifiers});

                        if (!result.Result)
                            throw new ArgumentException($"{result.Details}");
                    }
                }
            }
        }

        private void EnsureValidOtherContact(IOtherContact otherContact)
        {
            if (otherContact == null)
                throw new ArgumentNullException($"{nameof(otherContact)} cannot be null");

            if (otherContact.Email != null)
            {
                foreach (var email in otherContact.Email)
                {
                    if (email == null)
                        throw new ArgumentNullException($"{nameof(email)} cannot be null");

                    if (string.IsNullOrWhiteSpace(email.ContactPerson))
                        throw new ArgumentNullException($"{nameof(email.ContactPerson)} cannot be null");
                }
            }
        }

        /// <summary>
        /// Validates Invoice Details
        /// </summary>
        /// <param name="loanApplicationNumber"></param>
        /// <param name="productPortfolioType"></param>
        /// <param name="invoiceDetails"></param>
        /// <exception cref="ArgumentException"></exception>
        private static void EnsureValidInvoiceDetails(string loanApplicationNumber,
            ProductPortfolioType productPortfolioType, IReadOnlyCollection<ILoanInvoiceDetails> invoiceDetails)
        {
            // no validations needed, see LFPROJ-1252
        }

        /// <summary>
        /// Set Invoice Details for Insertion eg during loan onboard
        /// </summary>
        /// <param name="loanInformation"></param>
        private static void SetInvoiceDetailsForLoanOnBoard(ILoanInformation loanInformation)
        {
            if (loanInformation.InvoiceDetails == null)
            {
                loanInformation.InvoiceDetails = new List<ILoanInvoiceDetails>();
                return;
            }

            foreach (var invoice in loanInformation.InvoiceDetails)
            {
                if (string.IsNullOrWhiteSpace(invoice.Id))
                {
                    invoice.Id = Guid.NewGuid().ToString("N");
                }
            }
        }

        private async Task EnsureValidApplicantDatails(List<IApplicantRequest> applicantRequest)
        {
            await Task.Run(() => EnsureValidApplicant(applicantRequest));
        }

        #endregion

        public async Task<ILoanInformation> GetLoanInformationByLoanReferenceNumber(string loanReferenceNumber)
        {
            var loanInformation =
                await LoanOnboardingRepository.GetLoanInformationByLoanReferenceNumber(loanReferenceNumber);
            if (loanInformation == null)
                throw new NotFoundException(
                    $"Loan information for application number: {loanReferenceNumber} is not available.");
            return loanInformation;
        }

        public async Task Unwind(string loanNumber, Abstractions.IUnwindRequest request)
        {
            var loanDetails = await PaymentProcessorService.GetFundingData("loan",loanNumber);

            if (loanDetails != null && loanDetails.Any())
            {
                throw new InvalidOperationException($"Payment is exist for the loan {loanNumber}.");
            }

            var transactions = await AccrualBalanceService.GetTransactions(loanNumber);

            if (request.UnwindFeesAtOrigination == true)
            {
                var fees = await AccrualBalanceService.GetFees(loanNumber);
                var feesAppliedDuringOnBoard = fees.Where(i => i.IsOnBoard == true);
                var transactionsPaymentReferenceIds = feesAppliedDuringOnBoard.Select(t => t.PaymentIdReference);
                var transactionToReverse = transactions.Where(t => transactionsPaymentReferenceIds.Contains(t.PaymentReferenceId));
                foreach (var transaction in transactionToReverse)
                {
                    await AccrualBalanceService.ReversePayment(loanNumber, new ReversePayment()
                    {
                        TransactionReferenceId = transaction.Id,
                        ProcessingDate = TenantTime.Now.Date
                    });
                }
            }

            await AccrualBalanceService.Unwind(loanNumber, new UnwindRequest(){ UnwindFeesAtOrigination=request.UnwindFeesAtOrigination });

            var statusWorkFlow = await StatusManagementService.GetActiveStatusWorkFlow("loan", loanNumber);

            await StatusManagementService.ChangeStatus("loan", loanNumber,
                statusWorkFlow != null
                    ? Convert.ToString(statusWorkFlow.StatusWorkFlowId)
                    : LoanConfiguration.DefaultStatusFlow, LoanConfiguration.Statuses["UnWind"], null);

        }

        public async Task SendBorrowerInvite(IBorrowerEmailData borrowerData)
        {
            var emailDetails = new
                {
                    EntityType = borrowerData.EntityType,
                    EntityId = borrowerData.BorrowerId,
                    Email = borrowerData.BorrowerEmail,
                    BorrowerName = borrowerData.BorrowerName,
                    PortalUrl = borrowerData.PortalUrl
                };
                var email = await EmailService.Send(borrowerData.EntityType, borrowerData.BorrowerId, borrowerData.TemplateName, borrowerData.TemplateVersion, emailDetails);
        }
    }
}
