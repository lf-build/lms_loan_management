using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Calendar.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LMS.Foundation.Amortization;
using LMS.Foundation.Calculus;
using LMS.LoanManagement.Abstractions;

namespace LMS.LoanManagement
{
    public partial class LoanScheduleService
    {
        /// <summary>
        /// schedule missed
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="id"></param>
        /// <param name="paymentScheduleRequest"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public async Task<ILoanSchedule> ScheduleMissed(string loanNumber, string id,
            IPaymentScheduleRequest paymentScheduleRequest)
        {
            ValidateInputForScheduleMissed(loanNumber, id, paymentScheduleRequest);

            var (loanSchedule, roundingMethod, roundingDigit, mcaPartialPayment, mcaMissedPayment,
                isDoubleScheduleForHoliday, partial, currentInstallmentDetails
                ) = await GetDataForScheduleMissed(loanNumber, paymentScheduleRequest);

            ValidateDataForScheduleMissed(loanNumber, mcaPartialPayment, partial);

            PrepareDataForScheduleMissed(loanNumber, paymentScheduleRequest, loanSchedule, partial,
                mcaMissedPayment, isDoubleScheduleForHoliday, roundingDigit, roundingMethod,
                LoanConfiguration.ScheduleHolidays, CalendarService, mcaPartialPayment, currentInstallmentDetails);

            await UpdateDataForScheduleMissed(loanSchedule);
            return loanSchedule;
        }

        #region Private Methods

        private void ValidateInputForScheduleMissed(string loanNumber, string id,
            IPaymentScheduleRequest paymentScheduleRequest)
        {
            //validateInput
            if (paymentScheduleRequest == null)
            {
                throw new ArgumentNullException(
                    $"For LoanNumber {loanNumber} and id {id}, the {nameof(paymentScheduleRequest)} cannot be null");
            }
        }

        /// <summary>
        /// get data for schedule missed
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="paymentScheduleRequest"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        /// <exception cref="ArgumentException"></exception>
        private async Task<(ILoanSchedule, string, string, string, MCA, bool, ILoanScheduleDetails,
                IEnumerable<ILoanScheduleDetails>)>
            GetDataForScheduleMissed(string loanNumber, IPaymentScheduleRequest paymentScheduleRequest)
        {
            var loanInformation = await LoanOnboardingRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
            {
                throw new NotFoundException($"Loan details for loan number: {loanNumber} is not available.");
            }

            var loanSchedule = await LoanScheduleRepository.GetLoanSchedule(loanNumber, null);
            if (loanSchedule == null)
            {
                throw new NotFoundException($"Loan schedule details for loan number: {loanNumber} is not available.");
            }

            if (loanSchedule.ScheduleDetails == null)
            {
                throw new NotFoundException(
                    $"Payment schedule details for loan number: {loanNumber} is not available.");
            }

            //Get all the details of the missed installment
            // - Record of Miss
            // - Original record
            // - all related records that were created owing to the miss
            var currentInstallmentDetails = loanSchedule.ScheduleDetails.Where(p =>
                p.Installmentnumber == paymentScheduleRequest.InstallmentNumber ||
                p.RefernceInstallmentnumber == paymentScheduleRequest.InstallmentNumber);

            var partial = loanSchedule.ScheduleDetails.FirstOrDefault(p =>
                p.Installmentnumber == paymentScheduleRequest.InstallmentNumber && !p.IsMissed &&
                p.ScheduleDate == paymentScheduleRequest.ScheduleDate.Date);

            var productDetails = await GetProductParameters(loanInformation.LoanProductId, loanNumber);

            if (productDetails == null)
            {
                throw new ArgumentException($"Product details not set for loan number: {loanNumber}");
            }

            //validateParameters
            if (productDetails.ProductParameters == null)
            {
                throw new ArgumentException($"Product parameters not set for loan number: {loanNumber}");
            }

            var productParameter = productDetails.ProductParameters.ToList();
            var roundingMethod = productParameter.Where(p => p.Name == "RoundingMethod").Select(r => r.Value)
                .FirstOrDefault();
            var roundingDigit = productParameter.Where(p => p.Name == "CurrencyDecimalPoint").Select(r => r.Value)
                .FirstOrDefault();
            var mcaPartialPayment = productParameter.Where(p => p.ParameterId == "MCA_PARTIAL_PAYMENT")
                .Select(r => r.Value).FirstOrDefault();
            if (!Enum.TryParse(productParameter.Where(p => p.ParameterId == "MCA_MISSED_PAYMENT")
                .Select(r => r.Value).FirstOrDefault(), out MCA mcaMissedPayment))
            {
                throw new ArgumentException(
                    $"Product parameter MCA_MISSED_PAYMENT not set for loan number: {loanNumber}");
            }

            var isDoubleScheduleForHoliday = productParameter.Where(r => r.Name == "IsDoubleScheduleForHoliday")
                                                 .Select(r => r.Value)
                                                 .FirstOrDefault()?.ToLower().Equals("true") ?? false;

            return (loanSchedule, roundingMethod, roundingDigit, mcaPartialPayment, mcaMissedPayment,
                isDoubleScheduleForHoliday,
                partial, currentInstallmentDetails);
        }

        /// <summary>
        /// validate data for schedule missed
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="mcaPartialPayment"></param>
        /// <param name="partial"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        private static void ValidateDataForScheduleMissed(string loanNumber, string mcaPartialPayment,
            ILoanScheduleDetails partial)
        {
            if (string.IsNullOrEmpty(mcaPartialPayment))
            {
                throw new ArgumentException(
                    $"Product parameter MCA_PARTIAL_PAYMENT not set for loan number: {loanNumber}");
            }

            if (partial == null)
            {
                throw new InvalidOperationException("Schedule not found");
            }
        }


        /// <summary>
        /// prepare data for schedule missed
        /// </summary>
        /// <param name="loanNumber"></param>
        /// <param name="paymentScheduleRequest"></param>
        /// <param name="loanSchedule"></param>
        /// <param name="partial"></param>
        /// <param name="mcaMissedPayment"></param>
        /// <param name="isDoubleScheduleForHoliday"></param>
        /// <param name="roundingDigit"></param>
        /// <param name="roundingMethod"></param>
        /// <param name="scheduleHolidays"></param>
        /// <param name="calendarService"></param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="Exception"></exception>
        private void PrepareDataForScheduleMissed(
            string loanNumber, IPaymentScheduleRequest paymentScheduleRequest,
            ILoanSchedule loanSchedule, ILoanScheduleDetails partial, MCA mcaMissedPayment,
            bool isDoubleScheduleForHoliday,
            string roundingDigit, string roundingMethod, bool scheduleHolidays, ICalendarService calendarService,
            string mcaPartialPayment, IEnumerable<ILoanScheduleDetails> currentInstallmentDetails)
        {
            // The following possibilities exist:
            // 1. Accrual was done, and one or more payment for that date is found to be missed. - multiple installment numbers
            // 2. Installment was fully paid, then accrual was done, and then it was reversed. 
            // 3. Installment was partially paid, then accrual was done, leading to other related installments getting created/amended, then 
            //    it was reversed
            // 4. None of the above happened
            var nextScheduleInterest = 0.0;
            var nextSchedulePrincipal = 0.0;
            var nextScheduleFee = 0.0;
            var isNextScheduleSame = false;
            ILoanScheduleDetails partialMissedSchedule = new LoanScheduleDetails();
            partial = new LoanScheduleDetails(partial);
            ILoanScheduleDetails schedule = ScheduleCheck(paymentScheduleRequest, loanSchedule,
                ref nextScheduleInterest, ref nextSchedulePrincipal, ref nextScheduleFee, ref isNextScheduleSame);
            ILoanScheduleDetails newSchedule = new LoanScheduleDetails();
            LoanScheduleRequest loanRequest = null;
            switch (mcaMissedPayment)
            {
                case MCA.Shift:
                    if (!isNextScheduleSame)
                    {
                        if (isDoubleScheduleForHoliday && !scheduleHolidays &&
                            loanSchedule.PaymentFrequency == PaymentFrequency.Daily)
                        {
                            ShiftScheduleForDoublePayment(loanSchedule, schedule.InterestAmount,
                                schedule.PrincipalAmount,
                                schedule.FeeAmount, schedule, newSchedule);
                        }
                        else
                        {
                            ShiftSchedule(loanSchedule, schedule.InterestAmount, schedule.PrincipalAmount,
                                schedule.FeeAmount, schedule, newSchedule);
                        }
                    }
                    else
                    {
                        PartialPayment(loanSchedule, nextScheduleInterest, nextSchedulePrincipal, nextScheduleFee,
                            isNextScheduleSame, schedule, newSchedule, mcaPartialPayment, isDoubleScheduleForHoliday);
                    }

                    break;
                case MCA.Sum:
                    var needToUpdateSchedules = loanSchedule.ScheduleDetails
                        .Where(i => i.Installmentnumber > schedule.Installmentnumber && i.IsMissed == false &&
                                    i.IsPaid == false && i.IsProcessed == false &&
                                    i.ScheduleDate > schedule.ScheduleDate.Date).OrderBy(i => i.ScheduleDate)
                        .FirstOrDefault();
                    loanRequest = new LoanScheduleRequest {PaymentFrequency = loanSchedule.PaymentFrequency};
                    if (!isNextScheduleSame)
                    {
                        if (needToUpdateSchedules != null)
                        {
                            needToUpdateSchedules.FeeAmount += schedule.FeeAmount;
                            needToUpdateSchedules.PrincipalAmount += schedule.PrincipalAmount;
                            needToUpdateSchedules.InterestAmount += schedule.InterestAmount;
                            needToUpdateSchedules.PaymentAmount += schedule.PaymentAmount;
                        }
                        else
                        {
                            // If there is no last payment then need to add the new entry for the Payment
                            var lastSchedule = loanSchedule.ScheduleDetails.LastOrDefault();
                            newSchedule.Installmentnumber = lastSchedule.Installmentnumber + 1;
                            newSchedule.InterestAmount = schedule.InterestAmount;
                            newSchedule.PrincipalAmount = schedule.PrincipalAmount;
                            newSchedule.FeeAmount = schedule.FeeAmount;
                            newSchedule.PaymentAmount = schedule.PaymentAmount;
                            newSchedule.RefernceInstallmentnumber = schedule.Installmentnumber;
                            newSchedule.ScheduleDate = GetFirstPaymentDateBasedOnFrequency(loanRequest,
                                new TimeBucket(lastSchedule.OriginalScheduleDate)).Value;
                            newSchedule.OriginalScheduleDate = loanSchedule.PaymentFrequency == PaymentFrequency.Daily
                                ? newSchedule.ScheduleDate
                                : loanRequest.OriginalFirstPaymentDate.Date;

                            //    newSchedule.ScheduleDate = GetNextScheduleForMCA(loanSchedule.PaymentFrequency, schedule.ScheduleDate);
                            // newSchedule.ScheduleDate = GetNextSchedule(loanSchedule.PaymentFrequency, schedule.ScheduleDate);
                            loanSchedule.ScheduleDetails.Add(newSchedule);
                        }
                    }
                    else
                        PartialPayment(loanSchedule, nextScheduleInterest, nextSchedulePrincipal, nextScheduleFee,
                            isNextScheduleSame, schedule, newSchedule, mcaPartialPayment, isDoubleScheduleForHoliday);

                    break;
                case MCA.Last:
                    if (!isNextScheduleSame)
                    {
                        // Based on last schedule, create new schedule
                        loanRequest = new LoanScheduleRequest {PaymentFrequency = loanSchedule.PaymentFrequency};

                        var lastSchedule = loanSchedule.ScheduleDetails.LastOrDefault();
                        newSchedule.Installmentnumber = lastSchedule.Installmentnumber + 1;
                        newSchedule.InterestAmount = schedule.InterestAmount;
                        newSchedule.PrincipalAmount = schedule.PrincipalAmount;
                        newSchedule.FeeAmount = schedule.FeeAmount;
                        newSchedule.PaymentAmount = schedule.PaymentAmount;
                        newSchedule.RefernceInstallmentnumber = schedule.Installmentnumber;

                        if (isDoubleScheduleForHoliday && !LoanConfiguration.ScheduleHolidays &&
                            loanSchedule.PaymentFrequency == PaymentFrequency.Daily)
                        {
                            var scheduleDate = GetNextSchedule(loanSchedule.PaymentFrequency,
                                lastSchedule.OriginalScheduleDate, true);

                            newSchedule.ScheduleDate = scheduleDate.Item1;
                            newSchedule.OriginalScheduleDate =
                                scheduleDate.Item3 ? scheduleDate.Item2 : newSchedule.ScheduleDate;
                        }
                        else
                        {
                            newSchedule.ScheduleDate = GetFirstPaymentDateBasedOnFrequency(loanRequest,
                                new TimeBucket(lastSchedule.OriginalScheduleDate)).Value;
                            newSchedule.OriginalScheduleDate = loanSchedule.PaymentFrequency == PaymentFrequency.Daily
                                ? newSchedule.ScheduleDate
                                : loanRequest.OriginalFirstPaymentDate.Date;
                        }

                        // newSchedule.ScheduleDate = GetNextScheduleForMCA(loanSchedule.PaymentFrequency, lastSchedule.ScheduleDate);
                        // newSchedule.ScheduleDate = GetNextSchedule(loanSchedule.PaymentFrequency, lastSchedule.ScheduleDate);
                        if (newSchedule.PaymentAmount > 0)
                            loanSchedule.ScheduleDetails.Add(newSchedule);
                    }
                    else
                    {
                        PartialPayment(loanSchedule, nextScheduleInterest, nextSchedulePrincipal, nextScheduleFee,
                            isNextScheduleSame, schedule, newSchedule, mcaPartialPayment, isDoubleScheduleForHoliday);
                    }

                    break;
            }

            if (isNextScheduleSame)
            {
                if (partial != null)
                {
                    partialMissedSchedule.Installmentnumber = partial.Installmentnumber;
                    partialMissedSchedule.InterestAmount = partial.InterestAmount;
                    partialMissedSchedule.PrincipalAmount = partial.PrincipalAmount;
                    partialMissedSchedule.FeeAmount = partial.FeeAmount;
                    partialMissedSchedule.PaymentAmount = partial.PaymentAmount;
                    partialMissedSchedule.OriginalScheduleDate = partial.OriginalScheduleDate;
                    partialMissedSchedule.ScheduleDate = partial.ScheduleDate;
                    partialMissedSchedule.IsMissed = true;
                    loanSchedule.ScheduleDetails.Add(partialMissedSchedule);
                }
            }

            if (loanSchedule.ModStatus == ModStatus.None)
            {
                var FinanceCharge = loanSchedule.OriginalScheduleDetails.Select(c => c.CumulativeInterestAmount)
                    .LastOrDefault();
                double sumOfInterest =
                    RoundOff.Round(
                        loanSchedule.ScheduleDetails.Where(i => i.IsPaid || !i.IsMissed)
                            .Sum(i => i.InterestAmount), Convert.ToInt32(roundingDigit), roundingMethod);
                if (sumOfInterest != FinanceCharge)
                {
                    throw new Exception($"New schedule didn't work, sumOfInterest {sumOfInterest} does not match FinanceCharge {FinanceCharge}");
                }

                double sumOfPrincipal =
                    RoundOff.Round(
                        loanSchedule.ScheduleDetails.Where(i => i.IsPaid || !i.IsMissed)
                            .Sum(i => i.PrincipalAmount), Convert.ToInt32(roundingDigit), roundingMethod);
                if (sumOfInterest + sumOfPrincipal != RoundOff.Round((loanSchedule.LoanAmount + FinanceCharge),
                        Convert.ToInt32(roundingDigit), roundingMethod))
                {
                    throw new Exception($"New schedule didn't work, sumOfInterest {sumOfInterest} + sumOfPrincipal {sumOfPrincipal} does not match LoanAmount {loanSchedule.LoanAmount} + FinanceCharge {FinanceCharge}");
                }
            }

            loanSchedule.ScheduleDetails = loanSchedule.ScheduleDetails.OrderBy(i => i.ScheduleDate)
                .ThenBy(x => x.Installmentnumber).ToList();
        }

        private void ShiftSchedule(ILoanSchedule loanSchedule, double nextScheduleInterest,
            double nextSchedulePrincipal, double nextScheduleFee, ILoanScheduleDetails schedule,
            ILoanScheduleDetails newSchedule)
        {
            newSchedule.Installmentnumber = schedule.Installmentnumber + 1;
            newSchedule.RefernceInstallmentnumber = schedule.Installmentnumber;
            newSchedule.InterestAmount = nextScheduleInterest;
            newSchedule.PrincipalAmount = nextSchedulePrincipal;
            newSchedule.FeeAmount = nextScheduleFee;
            newSchedule.PaymentAmount = nextScheduleInterest + nextSchedulePrincipal + nextScheduleFee;
            var nextInitiatedPayment = loanSchedule.ScheduleDetails
                .Where(x => x.Installmentnumber > schedule.Installmentnumber && x.IsMissed == false &&
                            x.IsProcessed).OrderByDescending(i => i.ScheduleDate).FirstOrDefault();
            if (nextInitiatedPayment == null)
            {
                nextInitiatedPayment = loanSchedule.ScheduleDetails
                    .Where(x => x.Installmentnumber > schedule.Installmentnumber && !x.IsMissed &&
                                x.IsPaid).OrderByDescending(i => i.ScheduleDate).FirstOrDefault();
            }

            var loanRequest = new LoanScheduleRequest {PaymentFrequency = loanSchedule.PaymentFrequency};
            newSchedule.ScheduleDate = GetFirstPaymentDateBasedOnFrequency(loanRequest,
                new TimeBucket(nextInitiatedPayment != null
                    ? nextInitiatedPayment.OriginalScheduleDate
                    : schedule.OriginalScheduleDate)).Value;
            //  newSchedule.ScheduleDate = GetNextSchedule(loanSchedule.PaymentFrequency, schedule.ScheduleDate);
            newSchedule.OriginalScheduleDate = loanSchedule.PaymentFrequency == PaymentFrequency.Daily
                ? newSchedule.ScheduleDate
                : loanRequest.OriginalFirstPaymentDate.Date;
            DateTime updateDateTime = newSchedule.OriginalScheduleDate;
            int updateInstallment = newSchedule.Installmentnumber;
            var needToUpdateSchedules = loanSchedule.ScheduleDetails
                .Where(i => i.Installmentnumber >= schedule.Installmentnumber && !i.IsMissed &&
                            !i.IsPaid && !i.IsProcessed).OrderBy(i => i.ScheduleDate).ToList();
            if (needToUpdateSchedules != null && needToUpdateSchedules.Count > 0)
            {
                if (newSchedule.ScheduleDate.Date < needToUpdateSchedules.FirstOrDefault().ScheduleDate.Date)
                {
                    newSchedule.ScheduleDate = needToUpdateSchedules.FirstOrDefault().ScheduleDate;
                    newSchedule.OriginalScheduleDate = needToUpdateSchedules.FirstOrDefault().OriginalScheduleDate;
                    updateDateTime = newSchedule.OriginalScheduleDate;
                }
            }

            foreach (var item in needToUpdateSchedules)
            {
                var loanDetails = new LoanScheduleRequest {PaymentFrequency = loanSchedule.PaymentFrequency};
                item.ScheduleDate = GetFirstPaymentDateBasedOnFrequency(loanDetails, new TimeBucket(updateDateTime))
                    .Value;
                item.OriginalScheduleDate = loanSchedule.PaymentFrequency == PaymentFrequency.Daily
                    ? item.ScheduleDate
                    : loanDetails.OriginalFirstPaymentDate.Date;

                // item.ScheduleDate = GetNextScheduleForMCA(loanSchedule.PaymentFrequency, updateDateTime);
                // item.ScheduleDate = GetNextSchedule(loanSchedule.PaymentFrequency, updateDateTime);
                item.Installmentnumber = updateInstallment + 1;
                updateDateTime = item.OriginalScheduleDate.Date;
                updateInstallment = item.Installmentnumber;
            }

            loanSchedule.ScheduleDetails.Add(newSchedule);
        }


        /// <summary>
        /// update data for schedule missed
        /// </summary>
        /// <param name="loanSchedule"></param>
        /// <returns></returns>
        private async Task UpdateDataForScheduleMissed(ILoanSchedule loanSchedule)
        {
            await LoanScheduleRepository.UpdateLoanSchedule(loanSchedule.LoanNumber, loanSchedule.Id, loanSchedule);
        }

        /// <summary>
        /// Schedule Check
        /// </summary>
        /// <param name="paymentScheduleRequest"></param>
        /// <param name="loanSchedule"></param>
        /// <param name="nextScheduleInterest"></param>
        /// <param name="nextSchedulePrincipal"></param>
        /// <param name="nextScheduleFee"></param>
        /// <param name="isNextScheduleSame"></param>
        /// <returns></returns>
        private static ILoanScheduleDetails ScheduleCheck(IPaymentScheduleRequest paymentScheduleRequest,
            ILoanSchedule loanSchedule, ref double nextScheduleInterest, ref double nextSchedulePrincipal,
            ref double nextScheduleFee, ref bool isNextScheduleSame)
        {
            var schedule = loanSchedule.ScheduleDetails.FirstOrDefault(p =>
                p.Installmentnumber == paymentScheduleRequest.InstallmentNumber && p.IsMissed == false &&
                p.ScheduleDate == paymentScheduleRequest.ScheduleDate.Date);
            if (schedule != null)
            {
                if (schedule.IsPaid)
                {
                    schedule.RefernceInstallmentnumber = schedule.Installmentnumber;
                    schedule.IsPaid = false;
                    schedule.PaidAmount = 0;
                    schedule.IsMissed = true;
                }
                else if (schedule.PaidAmount > 0)
                {
                    isNextScheduleSame = true;
                    double paidAmount = schedule.PaidAmount;
                    schedule.IsPaid = true;
                    schedule.PaymentAmount = schedule.PaidAmount;
                    if (paidAmount < schedule.InterestAmount)
                    {
                        nextScheduleInterest = schedule.InterestAmount - paidAmount;
                        schedule.InterestAmount = schedule.PaymentAmount;
                        paidAmount = 0;
                    }
                    else
                    {
                        paidAmount = paidAmount - schedule.InterestAmount;
                        nextScheduleInterest = 0;
                    }

                    if (paidAmount < schedule.PrincipalAmount)
                    {
                        nextSchedulePrincipal = schedule.PrincipalAmount - paidAmount;
                        schedule.PrincipalAmount = paidAmount;
                        paidAmount = 0;
                    }
                    else
                    {
                        if (schedule.FeeAmount <= 0)
                        {
                            paidAmount = paidAmount - schedule.PrincipalAmount;
                            nextSchedulePrincipal = 0;
                        }
                        else
                        {
                            if (paidAmount < schedule.FeeAmount)
                            {
                                paidAmount = paidAmount - schedule.FeeAmount;
                                nextScheduleFee = 0;
                            }
                            else
                            {
                                nextScheduleFee = schedule.FeeAmount - paidAmount;
                                paidAmount = 0;
                            }
                        }
                    }
                }
                else
                {
                    // check if this is reversal of a partial payment
                    var originalMissedSchedule = loanSchedule.ScheduleDetails.FirstOrDefault(p =>
                        p.Installmentnumber == paymentScheduleRequest.InstallmentNumber && p.IsMissed &&
                        p.ScheduleDate == paymentScheduleRequest.ScheduleDate.Date);
                    
                    // we come here when there is a reversal of a partial payment. 
                    // In such a case, the following will hold true:
                    // 1. there is a record with !IsPaid and !IsMissed and InstallmentNumber i - call it record C (current)
                    // 2. (optional) there can also be a record with !IsPaid and !IsMissed and RefernceInstallmentnumber i - call it record S (split previously)
                    // 3. there can also be a record with !IsPaid and IsMissed and InstallmentNumber i - call it record M (missed perviously)
                    // In that case :
                    // For Shift case -
                    // C (= schedule) must get deleted - and replaced with new record N
                    // M (= priorMissedSchedule) must get deleted
                    // N - A new record must get inserted with same details as M - and installment date = C + 1 time period
                    // All other records must move forward by 1 time period
                    // nextScheduleInterest = M.InterestAmount
                    // nextSchedulePrincipal = M.PrincipalAmount
                    // nextScheduleFee = M.FeeAmount
                    // isNextScheduleSame = false
                    
                    
                    // This is reversal of a partial payment, so after reversal, the original missed schedule details must hold
                    // eg original instalment was 4500
                    // only 1000 was paid
                    // So, we got a missed payment for 4500, and two new records for 1000 and 3500
                    // Now, this 1000 got reversed
                    // So, we need to remove both the 1000 and 3500 payments, retain the missed record for 4500 and add a new schedule for 4500 with same date as the 1000 schedule
                    if (originalMissedSchedule != null)
                    {
                        nextScheduleInterest = originalMissedSchedule.InterestAmount;
                        nextSchedulePrincipal = originalMissedSchedule.PrincipalAmount;
                        nextScheduleFee = originalMissedSchedule.FeeAmount;
                        
                        schedule.InterestAmount = originalMissedSchedule.InterestAmount;
                        schedule.PrincipalAmount = originalMissedSchedule.PrincipalAmount;
                        schedule.FeeAmount = originalMissedSchedule.FeeAmount;
                        
                        var priorSplitSchedule = loanSchedule.ScheduleDetails.FirstOrDefault(p =>
                            p.RefernceInstallmentnumber == paymentScheduleRequest.InstallmentNumber && !p.IsMissed);
                        if (priorSplitSchedule != null)
                        {
                            loanSchedule.ScheduleDetails.Remove(priorSplitSchedule);
                        }

                    }
                    
                    schedule.IsMissed = true;
                }
            }

            return schedule;
        }

        private void PartialPayment(ILoanSchedule loanSchedule, double nextScheduleInterest,
            double nextSchedulePrincipal, double nextScheduleFee, bool isNextScheduleSame,
            ILoanScheduleDetails schedule, ILoanScheduleDetails newSchedule, string mcaPartialPayment,
            bool isDoubleScheduleForHoliday)
        {
            var loanRequest = new LoanScheduleRequest {PaymentFrequency = loanSchedule.PaymentFrequency};

            if (mcaPartialPayment == MCA.Shift.ToString())
            {
                if (isDoubleScheduleForHoliday && !LoanConfiguration.ScheduleHolidays &&
                    loanSchedule.PaymentFrequency == PaymentFrequency.Daily)
                {
                    ShiftScheduleForDoublePayment(loanSchedule, nextScheduleInterest, nextSchedulePrincipal,
                        nextScheduleFee, schedule, newSchedule);
                }
                else
                {
                    ShiftSchedule(loanSchedule, nextScheduleInterest, nextSchedulePrincipal, nextScheduleFee, schedule,
                        newSchedule);
                }
            }
            else if (mcaPartialPayment == MCA.Sum.ToString())
            {
                var nextInstallment = loanSchedule.ScheduleDetails
                    .Where(i => i.Installmentnumber > schedule.Installmentnumber && i.IsMissed == false &&
                                i.IsPaid == false && i.IsProcessed == false &&
                                i.ScheduleDate > schedule.ScheduleDate.Date).OrderBy(i => i.ScheduleDate)
                    .FirstOrDefault();

                if (nextInstallment != null)
                {
                    nextInstallment.PrincipalAmount += nextSchedulePrincipal;
                    nextInstallment.InterestAmount += nextScheduleInterest;
                    nextInstallment.FeeAmount += nextScheduleFee;
                    nextInstallment.PaymentAmount += (nextScheduleInterest + nextSchedulePrincipal + nextScheduleFee);
                }
                else
                {
                    var lastRecord = loanSchedule.ScheduleDetails.LastOrDefault();
                    newSchedule.Installmentnumber = lastRecord.Installmentnumber + 1;
                    newSchedule.InterestAmount = nextScheduleInterest;
                    newSchedule.PrincipalAmount = nextSchedulePrincipal;
                    newSchedule.FeeAmount = nextScheduleFee;
                    newSchedule.PaymentAmount = nextScheduleInterest + nextSchedulePrincipal + nextScheduleFee;
                    newSchedule.RefernceInstallmentnumber = schedule.Installmentnumber;
                    //if (loanSchedule.PaymentFrequency != PaymentFrequency.Daily)
                    // newSchedule.ScheduleDate = GetFirstPaymentDateBasedOnFrequency (loanRequest, new TimeBucket (schedule.OriginalScheduleDate)).Value;
                    // else {
                    // Because in this case Daily will generate the date which is already scheduled                      
                    newSchedule.ScheduleDate =
                        GetFirstPaymentDateBasedOnFrequency(loanRequest,
                            new TimeBucket(lastRecord.OriginalScheduleDate)).Value;
                    // }
                    newSchedule.OriginalScheduleDate = loanSchedule.PaymentFrequency == PaymentFrequency.Daily
                        ? newSchedule.ScheduleDate
                        : loanRequest.OriginalFirstPaymentDate.Date;

                    // newSchedule.ScheduleDate = GetNextScheduleForMCA(loanSchedule.PaymentFrequency, schedule.ScheduleDate);
                    // newSchedule.ScheduleDate = GetNextSchedule(loanSchedule.PaymentFrequency, schedule.ScheduleDate);
                    loanSchedule.ScheduleDetails.Add(newSchedule);
                }
            }
            else if (mcaPartialPayment == MCA.Last.ToString())
            {
                var lastSchedule = loanSchedule.ScheduleDetails.LastOrDefault();
                newSchedule.Installmentnumber = lastSchedule.Installmentnumber + 1;
                newSchedule.InterestAmount = nextScheduleInterest;
                newSchedule.PrincipalAmount = nextSchedulePrincipal;
                newSchedule.FeeAmount = nextScheduleFee;
                newSchedule.PaymentAmount = nextScheduleInterest + nextSchedulePrincipal + nextScheduleFee;
                newSchedule.RefernceInstallmentnumber = schedule.Installmentnumber;

                if (isDoubleScheduleForHoliday && !LoanConfiguration.ScheduleHolidays &&
                    loanSchedule.PaymentFrequency == PaymentFrequency.Daily)
                {
                    var scheduleDate = GetNextSchedule(loanSchedule.PaymentFrequency, lastSchedule.OriginalScheduleDate,
                        true);

                    newSchedule.ScheduleDate = scheduleDate.Item1;
                    newSchedule.OriginalScheduleDate =
                        scheduleDate.Item3 ? scheduleDate.Item2 : newSchedule.ScheduleDate;
                }
                else
                {
                    newSchedule.ScheduleDate =
                        GetFirstPaymentDateBasedOnFrequency(loanRequest,
                            new TimeBucket(lastSchedule.OriginalScheduleDate)).Value;
                    newSchedule.OriginalScheduleDate = loanSchedule.PaymentFrequency == PaymentFrequency.Daily
                        ? newSchedule.ScheduleDate
                        : loanRequest.OriginalFirstPaymentDate.Date;
                }

                //newSchedule.ScheduleDate = GetNextScheduleForMCA(loanSchedule.PaymentFrequency, lastSchedule.ScheduleDate);
                //newSchedule.ScheduleDate = GetNextSchedule(loanSchedule.PaymentFrequency, lastSchedule.ScheduleDate);
                if (newSchedule.PaymentAmount > 0)
                    loanSchedule.ScheduleDetails.Add(newSchedule);
            }
        }

        #endregion
    }
}